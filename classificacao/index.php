<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Classificação"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//
?>

<?php

if (!empty($_POST)){

	$edital_id = $_POST['edital_id'];

 	$sql_avaliado = "UPDATE acoes_extensao 
 										SET estado_acao = 5 
 									WHERE estado_acao = 7 
 									AND edital = $edital_id";

  if ($mysqli->query($sql_avaliado) === FALSE) {
    $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_avaliado;
  }else{

		if(isset($_POST['projetos_aprovados'])) {
		    foreach($_POST['projetos_aprovados'] as $key => $value) {
	     	$sql_aprovado = "UPDATE acoes_extensao 
	     										SET estado_acao = 7 
	     									WHERE id = $value";
	      if ($mysqli->query($sql_aprovado) === FALSE) {
	        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_aprovado;
	      }
	    }
	  }
  }
  
  if(empty($msg_erro)){
      $mysqli->commit();
      $msg = "Projetos aprovados com sucesso!";
  }else{
      $mysqli->rollback();
      $erro = TRUE;
      $msg = "Erro ao aprovar os projetos.";
  }
?>
<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $msg ?></p>
                <p class="text-warning"><small><?php echo $msg_erro ?></small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#myModalErro").modal('show');
});
</script>
<!-- Modal HTML -->
<?php
}// end post
?>
<div class="container">
<?php
if (!empty($_GET)){

	$edital_id = $_GET['id_edital'];
	$edital_titulo = '';

    $sql = "SELECT * 
            FROM editais e 
            WHERE id = $edital_id";
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
            $edital_titulo = $dados['titulo'];
        }
    }

	$sql = "SELECT a.id, a.titulo , SUM(pa.nota) nota_final, a.estado_acao 
			FROM pergunta_avaliacao pa, 
				acoes_avaliacao av, 
				acoes_extensao a 
			WHERE av.id = pa.id_avaliacao 
			AND av.id_acao_extensao = a.id 
			AND (a.estado_acao = 5 OR a.estado_acao = 7)
			AND a.edital = $edital_id
			GROUP BY pa.id_avaliacao 
			ORDER BY nota_final DESC";

	// para cada permissão exibe os projetos que podem ser vistos
	if ($result = $mysqli->query($sql)) {
		if ($result->num_rows > 0){
			$valor_projeto = 0;
			$soma_valor_projetos = 0;
?>
<h1><b>Classificação - <?php echo $edital_titulo ?></b></h1>
<hr>
<form class="form-horizontal" name="classificacao_edital" id="classificacao_edital" method="post" accept-charset="utf-8">
	<input type="hidden" name="edital_id" value="<?php echo $edital_id;?>" />
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th class="col-md-1">Aprovado?</th>
				<th class="col-md-6">Título</th>
				<th class="col-md-1 text-center">Nota</th>
				<th class="col-md-2 text-center">Orçamento</th>
				<th class="col-md-2 text-center">Acumulado</th>
			</tr>
		</thead>
		<tbody>
<?php
	    	while ($dados = $result->fetch_array()) {
	    		$sql_valor = "select sum(o.valor_solicitado) valor_projeto from orcamentos o where o.acao_extensao = ".$dados['id'];
					if ($result_valor = $mysqli->query($sql_valor)) {
						while ($dados_valor = $result_valor->fetch_array()) {
							$valor_projeto = $dados_valor['valor_projeto'];
						}
					}
					$soma_valor_projetos += $valor_projeto;
					$projeto_aprovado = ($dados['estado_acao'] == 7) ? "checked" : "" ;
?>
			<tr>
				<td class="text-center"><input type='checkbox' name='projetos_aprovados[]' value='<?php echo $dados['id'];?>' <?php echo $projeto_aprovado;?>></td>
				<td><a href="/acoes_extensao/show.php?id=<?php echo $dados['id'];?>" target="_blank"><?php echo $dados['titulo'];?></a></td>
				<td class="text-center"><?php echo $dados['nota_final'];?></td>
				<td class="text-right"><?php echo "R$ ".number_format($valor_projeto,2,",",".");?></td>
				<td class="text-right"><?php echo "R$ ".number_format($soma_valor_projetos,2,",",".");?></td>
			</tr>
<?php
    		}
?>
	    </tbody>
	</table>
	<button type="submit" class="btn btn-danger col-sm-2" id="btnBuscarClassificacao" value="Aprovar" style="margin-bottom: 20px;">Aprovar selecionados</button>
</form>

<?php
		}else{
			echo "
					<h1><b>Classificação - $edital_titulo</b></h1>
					<hr>
				  <h1>Classificação ainda não disponível.</h1>";
		}
	}
}
?>
</div>

<script type="text/javascript">
    $(document).ready( function(){
       $("#classificacao").addClass("active");
    });
</script>
