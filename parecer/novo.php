<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Enviar parecer da comissão extensão da Unidade"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

if( !empty($_POST) ){

    // pega os dados preenchidos no formulario
    $acao_id                        = $_POST['acao_id'];

    $uploaddir = '../uploads/';

    $uploadfile = $uploaddir . 'PARECER_ACAO_' . $acao_id . '.pdf';

    $sql = "UPDATE acoes_extensao SET parecer_pdf = '$uploadfile' WHERE id = $acao_id";

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {
        $upload_ok = TRUE;
        
        if (isset($_FILES['parecer_pdf']) && !empty($_FILES['parecer_pdf']['tmp_name'])){
            $upload_ok = move_uploaded_file($_FILES['parecer_pdf']['tmp_name'], $uploadfile);
        }
        if ($upload_ok) {

            $mysqli->commit();
            echo '<script>window.location.href = "../acoes_extensao/show.php?id='.$acao_id.'"</script>';
            exit;

        } else {
            $msg_erro .= "Possível ataque de upload de arquivo!\n";
        }

    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao realizar upload. '.$msg_erro;
    }
    $mysqli->close();
}

// se estiver alterando uma ação existente pega todos os dados dela
if (isset($_GET['id'])) {
    
    $acao_id = $_GET['id'];

    $link_voltar = "show.php?id=$acao_id";
    
    $sql = "SELECT * FROM acoes_extensao WHERE id = ".$acao_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {

            $acao_parecer_pdf               = $dados['parecer_pdf'];
        }
    }
}


?>

<div class="container">

<form role="form" enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
				<input type="hidden" name="acao_id" value="<?php echo $acao_id;?>" />

<h4>Upload do parecer da Unidade</h4>

<?php 
if(!empty($acao_parecer_pdf)){
?>
                <div class="form-group">
                    <div class="col-sm-8">
                        <a href="<?php echo $acao_parecer_pdf ?>" target="_blank" class="btn btn-danger" role="button"> <span class="glyphicon glyphicon-file"></span> Visualizar parecer anexado</a>
                    </div>
                </div><!--div form-group-->

<?php    
}
?>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="parecer_pdf"><?php echo (empty($acao_parecer_pdf)) ? "Selecione o arquivo com o parecer:" : "Para trocar o parecer, selecione outro arquivo:" ; ?></label>
                    <div class="col-sm-8">
                        <input type="file" name="parecer_pdf" id="parecer_pdf" accept="application/pdf" <?php echo (empty($acao_parecer_pdf)) ? "required" : "" ; ?> />
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='<?php echo "../acoes_extensao/show.php?id=$acao_id";?>';">< Voltar ao Principal</button></li>
                        <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button></li>
                    </ul>
                </div>
                
                <br>

           
    </form>
</div><!--div container-->


<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                parecer_pdf: {
                    validators: {
                        file: {
                            extension: 'pdf',
                            type: 'application/pdf',
                            message: 'O arquivo deve ser PDF'
                        }
                    }
                }                
            }
        })
}
</script>