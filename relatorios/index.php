<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Buscar ação de extensão"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<div class="container-fluid" style="margin-right: 30px; margin-left: 30px;">
<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//
if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
    $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
    $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
    $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
    return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}

?>
  <h1><b>Listar Projetos</b></h1><hr>

  <div class="row" style="margin-bottom: 50px;">
  <div class="table-responsive">
    <?php
    $ano_base = $_GET['ano'];
    $sql_busca = "SELECT e.titulo as edital_titulo,
                    ae.titulo,
                    (SELECT u.nome FROM usuarios u WHERE id = ae.coordenador) AS coordenador,
                    rf.dt_inicio,
                    rf.dt_fim,
                    (SELECT c.nome AS cidade FROM cidades c WHERE c.cod_cidades = rf.local) as cidade,
                    (SELECT e.nome AS estado FROM cidades c, estados e WHERE c.estados_cod_estados = e.cod_estados AND c.cod_cidades = rf.local) as estado,
                    rf.total_despesa_realizada,
                    rf.resultados_alcancados,
                    rf.conclusao,
                    rf.id as rel_final_id
                  FROM rel_final rf,
                    acoes_extensao ae,
                    editais e
                  WHERE rf.id_projeto = ae.id
                    AND ae.estado_acao = 7
                    AND rf.estado = 2
                    AND ae.edital = e.id
                    AND e.ano_base = $ano_base";

    if ($result_busca = $mysqli->query($sql_busca)) {
        if($result_busca->num_rows > 0){

          $tabela = "<table id='table_id' class='table table-bordered table-striped'>";
          $tabela .=  "<thead>";
          $tabela .=    "<tr>";
          $tabela .=      "<th>Edital</th>";
          $tabela .=      "<th>Título</th>";
          $tabela .=      "<th>Coordenador</th>";
          $tabela .=      "<th>Período de Realização</th>";
          $tabela .=      "<th>Estado</th>";
          $tabela .=      "<th>Cidade</th>";

          $sql_perguntas = "SELECT p.*
                            FROM rel_final_perguntas p,
                              edital_rel_final_perguntas ep,
                              editais e
                            where p.id = ep.id_pergunta
                              and ep.id_edital = e.id
                              and e.ano_base = $ano_base
                            ORDER BY p.id ";

          if ($result_perguntas = $mysqli->query($sql_perguntas)) {
            while ($dados_perguntas = $result_perguntas->fetch_array()) {
              $tabela .=      "<th>".$dados_perguntas["descricao"]."</th>";
            }
          }



          $tabela .=       "<th>Total Despeza Realizada</th>";
          $tabela .=       "<th>Resultados Alcançados</th>";
          $tabela .=       "<th>Conclusão</th>";
          $tabela .=    "</tr>";
          $tabela .=  "</thead>";
          $tabela .= "<tbody>";

          while($dados = $result_busca->fetch_array()){

            $tabela .= "<tr>";
            $tabela .= "  <td>".$dados['edital_titulo']."</td>";
            $tabela .= "  <td>".$dados['titulo']."</td>";
            $tabela .= "  <td>".$dados['coordenador']."</td>";

            $data_fim = new DateTime( $dados["dt_fim"] );
            $data_inicio = new DateTime( $dados["dt_inicio"] );

            $intervalo = $data_fim->diff( $data_inicio );
            $periodo_meses = $intervalo->m + ($intervalo->y * 12);

            $tabela .= "  <td>".$periodo_meses." meses</td>";
            $tabela .= "  <td>".$dados['estado']."</td>";
            $tabela .= "  <td>".$dados['cidade']."</td>";

            $sql_perguntas = "SELECT p.*
                              FROM rel_final_perguntas p,
                                edital_rel_final_perguntas ep,
                                editais e
                              where p.id = ep.id_pergunta
                                and ep.id_edital = e.id
                                and e.ano_base = $ano_base
                              ORDER BY p.id ";

            if ($result_perguntas = $mysqli->query($sql_perguntas)) {
              while ($dados_perguntas = $result_perguntas->fetch_array()) {
                $sql_resposta = "SELECT p.descricao,
                              r.detalhe
                        FROM rel_final_item_respostas r,
                            rel_final_pergunta_itens p
                        WHERE r.id_item = p.id
                        AND r.id_rel_final = ".$dados['rel_final_id']."
                        AND p.id_pergunta = ".$dados_perguntas['id'];

                if ($result_resposta = $mysqli->query($sql_resposta)) {
                  $tabela .=      "<td>";
                  while ($dados_resposta = $result_resposta->fetch_array()) {
                    $resp = "";
                    if ($dados_resposta['descricao'] !== "SIM/NÃO" ) {
                      $resp .= $dados_resposta['descricao'].". ";
                      if ($dados_resposta['detalhe'] != "") {
                        $resp .= "<b>Especifique: </b>".$dados_resposta['detalhe'];
                      }
                    } else {
                      if ($dados_resposta['detalhe'] != "") {
                        $resp .= "Não. <b>Especifique: </b>".$dados_resposta['detalhe'];
                      } else {
                        $resp .= "Sim.";
                      }
                    }
                    $tabela .=      $resp."<br>";
                  }
                  $tabela .=      "</td>";
                }

              }
            }
            $tabela .= "  <td>".convertCoin("BR",2,$dados['total_despesa_realizada'])."</td>";
            $tabela .= "  <td>".$dados['resultados_alcancados']."</td>";
            $tabela .= "  <td>".$dados['conclusao']."</td>";
            $tabela .= "</tr>";
          }
          $tabela .= "
              </tbody>
            </table>
          ";
          echo $tabela;
        ?>
        <script type="text/javascript">
          $(document).ready( function () {
            var table = $('#table_id').DataTable( {
              "dom": '<"col-sm-4"l><"col-sm-4 text-center"B><"col-sm-4"f><"col-sm-6"i><"col-sm-6"p><rt><"col-sm-6"i><"col-sm-6"p>',
              "buttons": [
                { extend: 'excel', text: 'Gerar Excel' },
                { extend: 'print', text: 'Imprimir' },
              ],
              "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
              "language": {
                "url": "../style/Portuguese-Brasil.json"
              }
            } );
            $("#table_id tfoot th").each( function ( i ) {

              if ( $(this).text() !== '' ) {
                var select = $('<select style="width: 200px;"><option value=""></option></select>')
                  .appendTo( $(this).empty() )
                  .on( 'change', function () {
                    var val = $(this).val();

                    table.column( i )
                      .search( val ? '^'+$(this).val()+'$' : val, true, false )
                      .draw();
                    } );

                table.column( i ).data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );

              }
            } );
          } );
        </script>
        <?php
        }
    }
    ?>
  </div>
  </div>
</div>
