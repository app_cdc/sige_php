<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Alterar ação de extensão"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<script type="text/javascript">
function alterar_estado(acao_id,rel_final_id,rel_final_estado,rel_final_titulo)
{
  $.ajax({
    type: 'post',
    url: 'alterar_estado.php',
    data: {
      id:rel_final_id,
      estado:rel_final_estado,
      titulo:rel_final_titulo
    },
    success: function (response) {
      $("#myModal").modal('hide');
      if(response){
        //alert("Enviado com sucesso!");
        caminho = "/rel_final/index.php?id=" + acao_id;
        location.replace(caminho);
      }else{
        //document.getElementById("msg").innerHTML="Erro ao enviar";
        $("#myModalErro").modal().find('.modal-body p').text("Erro ao concluir");
        $("#myModalErro").modal('show');
      }
    }
  });
}
</script>

<div class="container">
<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO, AVALIADOR, COORDENADOR);
protegePagina($permissoes);
//

$usuario_id = $_SESSION['UsuarioID'];
$sql_permissao = "SELECT * FROM permissao_usuario pu WHERE pu.usuario = $usuario_id ORDER BY pu.permissao";
$query_permissao = $mysqli->query($sql_permissao);
$usuario_permissoes = array();

if ($result_permissao = $mysqli->query($sql_permissao)) {
    while ($dados_permissao = $query_permissao->fetch_array()) {
        $usuario_permissoes[] = $dados_permissao['permissao'];
    }
}

if( !empty($_POST) ){

  if(isset($_POST['parecer_estado']) && !empty($_POST['parecer_estado'])) {

    $msg_erro = "";

    $rel_final_id = $_POST['rel_final_id'];
    $rel_final_estado = $_POST['parecer_estado'];
    $rel_final_observacao = $_POST['parecer_observacao'];
    $data_atual = date("Y-m-d H:i:s"); // data atual
    $administrador_id = $_POST['administrador_id'];

    $sql_parecer_final = "INSERT INTO parecer_final (id_rel_final,
                                                    id_estado,
                                                    observacao,
                                                    id_usuario,
                                                    data)
                                    VALUES ($rel_final_id,
                                            $rel_final_estado,
                                            '$rel_final_observacao',
                                            $administrador_id,
                                            '$data_atual')";

    if ($mysqli->query($sql_parecer_final) === FALSE) {
        $msg_erro .= "Error: " . $sql_parecer_final . "<br>" . $mysqli->error;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao salvar o parecer final. '.$msg_erro;
    }else{
        $mysqli->commit();
        echo "<p>Parecer final salvo com sucesso!</p>\n";
    }
  }

?>
  <br>
  <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/acoes_extensao/';">< Voltar</button>
<?php

}
else
{

  if (isset($_GET['id'])) {

    $acao_id = $_GET['id'];
    $acao_estado_acao = "";
    $rel_final_id = "";
    $rel_final_estado = "";

    $sql_projeto = "SELECT * FROM acoes_extensao WHERE id=$acao_id ORDER BY id";

    if ($result_projeto = $mysqli->query($sql_projeto)) {
      while ($dados_projeto = $result_projeto->fetch_array()) {

        $acao_titulo                    = $dados_projeto['titulo'];
        $acao_edital                    = $dados_projeto['edital'];
        $acao_estado_acao               = $dados_projeto['estado_acao'];
        $acao_coordenador               = $dados_projeto['coordenador'];
        $acao_criacao                   = $dados_projeto['criacao'];
        $acao_alteracao                 = $dados_projeto['alteracao'];
        $acao_tipo_extensao             = $dados_projeto['tipo_extensao'];
        $acao_indicador1                = $dados_projeto['indicador1'];
        $acao_indicador2                = $dados_projeto['indicador2'];
        $acao_indicador3                = $dados_projeto['indicador3'];
        $acao_indicador4                = $dados_projeto['indicador4'];
        $acao_indicador5                = $dados_projeto['indicador5'];
        $acao_indicador6                = $dados_projeto['indicador6'];
        $acao_indicador7                = $dados_projeto['indicador7'];
        $acao_indicador8                = $dados_projeto['indicador8'];
        $acao_indicador9                = $dados_projeto['indicador9'];
        $acao_indicador10               = $dados_projeto['indicador10'];
        $acao_metodologia               = $dados_projeto['metodologia'];
        $acao_local                     = $dados_projeto['local'];
        $acao_ano_inicio                = $dados_projeto['ano_inicio'];
        $acao_inicio                    = $dados_projeto['inicio'];
        $acao_termino                   = $dados_projeto['termino'];
        $acao_situacao                  = $dados_projeto['situacao'];
        $acao_n_pessoas_alvo_interno    = $dados_projeto['n_pessoas_alvo_interno'];
        $acao_n_pessoas_alvo_externo    = $dados_projeto['n_pessoas_alvo_externo'];
        $acao_caracterizacao_comunidade = $dados_projeto['caracterizacao_comunidade'];
        $acao_introducao                = $dados_projeto['introducao'];
        $acao_objetivo_geral            = $dados_projeto['objetivo_geral'];
        $acao_grau_envolvimento_equipe  = $dados_projeto['grau_envolvimento_equipe'];
        $acao_parceria_instituicao      = $dados_projeto['parceria_instituicao'];
        $acao_em_execucao               = $dados_projeto['em_execucao'];
        $acao_quanto_tempo              = $dados_projeto['quanto_tempo'];
        $acao_em_execucao_descricao     = $dados_projeto['em_execucao_descricao'];
        $acao_linha_extensao            = $dados_projeto['linha_extensao'];

        $sql_local = "SELECT c.nome AS cidade, e.nome AS estado FROM cidades c, estados e WHERE c.estados_cod_estados = e.cod_estados AND c.cod_cidades = ".$acao_local;

        if ($result_local = $mysqli->query($sql_local)) {
            while ($dados_local = $result_local->fetch_array()) {
                $acao_local_estado = $dados_local['estado'];
                $acao_local_cidade = $dados_local['cidade'];
            }
        }

        $sql_rel_final = "SELECT * FROM rel_final WHERE id_projeto=$acao_id";
        $query_rel_final = $mysqli->query($sql_rel_final);
        $count_rel_final = $query_rel_final->num_rows;

        if ($result = $mysqli->query($sql_rel_final)) {
          while ($dados = $result->fetch_array()) {
            $rel_final_id = $dados["id"];
            $rel_final_estado = $dados["estado"];
            $rel_final_dt_inicio = $dados["dt_inicio"];
            $rel_final_dt_fim = $dados["dt_fim"];
            $rel_final_local = $dados["local"];
            $rel_final_conclusao = $dados["conclusao"];
            $rel_final_total_despesa_realizada = $dados["total_despesa_realizada"];

            $sql_local = "SELECT c.nome AS cidade, e.nome AS estado FROM cidades c, estados e WHERE c.estados_cod_estados = e.cod_estados AND c.cod_cidades = ".$rel_final_local;

            if ($result_local = $mysqli->query($sql_local)) {
              while ($dados_local = $result_local->fetch_array()) {
                $rel_final_local_estado = $dados_local['estado'];
                $rel_final_local_cidade = $dados_local['cidade'];
              }
            }


          }
        }

      }
    }

    if ($acao_estado_acao == 7 && ($rel_final_estado == "" || $rel_final_estado == 1) ){ // não terminou de incluir, ainda pode alterar

      //se for alterar os dados mas não é o dono do projeto, expulsa o usuário
      if($acao_coordenador <> $usuario_id){
        //
        echo "<script language='JavaScript'>window.location='../logout.php?erro=RESTRITO';</script><noscript>Se não for direcionado automaticamente, clique <a href='../logout.php?erro=RESTRITO'>aqui</a>.</noscript>";
        exit;
      }

      echo "<h1>Relatório Técnico Final</h1><h3>Projeto: $acao_titulo</h3><hr>";

      $count_participantes = 0;

      $sql = "SELECT valor, data_validade FROM editais WHERE id=".$acao_edital;
      $query = $mysqli->query($sql);
      if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
            $edital_valor = $dados["valor"];
            $edital_data_validade = $dados["data_validade"];
            $data_atual = date("Y-m-d");
        }
      }

      $dados_iniciais_ok = FALSE;
      $participantes_ok = FALSE;

      if($count_rel_final > 0){

        $dados_iniciais_ok = TRUE;

        $sql = "SELECT * FROM participantes WHERE id_rel_final=".$rel_final_id;
        $query = $mysqli->query($sql);
        $count_participantes = $query->num_rows; // conta quantos participantes tem
        if($count_participantes > 0){
          $participantes_ok = TRUE;
        }
      }

        //Mostra a tela para inserir um novo edital
      $iconRelatorio = ($dados_iniciais_ok) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ;
      $styleRelatorio = ($dados_iniciais_ok) ? 'btn btn-primary btn-lg' : 'btn btn-warning btn-lg' ;

      $iconParticipante = ($participantes_ok) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ;
      $styleParticipante = ($participantes_ok) ? 'btn btn-primary btn-lg' : 'btn btn-warning btn-lg' ;

?>

<button onclick="window.location.href='editar.php?acao_id=<?php echo $acao_id; ?>'" class="<?php echo $styleRelatorio; ?>" <?php echo ($acao_estado_acao == 7)?"":"disabled"?> >
  <span class="glyphicon glyphicon-map-marker"></span> Informações <?php echo $iconRelatorio; ?>
</button>&nbsp

<?php
      if ($dados_iniciais_ok) {
?>

<button onclick="window.location.href='../participantes/novo.php?acao_id=<?php echo $acao_id; ?>'" class="<?php echo $styleParticipante; ?>" <?php echo ($acao_estado_acao == 7)?"":"disabled"?> >
  <span class="glyphicon glyphicon-user"></span> Participantes  <?php echo $iconParticipante; ?>
</button>&nbsp


<?php
      }
  if ($dados_iniciais_ok && $participantes_ok) {
    echo "<a href='impressao_pdf.php?id=".$acao_id."' target='_blank' class='btn btn-danger btn-lg' role='button'><span class='glyphicon glyphicon-file'></span> Gerar PDF</a> ";
  }
?>
<hr>
<button class="btn btn-success btn-lg" <?php echo ($dados_iniciais_ok && $participantes_ok) ? "" : "disabled" ; ?> data-toggle="modal" data-target="#myModal">
  <span class="glyphicon glyphicon-ok"></span>  Enviar Relatório Final
</button>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>Deseja enviar mesmo?</p>
                <p class="text-warning"><small>Se você enviar, não poderá fazer mais alterações</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="alterar_estado(<?php echo $acao_id ?>,<?php echo $rel_final_id ?>,2,'<?php echo $acao_titulo ?>')">Enviar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<?php
    }else{ // exibe os dados que foram emviados

      if (!function_exists('convertCoin')) {
        function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
           $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
           $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
           $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
           return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
        }
      }

      if (in_array(ADMINISTRADOR, $usuario_permissoes) || in_array(AVALIADOR, $usuario_permissoes)) {

        if($acao_estado_acao == 7 && $rel_final_estado == 2){

          $count_parecer_final = 0;

          $sql_parecer_final = "SELECT *
                  FROM parecer_final p
                  WHERE p.id_rel_final = $rel_final_id ";

          if ($result_parecer_final = $mysqli->query($sql_parecer_final)) {
            $count_parecer_final = $result_parecer_final->num_rows;
            while ($dados_parecer_final = $result_parecer_final->fetch_array()) {
              $parecer_final_id = $dados_parecer_final['id'];
              $parecer_final_id_estado = $dados_parecer_final['id_estado'];
              $parecer_final_observacao = $dados_parecer_final['observacao'];
              $parecer_final_id_usuario = $dados_parecer_final['id_usuario'];
              $parecer_final_data = $dados_parecer_final['data'];
            }
          }

?>
<h1><b>Parecer Final</b></h1><hr>
<?php
          if ($count_parecer_final == 0) { // se o parecer final não foi feito ainda
?>
<form class="form-horizontal" name="form_parecer_final" id="form_parecer_final" method="post" accept-charset="utf-8">
  <input type="hidden" name="rel_final_id" value="<?php echo $rel_final_id;?>" />
  <input type="hidden" name="administrador_id" value="<?php echo $usuario_id;?>" />
  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_estado">Estado:</label>
      <div class="col-sm-10">
        <select class="form-control" name="parecer_estado" id="parecer_estado" required>
            <option value="">Selecione o estado</option>
            <?php

            $sql_parecer_estado = "SELECT *
                                    FROM parecer_final_estados pfe
                                    ORDER BY pfe.id ";

            if ($result_parecer_estado = $mysqli->query($sql_parecer_estado)) {
                while ($dados_parecer_estado = $result_parecer_estado->fetch_array()) {
                    $parecer_estado_id = $dados_parecer_estado['id'];
                    $parecer_estado_nome = $dados_parecer_estado['descricao'];
                    echo "<option value='$parecer_estado_id'>$parecer_estado_nome</option>";
                }
            }

            ?>
        </select>
      </div>
  </div><!--div form-group-->
  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_observacao">Observação:</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="3" name="parecer_observacao" maxlength="500" required></textarea>
      </div>
  </div><!--div form-group-->
  <div class="col-sm-2"></div>
  <div class="col-sm-10">
    <button type="submit" class="btn btn-warning col-sm-12" id="btnSalvarParecerFinal" value="Salvar Parecer Final">Salvar Parecer Final</button>
  </div>
</form>
<?php
          }else{ // se o parecer final já foi preenchido
?>
<form class="form-horizontal" name="form_parecer_final" id="form_parecer_final" method="post" accept-charset="utf-8">
  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_estado">Estado:</label>
      <div class="col-sm-10">
            <?php

            $sql_parecer_estado = "SELECT *
                                    FROM parecer_final_estados p
                                    WHERE p.id = $parecer_final_id_estado";

            if ($result_parecer_estado = $mysqli->query($sql_parecer_estado)) {
                while ($dados_parecer_estado = $result_parecer_estado->fetch_array()) {
                    $parecer_estado_id = $dados_parecer_estado['id'];
                    $parecer_estado_nome = $dados_parecer_estado['descricao'];
                }
            }

            ?>
          <p class="show_acao_p"><?php echo $parecer_estado_nome;?></p>
      </div>
  </div><!--div form-group-->

  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_observacao">Observação:</label>
      <div class="col-sm-10">
          <p class="show_acao_p"><?php echo $parecer_final_observacao;?></p>
      </div>
  </div><!--div form-group-->

  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_estado">Responsável pelo parecer:</label>
      <div class="col-sm-10">
            <?php

            $sql_usuario = "SELECT *
                                    FROM usuarios u
                                    WHERE id = $parecer_final_id_usuario";

            if ($result_usuario = $mysqli->query($sql_usuario)) {
                while ($dados_usuario = $result_usuario->fetch_array()) {
                    $parecer_usuario_nome = $dados_usuario['nome'];
                }
            }

            ?>
          <p class="show_acao_p"><?php echo $parecer_usuario_nome;?></p>
      </div>
  </div><!--div form-group-->


  <div class="form-group">
      <label class="control-label col-sm-2" for="parecer_observacao">Data:</label>
      <div class="col-sm-10">
          <p class="show_acao_p"><?php echo date_format(date_create($parecer_final_data),'d/m/y - H:i');?></p>
      </div>
  </div><!--div form-group-->


</form>
<?php

          }
        }
      }

?>

<!--<div class="container">-->

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Dados">Dados <i class="fa"></i></a></li>
        <li><a href="#Participantes" data-toggle="tab">Participantes <i class="fa"></i></a></li>
    </ul>

    <form role="form" enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

        <div class="tab-content">
            <div class="tab-pane fade in active" id="Dados">
                <h3>Dados do Relatório Técnico Final</h3>
                <hr>
<?php
      $sql_nome_coordenador = "SELECT * FROM usuarios u WHERE id = $acao_coordenador ";
      $query_nome_coordenador = $mysqli->query($sql_nome_coordenador);

      if ($result_nome_coordenador = $mysqli->query($sql_nome_coordenador)) {
        while ($dados_nome_coordenador = $query_nome_coordenador->fetch_array()) {
          $acao_coordenador_nome = $dados_nome_coordenador['nome'];
        }
      }
?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="coordenador">Coordenador:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_coordenador_nome;?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="titulo">Título:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_titulo;?></p>
                    </div>
                </div><!--div form-group-->
<?php
$data_fim = new DateTime( $rel_final_dt_fim );
$data_inicio = new DateTime( $rel_final_dt_inicio );

$intervalo = $data_fim->diff( $data_inicio );
$periodo_meses = $intervalo->m + ($intervalo->y * 12);
?>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="n_pessoas_alvo_interno">Período de Realização:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $periodo_meses;?> meses</p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>Local</h4>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="estado">Estado:</label>
                    <div class="col-sm-3">
                        <p class='show_acao_p'><?php echo $rel_final_local_estado ?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-2" for="cidade">Cidade:</label>
                    <div class="col-sm-3">
                        <p class='show_acao_p'><?php echo $rel_final_local_cidade ?></p>
                    </div>
                </div><!--div form-group-->

<?php
        $sql_perguntas = "SELECT p.* FROM rel_final_perguntas p, edital_rel_final_perguntas ep where p.id = ep.id_pergunta and ep.id_edital = $acao_edital ORDER BY p.id ";

        if ($result_perguntas = $mysqli->query($sql_perguntas)) {
          while ($dados_perguntas = $result_perguntas->fetch_array()) {
?>
                <hr>
                <h4><?php echo $dados_perguntas["descricao"];?></h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                        $sql_resposta = "SELECT p.descricao,
                                      r.detalhe
                                FROM rel_final_item_respostas r,
                                    rel_final_pergunta_itens p
                                WHERE r.id_item = p.id
                                AND r.id_rel_final = $rel_final_id
                                AND p.id_pergunta = ".$dados_perguntas['id'];

                        if ($result_resposta = $mysqli->query($sql_resposta)) {
                          while ($dados_resposta = $result_resposta->fetch_array()) {
                            $resp = "";
                            if ($dados_resposta['descricao'] !== "SIM/NÃO" ) {
                              $resp .= $dados_resposta['descricao'].". ";
                              if ($dados_resposta['detalhe'] != "") {
                                $resp .= "<b>Especifique: </b>".$dados_resposta['detalhe'];
                              }
                            } else {
                              if ($dados_resposta['detalhe'] != "") {
                                $resp .= "Não. <b>Especifique: </b>".$dados_resposta['detalhe'];
                              } else {
                                $resp .= "Sim.";
                              }
                            }
                            echo "<p class='show_acao_p'> $resp</p>";
                          }
                        }
                        ?>
                    </div>
                </div><!--div form-group-->
<?php
          }
        }
?>

                <hr>
                <h4>Conclusão</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class='show_acao_p'><?php echo $rel_final_conclusao;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

            </div><!--div tab-pane-->

            <div id="Participantes" class="tab-pane fade">

                <h3>Participantes</h3>

                <hr>

                <table class="table table-striped">

                    <thead>
                      <tr class="tabela_cabecalho">
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>RA</th>
                        <th>Unidade</th>
                        <th>Instituição</th>
                        <th>Carga Semanal</th>
                        <th>Carga Total</th>
                      </tr>
                    </thead>

                    <tbody>

                <?php
                    $sql_participantes = "SELECT *
                                            FROM participantes p,
                                                participante_categorias c
                                          WHERE p.id_categoria = c.id
                                            AND p.id_rel_final = $rel_final_id";

                    if ($result_participantes = $mysqli->query($sql_participantes)) {
                      while ($dados_participantes = $result_participantes->fetch_array()) {

                        $participante_nome = $dados_participantes['nome'];
                        $participante_categoria = $dados_participantes['descricao'];
                        $participante_ra = (empty($dados_participantes['ra'])) ? "-" : $dados_participantes['ra'];
                        $participante_unidade = (empty($dados_participantes['unidade'])) ? "-" : $dados_participantes['unidade'];
                        $participante_instituicao = (empty($dados_participantes['instituicao'])) ? "-" : $dados_participantes['instituicao'];
                        $participante_carga_semanal = $dados_participantes['carga_horaria_semanal'];
                        $participante_carga_total = $dados_participantes['carga_horaria_total'];

                        echo "
                        <tr>
                          <td>$participante_nome</td>
                          <td>$participante_categoria</td>
                          <td>$participante_ra</td>
                          <td>$participante_unidade</td>
                          <td>$participante_instituicao</td>
                          <td>$participante_carga_semanal</td>
                          <td>$participante_carga_total</td>
                        </tr>";

                        }
                      }
                ?>
                </tbody>
                </table>
                <hr>
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                    </ul>
                </div>

                <br>

            </div><!--div tab-pane-->

        </div><!--div tab-content-->
    </form>
<!--</div>--> <!--div container-->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$('.btnProximo').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});

$('.btnAnterior').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#avaliacao')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
<?php
$sql = "SELECT * FROM perguntas p ";
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
    while ($dados = $query->fetch_array()) {
?>
                pergunta_<?php echo $dados['id'] ;?>: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha uma nota'
                        }
                    }
                },
<?php
    }
  }
?>
                observacao: {
                    validators: {
                        stringLength: {
                            max: 500,
                            message: 'A observação deve ter no máximo 500 caracteres'
                        }
                    }
                }
            }
        }).on('status.field.bv', function(e, data) {
<?php
$sql = "SELECT * FROM perguntas p ";
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
    while ($dados = $query->fetch_array()) {
?>
            if (data.field === 'pergunta_<?php echo $dados['id'] ;?>') {
                // Remove the success class from the container
                data.element.closest('.form-group').removeClass('has-success');

                // Hide the tick icon
                data.element.data('bv.icon').hide();
            }
<?php
    }
  }
?>
        });

    $('#form_parecer_final')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                parecer_estado: {
                  validators: {
                    notEmpty: {
                      message: 'Preencha o estado'
                    }
                  }
                },
                parecer_observacao: {
                  validators: {
                    notEmpty: {
                      message: 'Preencha a observação'
                    }
                  }
                }
            }
        });
});
</script>

<?php
    }
  }
}
?>
</div>
