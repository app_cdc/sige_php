<?php
include '../vendor/autoload.php';
include "../includes/conecta.php";

if(isset($_POST['id']) && isset($_POST['estado']))
{
  $id = $_POST['id'];
  $estado = $_POST['estado'];
  $titulo = $_POST['titulo'];

  $sql = "UPDATE rel_final SET estado = $estado WHERE id=$id";
  $query = $mysqli->query($sql);

  if ($mysqli->query($sql) === TRUE) {
    $mysqli->commit();

    $html = '<b>Relatório Técnico Final do projeto "'.$titulo.'" foi enviado.</b>';

    // Create the Transport
    $transport = (new Swift_SmtpTransport('192.168.0.122', 25))
      ->setUsername('sistemas')
      ->setPassword('*Pro@SQ')
    ;

    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

    // Create a message
    $message = (new Swift_Message('Atualização sobre os projetos SIGE'))
      ->setFrom(['nao-responda@mailing.extecamp.unicamp.br' => 'SIGE'])
      ->setBody($html, 'text/html')
      ;

    $message->addTo('pec@unicamp.br','PEC');

    // Send the message
    $result = $mailer->send($message);

    echo TRUE;
  }else{
    $mysqli->rollback();
    echo FALSE;
  }
  exit;
}

?>