<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Criar nova ação de extensão"; // Título da Página
    //$TPL->ContentHead = "../includes/head_abas.php"; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
    function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
       $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
       $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
       $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
       return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);        }
}

//inicializa as variaveis que vão armazenar os dados da ação
$rel_final_id = '';
$rel_final_fim = '';
$rel_final_despesa_realizada = 0;
$rel_final_resultados_alcancados = '';
$rel_final_conclusao = '';
$acao_id ='';
$acao_titulo ='';
$acao_edital_id ='';
$acao_local ='';
$rel_final_inicio = '';

//se o botão salvar foi apertado o metodo POST não estara vazio
if( !empty($_POST) ){

    // pega os dados preenchidos no formulario
    $rel_final_id                   = $_POST['rel_final_id'];
    $acao_id                        = $_POST['acao_id'];
    $acao_edital_id                 = $_POST['edital_id'];
    $acao_local                     = $_POST['cidade'];
    $rel_final_inicio               = $_POST['data_inicio']; // date("Y-m-d"); // dia atual
    $rel_final_fim                  = $_POST['data_fim']; // date("Y-m-d"); // dia atual
    $acao_despesa_realizada         = convertCoin("EN",2,$_POST['despesa_realizada']);
    $acao_resultados_alcancados                 = $mysqli->real_escape_string($_POST['resultados_alcancados']);
    $acao_conclusao                 = $mysqli->real_escape_string($_POST['conclusao']);

    // se não tiver um id da ação então deve inserir um nova ação
    if(empty($rel_final_id)){

      $sql = "INSERT INTO rel_final (id_projeto,
                                    estado,
                                    dt_inicio,
                                    dt_fim,
                                    local,
                                    resultados_alcancados,
                                    conclusao,
                                    total_despesa_realizada) 
                                VALUES ($acao_id,
                                        1,
                                        '$rel_final_inicio',
                                        '$rel_final_fim',
                                        $acao_local,
                                        '$acao_resultados_alcancados',
                                        '$acao_conclusao',
                                        $acao_despesa_realizada)";
    }else{

       $sql = "UPDATE rel_final 
                SET id_projeto = $acao_id,
                    estado = 1,
                    dt_inicio = '$rel_final_inicio',
                    dt_fim = '$rel_final_fim',
                    local = $acao_local,
                    resultados_alcancados = '$acao_resultados_alcancados',
                    conclusao = '$acao_conclusao',
                    total_despesa_realizada = $acao_despesa_realizada
                WHERE id = $rel_final_id";
    }

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {
      
        if(empty($rel_final_id)){
            $rel_final_id = $mysqli->insert_id;
        }

        $sql_del_respostas = "DELETE FROM rel_final_item_respostas WHERE id_rel_final=$rel_final_id";
        if ($mysqli->query($sql_del_respostas) === FALSE) {
            $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_del_respostas;
        }else{

            $sql_perguntas = "SELECT rp.* 
                            FROM rel_final_perguntas rp,
                                edital_rel_final_perguntas erp
                            WHERE erp.id_pergunta = rp.id
                            AND erp.id_edital = $acao_edital_id";

            if ($result_perguntas = $mysqli->query($sql_perguntas)) {
                while ($dados_perguntas = $result_perguntas->fetch_array()) {

                    $pergunta_id = 'perguntas'.$dados_perguntas['id'];

                    if(isset($_POST[$pergunta_id])) {

                        foreach($_POST[$pergunta_id] as $key => $value) {
                            
                            $resposta = 1;

                            if (strpos($value, 'S') !== FALSE || strpos($value, 'N') !== FALSE) {
                              $resposta = (strpos($value, 'S') !== FALSE) ? 1 : 2;
                              $value = str_replace(array("S", "N"), "", $value);
                            }

                            $pergunta_item_detalhe = 'detalhe'.$value;
                            $detalhe = '';

                            if(isset($_POST[$pergunta_item_detalhe])) {
                                $detalhe = $_POST[$pergunta_item_detalhe];
                            }

                            $sql_item_resposta = "INSERT INTO rel_final_item_respostas (id_rel_final,
                                                                                        id_item,
                                                                                        resposta,
                                                                                        detalhe) 
                                                                                VALUES ($rel_final_id,
                                                                                $value,
                                                                                $resposta,
                                                                                '$detalhe')";

                            if ($mysqli->query($sql_item_resposta) === FALSE) {
                                $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_item_resposta;
                            }
                        }

                    }
                }
            }
        }
    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao salvar o relatório técnico final. '.$msg_erro;
    }else{
        $mysqli->commit();
        echo "Relatório técnico final salvo com sucesso!\n";
        echo "<script>window.location.href = 'index.php?id=$acao_id'</script>";
        exit;
    }
    $mysqli->close();

}

// se estiver alterando uma ação existente pega todos os dados dela
if (isset($_GET['acao_id'])) {
    
    $acao_id = $_GET['acao_id'];

    $link_voltar = "index.php?id=$acao_id";
    
    $sql = "SELECT * FROM rel_final WHERE id_projeto = ".$acao_id;
    if ($result = $mysqli->query($sql)) {
        while ($dados = $result->fetch_array()) {
            $rel_final_id = $dados['id'];
            $rel_final_local = $dados['local'];

            $rel_final_inicio = date_format(date_create($dados['dt_inicio']), 'Y-m-d');
            $rel_final_fim = date_format(date_create($dados['dt_fim']), 'Y-m-d');
            
            $rel_final_despesa_realizada = convertCoin("BR",2,$dados['total_despesa_realizada']);
            $rel_final_resultados_alcancados = $dados['resultados_alcancados'];
            $rel_final_conclusao = $dados['conclusao'];
            
        }
    }

    $sql = "SELECT * FROM acoes_extensao WHERE id = ".$acao_id;

    if ($result = $mysqli->query($sql)) {
        while ($dados = $result->fetch_array()) {

            $acao_titulo                    = $dados['titulo'];
            $acao_edital_id                 = $dados['edital'];
            $acao_local                     = $dados['local'];

        }


    }

    if (!empty($rel_final_local)) {
        $acao_local = $rel_final_local;
    }

    $sql_cidade_estado = "SELECT * FROM cidades WHERE cod_cidades = ".$acao_local;
    if ($result_cidade_estado = $mysqli->query($sql_cidade_estado)) {
        while ($dados_cidade_estado = $result_cidade_estado->fetch_array()) {
            $acao_local_estado = $dados_cidade_estado['estados_cod_estados'];
        }
    }

    $sql_valor_orcamento = "SELECT SUM(valor_solicitado) AS valor FROM orcamentos WHERE acao_extensao=".$acao_id;
    if ($result_valor_orcamento = $mysqli->query($sql_valor_orcamento)) {
        while ($dados_valor_orcamento = $result_valor_orcamento->fetch_array()) {
            $orcamento_valor_total = $dados_valor_orcamento["valor"];
        }
    }

}

?>
<script type="text/javascript">
function fetch_select(val)
{
   $.ajax({
     type: 'post',
     url: '../includes/fetch_data_cidade.php',
     data: {
       estado_cod:val
     },
     success: function (response) {
       document.getElementById("cidade").innerHTML=response; 
     }
   });
}
</script>
<script language="javascript">
    function enableDisable(bEnable, elementId, otherElementId)
    {
        if (bEnable) {
            document.getElementById(elementId).style.display = 'block';
        } else {
            document.getElementById(elementId).style.display = 'none'
        }
    }
    function disableRadio(bEnable, elementId, otherElementId)
    {
        if (!bEnable) {
            document.getElementById(elementId).style.display = 'block';
        } else {
            document.getElementById(elementId).style.display = 'none'
        }
    }
    function enableRadio(bEnable, elementId, otherElementId)
    {
        if (bEnable) {
            document.getElementById(elementId).style.display = 'block';
        } else {
            document.getElementById(elementId).style.display = 'none'
        }
    }
</script>

<div class="container">

    <h1>Informações para o Relatório Técnico Final</h1>
    <hr>

    <form role="form" enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
        <h4>Dados do projeto</h4>

        <input type="hidden" name="edital_id" value="<?php echo $acao_edital_id;?>" />
        <input type="hidden" name="acao_id" value="<?php echo $acao_id;?>" />
        <input type="hidden" name="rel_final_id" value="<?php echo $rel_final_id;?>" />

        <div class="form-group">
            <label class="col-sm-2 control-label" for="titulo">Título:</label>
            <div class="col-sm-10">
                <p class="show_acao_p"><?php echo $acao_titulo;?></p>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="control-label col-sm-2" for="tipo_extensao">Tipo de Extensão:</label>
            <div class="col-sm-10">
                <p class="show_acao_p">Projeto</p>
            </div>
        </div><!--div form-group-->

        <hr>

        <h4>Local da realização</h4>

        <div class="form-group">
            <label class="control-label col-sm-2" for="estado">Estado:</label>
            <div class="col-sm-3">
                <select class="form-control" name="estado" id="estado" onchange="fetch_select(this.value);">
                    <option>Selecione o estado</option>
                    <?php

                    if (!empty($acao_local_estado)) {
                        $sql = "SELECT * FROM cidades WHERE cod_cidades";
                        $query = $mysqli->query($sql);
                    }

                    $sql = "SELECT * FROM estados";
                    $query = $mysqli->query($sql);

                    if ($result = $mysqli->query($sql)) {
                        while ($dados = $query->fetch_array()) {
                            $estado_cod = $dados['cod_estados'];
                            $estado_nome = $dados['nome'];
                            $estado_selecionado = ($acao_local_estado == $estado_cod) ? "selected" : "" ;
                            echo "<option value='$estado_cod' $estado_selecionado>$estado_nome</option>";
                        }
                    }

                    ?>
                </select>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="control-label col-sm-2" for="cidade">Cidade:</label>
            <div class="col-sm-3">
                <select class="form-control" name="cidade" id="cidade" required>
                    <?php
                    
                    if (!empty($acao_local_estado)) {

                        $sql = "SELECT * FROM cidades WHERE estados_cod_estados=$acao_local_estado";
                        $query = $mysqli->query($sql);

                        if ($result = $mysqli->query($sql)) {
                            while ($dados = $query->fetch_array()) {
                                $cidade_selecionada = ($dados['cod_cidades'] == $acao_local) ? "selected" : "" ;
                                echo "<option value='".$dados['cod_cidades']."' $cidade_selecionada>".$dados['nome']."</option>";
                            }
                        }
                    }

                    ?>
                </select>
            </div>
        </div><!--div form-group-->

        <hr>

        <h4>Periodo de realização</h4>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="data_inicio">Data de início:</label>
          <div class="col-sm-3">
              <input class="form-control" id="data_inicio" name="data_inicio" placeholder="DD/MM/YYY" type="date" value="<?php echo $rel_final_inicio;?>" />
          </div>
        </div><!--div form-group-->

        <div class="form-group">
          <label class="col-sm-2 control-label" for="data_fim">Data de término:</label>
          <div class="col-sm-3">
              <input class="form-control" id="data_fim" name="data_fim" placeholder="DD/MM/YYY" type="date" value="<?php echo $rel_final_fim;?>" />
          </div>
        </div><!--div form-group-->


        <?php
        $sql_perguntas = "SELECT rp.* 
                        from rel_final_perguntas rp,
                        edital_rel_final_perguntas erp
                        where erp.id_pergunta = rp.id
                        and erp.id_edital = $acao_edital_id";
                        
        if ($result_perguntas = $mysqli->query($sql_perguntas)) {
          while ($dados_perguntas = $result_perguntas->fetch_array()) {
            $pergunta_id = $dados_perguntas['id'];
            $pergunta_desc = $dados_perguntas['descricao'];

        ?>
            <hr>
            <h4><?php echo $pergunta_desc;?></h4>
            <div class="form-group">
                <span id="alertPergunta<?php echo $pergunta_id;?>Icon"></span>
                <div class="col-sm-12">
                    <?php
                    $sql_perguntas_itens = "SELECT * FROM rel_final_pergunta_itens WHERE id_pergunta = $pergunta_id";

                    if ($result_perguntas_itens = $mysqli->query($sql_perguntas_itens)) {
                      while ($dados_perguntas_itens = $result_perguntas_itens->fetch_array()) {
                        $pergunta_item_id = $dados_perguntas_itens['id'];
                        $pergunta_item_desc = $dados_perguntas_itens['descricao'];
                        $pergunta_item_detalhe = $dados_perguntas_itens['detalhe'];

                        $sql_item_resposta = "SELECT * FROM rel_final_item_respostas WHERE id_item = $pergunta_item_id AND id_rel_final = $rel_final_id";
        
                        $resposta_item_marcado = "";
                        $resposta_item_marcado_sim = "";
                        $resposta_item_marcado_nao = "";
                        $resposta_item_display = "none";
                        $resposta_item_detalhe = "";
                        
                        if ($pergunta_item_desc !== 'SIM/NÃO') {

                          if ($result_item_resposta = $mysqli->query($sql_item_resposta)) {
                              while ($dados_item_resposta = $result_item_resposta->fetch_array()) {
                                  $resposta_item_marcado = ($dados_item_resposta['resposta'] == 1) ? "checked" : "" ;
                                  $resposta_item_display = ($dados_item_resposta['resposta'] == 1) ? "block" : "none" ;
                                  $resposta_item_detalhe = $dados_item_resposta['detalhe'];
                              }
                          }else{
                              $resposta_item_display = "none";
                          }

                          $checkbox_itens = "<label class='radio-inline'><input type='checkbox' ";
                          $pergunta_item_numero = "'item_detalhe_".$pergunta_item_id."'";
                          $pergunta_item_numero_requerido = "'detalhe".$pergunta_item_id."'";
                          $checkbox_itens .= ($pergunta_item_detalhe == 1) ? 'onclick="enableDisable(this.checked,'.$pergunta_item_numero.','.$pergunta_item_numero_requerido.')" '  : "";
                          $checkbox_itens .= "name='perguntas".$pergunta_id."[]' id='p".$pergunta_id."i".$pergunta_item_id."' value='".$pergunta_item_id."' ".$resposta_item_marcado."> ".$pergunta_item_desc."</label>";
                          $checkbox_itens .= ($pergunta_item_detalhe == 1) ? "
                                  <div id='item_detalhe_".$pergunta_item_id."' style='display: ".$resposta_item_display.";'>
                                      <br>
                                      <label class='col-sm-2 control-label' for='detalhe".$pergunta_item_id."'>Especifique: </label>
                                      <div class='col-sm-10'>
                                          <input class='form-control' name='detalhe".$pergunta_item_id."' id='detalhe".$pergunta_item_id."' size='50' maxlength='100' value='".$resposta_item_detalhe."' />
                                      </div>
                                  </div>
                                  " : '';
                          $checkbox_itens .= "<br>";
                          echo $checkbox_itens;

                        } else {

                          $resposta_item_marcado_nao = "checked";
                          $resposta_item_display = "block";

                          if ($result_item_resposta = $mysqli->query($sql_item_resposta)) {
                              while ($dados_item_resposta = $result_item_resposta->fetch_array()) {
                                  $resposta_item_marcado_sim = ($dados_item_resposta['resposta'] == 1) ? "checked" : "" ;
                                  $resposta_item_marcado_nao = ($dados_item_resposta['resposta'] == 2) ? "checked" : "" ;
                                  $resposta_item_display = ($dados_item_resposta['resposta'] == 2) ? "block" : "none" ;
                                  $resposta_item_detalhe = $dados_item_resposta['detalhe'];
                              }
                          }

                          $pergunta_item_numero = "'item_detalhe_".$pergunta_item_id."'";
                          $pergunta_item_numero_requerido = "'detalhe".$pergunta_item_id."'";

                          $radio_itens = "<label class='radio-inline'><input type='radio' ";
                          $radio_itens .= ($pergunta_item_detalhe == 1) ? 'onclick="disableRadio(this.checked,'.$pergunta_item_numero.','.$pergunta_item_numero_requerido.')" '  : "";
                          $radio_itens .= "name='perguntas".$pergunta_id."[]' id='p".$pergunta_id."i".$pergunta_item_id."s' value='".$pergunta_item_id."S' ".$resposta_item_marcado_sim."> Sim</label>";
                          
                          $radio_itens .= "<label class='radio-inline'><input type='radio' ";
                          $radio_itens .= ($pergunta_item_detalhe == 1) ? 'onclick="enableRadio(this.checked,'.$pergunta_item_numero.','.$pergunta_item_numero_requerido.')" '  : "";
                          $radio_itens .= "name='perguntas".$pergunta_id."[]' id='p".$pergunta_id."i".$pergunta_item_id."n' value='".$pergunta_item_id."N' ".$resposta_item_marcado_nao."> Não</label>";

                          $radio_itens .= ($pergunta_item_detalhe == 1) ? "
                                  <div id='item_detalhe_".$pergunta_item_id."' style='display: ".$resposta_item_display.";'>
                                      <br>
                                      <label class='col-sm-2 control-label' for='detalhe".$pergunta_item_id."'>Especifique: </label>
                                      <div class='col-sm-10'>
                                          <input class='form-control' name='detalhe".$pergunta_item_id."' id='detalhe".$pergunta_item_id."' size='50' maxlength='100' value='".$resposta_item_detalhe."' />
                                      </div>
                                  </div>
                                  " : '';
                          $radio_itens .= "<br>";

                          echo $radio_itens;
                        }
                      }
                    }
                    ?>
                    <br>
                </div>
                <div class="help-block" id="alertPergunta<?php echo $pergunta_id;?>Message"></div>
            </div><!--div form-group-->
        <?php
          }
        }
        ?>

        <hr>
        <h4>Resultados alcançados (previstos e não previstos): (máx. 3000 caracteres)</h4>

        <div class="form-group">
            <div class="col-sm-12">
                <textarea class="form-control" rows="7" name="resultados_alcancados" maxlength="3000" required placeholder=""><?php echo $rel_final_resultados_alcancados;?></textarea>
            </div>
        </div><!--div form-group-->

        <hr>
        <h4>Descrição das despesas</h4>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="titulo">Despesas orçadas:</label>
            <div class="col-sm-10">
                <p class="show_acao_p"><?php echo convertCoin("BR",2,$orcamento_valor_total);?></p>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="col-sm-2 control-label" for="despesa_realizada">Despesas efetivamente realizadas:</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="despesa_realizada" id="despesa_realizada" required value="<?php echo convertCoin("BR",2,$rel_final_despesa_realizada);?>">
              <script type="text/javascript">$("#despesa_realizada").maskMoney({prefix:'R$ ', showSymbol:true, thousands:'.', decimal:',', affixesStay: false});</script>
            </div>
        </div><!--div form-group-->

        <hr>
        <h4>Conclusões finais sobre o projeto e sua realização: (máx. 3000 caracteres)</h4>

        <div class="form-group">
            <div class="col-sm-12">
                <textarea class="form-control" rows="7" name="conclusao" maxlength="3000" required placeholder=""><?php echo $rel_final_conclusao;?></textarea>
            </div>
        </div><!--div form-group-->

        <hr>

        <div>
            <ul class="pager">
                <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='<?php echo $link_voltar?>';">< Voltar ao Principal</button></li>
                <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button></li>
            </ul>
        </div>
    </form>
</div><!--div container-->
  
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .on('init.field.bv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The field element
<?php
$sql_perguntas = "SELECT rp.* 
                from rel_final_perguntas rp,
                edital_rel_final_perguntas erp
                where erp.id_pergunta = rp.id
                and erp.id_edital = $acao_edital_id";
                
if ($result_perguntas = $mysqli->query($sql_perguntas)) {
  while ($dados_perguntas = $result_perguntas->fetch_array()) {
    $pergunta_id = $dados_perguntas['id'];
    $pergunta_desc = $dados_perguntas['descricao'];

?>
            if (data.field === 'perguntas<?php echo $pergunta_id;?>[]') {
                var $icon = data.element.data('bv.icon');
                $icon.appendTo('#alertPergunta<?php echo $pergunta_id;?>Icon');
            }
<?php
  }
}
?>
        })
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                cidade: {
                    validators: {
                        notEmpty: {
                            message: 'Selecione o estado e depois a cidade'
                        }
                    }
                },
                data_inicio: {
                    validators: {
                        notEmpty: {
                            message: 'Selecione a data de início'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            max: 'data_fim',
                            message: 'Data de início inválida'
                        }
                    }
                },
                data_fim: {
                    validators: {
                        notEmpty: {
                            message: 'Selecione a data de término'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            min: 'data_inicio',
                            message: 'Data de fim inválida'
                        }
                    }
                },
<?php
$sql_perguntas = "SELECT rp.* 
                from rel_final_perguntas rp,
                edital_rel_final_perguntas erp
                where erp.id_pergunta = rp.id
                and erp.id_edital = $acao_edital_id";
                
if ($result_perguntas = $mysqli->query($sql_perguntas)) {
    while ($dados_perguntas = $result_perguntas->fetch_array()) {
        $pergunta_id = $dados_perguntas['id'];

?>
                'perguntas<?php echo $pergunta_id;?>[]': {
                    validators: {
                        notEmpty: {
                            message: 'Selecione pelo menos um item'
                        }
                    }
                },
<?php
    }
}
?>
<?php
$sql_itens = "SELECT rpi.* 
                from rel_final_perguntas rp,
                edital_rel_final_perguntas erp,
                rel_final_pergunta_itens rpi
                where rpi.id_pergunta = rp.id
                and rpi.detalhe = 1
                     and erp.id_pergunta = rp.id
                and erp.id_edital = $acao_edital_id";
                
if ($result_itens = $mysqli->query($sql_itens)) {
    while ($dados_itens = $result_itens->fetch_array()) {
        $item_id = $dados_itens['id'];
        $pergunta_id = $dados_itens['id_pergunta'];

?>
                detalhe<?php echo $item_id;?>: {
                    validators: {
                        callback: {
                            message: 'Especifique o item selecionado',
                            callback: function(value, validator, $field){
                              <?php if ($dados_itens['descricao'] !== 'SIM/NÃO') { ?>
                                var marcado = $('#p<?php echo $pergunta_id;?>i<?php echo $item_id;?>').is(":checked");
                                return (!marcado) ? true : (value !== '');
                              <?php } else { ?>
                                return ($('input[name="perguntas<?php echo $pergunta_id;?>[]"]:checked', '#cadastro').val().indexOf("S") > -1) ? true : (value !== '');
                              <?php } ?>
                            }
                        }
                    }
                },
<?php
    }
}
?>
                despesa_realizada: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a despesa realizada'
                        }
                    }
                },
                resultados_alcancados: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha os resultados alcançados'
                        },
                        stringLength: {
                            max: 3000,
                            message: 'Os resultados alcançados devem ter no máximo 3000 caracteres'
                        }
                    }
                },
                conclusao: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a conclusão'
                        },
                        stringLength: {
                            max: 3000,
                            message: 'A conclusão deve ter no máximo 3000 caracteres'
                        }
                    }
                }
            }
        })
        .on('success.field.bv', function(e, data) {
            if (data.field === 'data_inicio' && !data.bv.isValidField('data_fim')) {
                // We need to revalidate the end date
                data.bv.revalidateField('data_fim');
            }

            if (data.field === 'data_fim' && !data.bv.isValidField('data_inicio')) {
                // We need to revalidate the start date
                data.bv.revalidateField('data_inicio');
            }

<?php
$sql_itens_com_detalhe = "SELECT rpi.* 
                    FROM rel_final_pergunta_itens rpi,
                        rel_final_perguntas rp,
                        edital_rel_final_perguntas erp
                    WHERE rpi.id_pergunta = rp.id
                    AND rp.id = erp.id_pergunta
                    AND erp.id_edital = $acao_edital_id
                    AND rpi.detalhe = 1";
                
if ($result_itens_com_detalhe = $mysqli->query($sql_itens_com_detalhe)) {
    while ($dados_itens_com_detalhe = $result_itens_com_detalhe->fetch_array()) {
        $item_com_detalhe_id = $dados_itens_com_detalhe['id'];
        $item_com_detalhe_id_pergunta = $dados_itens_com_detalhe['id_pergunta'];

?>
            if (data.field === 'detalhe<?php echo $item_com_detalhe_id;?>') {

<?php if ($dados_itens['descricao'] !== 'SIM/NÃO') { ?>
                var itens = $('#cadastro').find('[name="perguntas<?php echo $item_com_detalhe_id_pergunta;?>"]');
                //value checked
                for (var i = 0; i < itens.length; i++) {
                    if (itens[i].value == <?php echo $item_com_detalhe_id;?> && itens[i].checked == false) {
                        // Remove the success class from the container
                        data.element.closest('.form-group').removeClass('has-success');

                        // Hide the tick icon
                        data.element.data('bv.icon').hide();
                    }
                }
<?php } else { ?>
                data.bv.revalidateField('detalhe<?php echo $item_com_detalhe_id;?>');
<?php }?>
            }
<?php
    }
}
?>

        });

    $('#despesa_realizada').change(function() {
        $('#cadastro').data('bootstrapValidator').updateStatus('despesa_realizada', 'NOT_VALIDATED').validateField('despesa_realizada');
    });
});

$(document).ready(function() {
<?php
$sql_itens = "SELECT rpi.* 
                from rel_final_perguntas rp,
                edital_rel_final_perguntas erp,
                rel_final_pergunta_itens rpi
                where rpi.id_pergunta = rp.id
                and rpi.detalhe = 1
                     and erp.id_pergunta = rp.id
                and erp.id_edital = $acao_edital_id";
                
if ($result_itens = $mysqli->query($sql_itens)) {
    while ($dados_itens = $result_itens->fetch_array()) {
        $item_id = $dados_itens['id'];
        $pergunta_id = $dados_itens['id_pergunta'];

?>
<?php if ($dados_itens['descricao'] !== 'SIM/NÃO') { ?>
    $('#p<?php echo $pergunta_id;?>i<?php echo $item_id;?>').change(function() {
      $('#cadastro').data('bootstrapValidator').resetField('detalhe<?php echo $item_id;?>',true).validateField('detalhe<?php echo $item_id;?>');
    });
<?php } else { ?>
    $('input[name="perguntas<?php echo $pergunta_id;?>[]"]', '#cadastro').change(function() {
      $('#cadastro').data('bootstrapValidator').resetField('detalhe<?php echo $item_id;?>',true).validateField('detalhe<?php echo $item_id;?>');
    });
<?php } ?>
<?php
    }
}
?> 
});

</script>
