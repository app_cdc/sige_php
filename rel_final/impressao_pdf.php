<?php
include "../includes/conecta.php";
include('../vendor/mpdf/mpdf/mpdf.php');

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
     $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
     $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
     $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
     return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}

if (isset($_GET['id'])) {

	$html = "";

	$id_acao = "";

	$titulo = "";

	$acao_id = $_GET['id'];

	$sql = "SELECT ae.titulo,
					e.ano_base,
					e.id as edital_id,
					u.nome,
					r.* 
           FROM acoes_extensao ae,
                rel_final r,
                editais e,
                usuarios u 
          WHERE ae.id = r.id_projeto
          	AND e.id = ae.edital
          	AND u.id = ae.coordenador
            AND ae.id = ".$acao_id;

	if ($result = $mysqli->query($sql)) {
		while ($dados = $result->fetch_array()) {

		$id_acao = $dados['id_projeto'];
		$id_rel_final = $dados['id'];
		$titulo = $dados['titulo'];
		$edital = $dados['edital_id'];

		$data_fim = new DateTime( $dados['dt_fim'] );
		$data_inicio = new DateTime( $dados['dt_inicio'] );

		$intervalo = $data_fim->diff( $data_inicio );
		$periodo_meses = $intervalo->m + ($intervalo->y * 12);


		$html .= "<style type='text/css'>
					body{
						font-family: Arial;
					}
					fieldset{
						width: 700px;
						margin: -1px auto;
						color: #444;
						border: 1px solid #ccc;
						padding: 15px;
						padding-bottom: 5px;
						padding-top: 5px;
						font-size: 12px;
					}

					h1{
						text-align: center;
					}
					body{
						display: inline;
					}

					p.sub-titulo{
						color: #444;
						font-size: 16px;
						font-weight: bold;
						display: inline;
					}

					.direita{
						text-align: right;
					}

					.center{
						text-align: center;
					}
					.objetivos {
							width:718px;
							margin: -10px -16px -6px -16px; 
							border:1px solid #C0C0C0;
							border-collapse:collapse;
							padding:5px;
							color: #444;
							font-size: 12px;
						}
						.objetivos th {
							border:1px solid #C0C0C0;
							padding:5px;
							background:#555;
							color: #ffffff;
						}
						.objetivos td {
							font-size: 12px;
							border:1px solid #C0C0C0;
							padding:7px;
						}
					.orcamento {
							width:718px;
							margin: -10px -16px -6px -16px;
							border:1px solid #C0C0C0;
							border-collapse:collapse;
							padding:5px;
							color: #444;
							font-size: 12px;
						}
						.orcamento th {
							border:1px solid #C0C0C0;
							padding:5px;
							text-align: left;
						}
						.orcamento td {
							border:1px solid #C0C0C0;
							padding:5px;
						}

					</style>";

			$sql_edital = "SELECT * FROM editais e WHERE e.id = $edital";

			if ($result_edital = $mysqli->query($sql_edital)) {
				while ($dados_edital = $result_edital->fetch_array()) {
					$html .= "<fieldset><img src='../style/images/".$dados_edital['cabecalho_relatorio']."' style='width: 100%'></fieldset>";
				}
			}

			$html .= "<fieldset><b>Coordenador do Projeto:</b> ".$dados["nome"]."</fieldset>";
			$html .= "<fieldset><b>Título do Projeto:</b> ".$dados['titulo']."</fieldset>";
			$html .= "<fieldset><b>Período de Realização:</b> ".$periodo_meses." meses</fieldset>";

			$sql_local = "SELECT c.nome AS cidade, 
								 e.nome AS estado 
						  FROM cidades c, estados e 
						  WHERE c.estados_cod_estados = e.cod_estados 
						  AND c.cod_cidades = ".$dados['local'];

			if ($result_local = $mysqli->query($sql_local)) {
				while ($dados_local = $result_local->fetch_array()) {
					$html .= "<fieldset><b>Local da Realização:</b> ".$dados_local['cidade']." - ".$dados_local['estado']."</fieldset>";
				}
			}

		    $sql_participante_categoria = "SELECT * 
		                                    FROM participante_categorias pc
		                                  ORDER BY pc.id";

		    if ($result_participante_categoria = $mysqli->query($sql_participante_categoria)) {
		      while ($dados_participante_categoria = $result_participante_categoria->fetch_array()) {


			$html .= "<fieldset><b>Participantes no Projeto: ".$dados_participante_categoria['descricao']."</b>";
			$html .= "<br>&nbsp;<br>";

			$html .= "";
			$html .= 	"<table class='objetivos'>";
			$html .= 	"<thead>";
			$html .= 		"<tr>";
			$html .= 			"<th>Nome</th>";
					if ($dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
			$html .= 			"<th>RA</th>";
					}
					if ($dados_participante_categoria['id'] == 1 || $dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
			$html .= 			"<th>Unidade</th>";
			$html .= 			"<th>Instituição</th>";
					}
			$html .= 			"<th>Carga Semanal</th>";
			$html .= 			"<th>Carga Total</th>";
			$html .= 		"</tr>";
			$html .= 	"</thead>";
			$html .= 	"<tbody>";
			        $sql_participantes = "SELECT * 
			                            FROM participantes p
			                            WHERE p.id_rel_final = $id_rel_final
			                            AND p.id_categoria = ".$dados_participante_categoria['id'];
					if ($result_participantes = $mysqli->query($sql_participantes)) {
						while ($dados_participantes = $result_participantes->fetch_array()) {


			$html .= 		"<tr>";
			$html .= 			"<td>".$dados_participantes['nome']."</td>";
							if ($dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
			$html .= 			"<td>".$dados_participantes['ra']."</td>";
							}
							if ($dados_participante_categoria['id'] == 1 || $dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
			$html .= 			"<td>".$dados_participantes['unidade']."</td>";
			$html .= 			"<td>".$dados_participantes['instituicao']."</td>";
							}
			$html .= 			"<td>".$dados_participantes['carga_horaria_semanal']."</td>";
			$html .= 			"<td>".$dados_participantes['carga_horaria_total']."</td>";
			$html .= 		"</tr>";
						}
					}
			$html .= 	"</tbody>";
			$html .= 	"</table>";
			$html .= "</fieldset>";
				}
			}
			
//inicio das perguntas
			$sql_perguntas = "SELECT rp.*
								FROM rel_final_perguntas rp,
									edital_rel_final_perguntas erp
								WHERE rp.id = erp.id_pergunta
								AND erp.id_edital = ".$dados['edital_id']."
								ORDER BY rp.id";
			
			if ($result_perguntas = $mysqli->query($sql_perguntas)) {
				while ($dados_perguntas = $result_perguntas->fetch_array()) {

					$html .= "<fieldset><b>".$dados_perguntas['descricao']." </b>";

					$sql_respostas = "SELECT rpi.descricao,
											rir.detalhe
										FROM rel_final_pergunta_itens rpi,
											rel_final_item_respostas rir
										WHERE rpi.id = rir.id_item
										AND rpi.id_pergunta = ".$dados_perguntas['id']."
										AND rir.id_rel_final = $id_rel_final
										ORDER BY rpi.id";

					if ($result_respostas = $mysqli->query($sql_respostas)) {
						while ($dados_respostas = $result_respostas->fetch_array()) {
							if ($dados_respostas['descricao'] !== "SIM/NÃO" ) {
								$html .= "<br>".$dados_respostas['descricao'].". ";
								if ($dados_respostas['detalhe'] != "") {
									$html .= "<b>Especifique: </b>".$dados_respostas['detalhe'];
								}
							} else {
								if ($dados_respostas['detalhe'] != "") {
									$html .= "<br>Não. <b>Especifique: </b>".$dados_respostas['detalhe'];
								} else {
									$html .= "<br>Sim.";
								}
							}
						}
					}
					$html .= "</fieldset>";
				}
			}
//fim das perguntas
			if (!empty($dados["resultados_alcancados"])) {
				$html .= "<fieldset><b>Resultados alcançados (previstos e não previstos):</b><br>".$dados["resultados_alcancados"]."</fieldset>";
			}

			$html .= "<fieldset><b>Descrição das despesas: </b><br>&nbsp;<br>";
			$html .= 	"<table class='objetivos'>";
			$html .= 	"<tbody>";

			$valor_solicitado = 0;
			$valor_realizado = $dados['total_despesa_realizada'];
			$valor_saldo = 0;

			$sql_orcamento = "SELECT SUM(o.valor_solicitado) as valor_solicitado_total
								FROM orcamentos o
								WHERE o.acao_extensao = ".$dados['id_projeto'];
			$html .= 			"<td>".$dados_participantes['nome']."</td>";

			if ($result_orcamento = $mysqli->query($sql_orcamento)) {
				while ($dados_orcamento = $result_orcamento->fetch_array()) {
					$valor_solicitado = $dados_orcamento['valor_solicitado_total'];
					$html .= "<tr><td>Despesas Orçadas no Projeto:</td><td class='direita'>R$ ".convertCoin("BR",2,$valor_solicitado)."</td></tr>";
				}
			}
			$html .= 		"<tr><td>Despesas Efetivamente Realizadas:</td><td class='direita'>R$ ".convertCoin("BR",2,$valor_realizado)."</td></tr>";
			$valor_saldo = $valor_solicitado - $valor_realizado;
			$html .= 		"<tr><td>Saldo:</td><td class='direita'>R$ ".number_format($valor_saldo,2,",",".")."</td></tr>";
			$html .= 	"</tbody>";
			$html .= "</table>";
			$html .= "</fieldset>";

			$html .= "<fieldset><b>Conclusões finais sobre o projeto e sua realização:</b><br>".$dados["conclusao"]."</fieldset>";






		}
	}


$mpdf=new mPDF();

$mpdf->SetDisplayMode('fullpage');

        $mpdf->AddPage('P', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            5, // margin top
            20, // margin bottom
            10, // margin header
            10);
            $footer = "<table width=\"1000\">
                   <tr>
                   	<td tyle='font-size: 18px;' align=\"left\">Protocolo de inscrição nº: ".$id_acao. "</td>
                     <td style='font-size: 18px;' align=\"right\">Página. {PAGENO}</td>
                   </tr>
                 </table>";

$mpdf->SetHTMLFooter($footer);

$mpdf->WriteHTML($html);

$mpdf->Output($id_acao."_".$titulo.".pdf",'D');


//echo $html;
exit;

}
?>

