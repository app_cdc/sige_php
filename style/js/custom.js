$(function() {

    // Atribui evento e função para limpeza dos campos
    $('#usuario_nome').on('input', limpaCampos);

    // Dispara o Autocomplete a partir do segundo caracter
    $( "#usuario_nome" ).autocomplete({
	    minLength: 2,
	    source: function( request, response ) {
	        $.ajax({
	            url: "consulta.php",
	            dataType: "json",
	            data: {
	            	acao: 'autocomplete',
	                parametro: $('#usuario_nome').val()
	            },
	            success: function(data) {
	               response(data);
	            }
	        });
	    },
	    focus: function( event, ui ) {
	        $("#usuario_nome").val( ui.item.nome );
	        $("#usuario_matricula").val( ui.item.matricula );
	        return false;
	    },
	    select: function( event, ui ) {
	        $("#usuario_nome").val( ui.item.nome );
	        $("#usuario_matricula").val( ui.item.matricula );
	        return false;
	    }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a><b>Nome: </b>" + item.nome + "<b> Matricula: </b>" + item.matricula + " <b> Unidade: </b>" + item.local + "</a><br>" )
        .appendTo( ul );
    };

	// Função para limpar os campos caso a busca esteja vazia
	function limpaCampos(){
		var busca = $('#usuario_nome').val();

		if(busca == ""){
			$('#usuario_matricula').val('');
		}
	}
});