$(function() {

    // Atribui evento e função para limpeza dos campos
    $('#usuario_nome').on('input', limpaCampos);

    // Dispara o Autocomplete a partir do segundo caracter
    $( "#usuario_nome" ).autocomplete({
	    minLength: 2,
	    source: function( request, response ) {
	        $.ajax({
	            url: "consulta.php",
	            dataType: "json",
	            data: {
	            	acao: 'autocomplete',
	                parametro: $('#usuario_nome').val()
	            },
	            success: function(data) {
	               response(data);
	            }
	        });
	    },
	    focus: function( event, ui ) {
	        $("#usuario_nome").val( ui.item.nome );
	        carregarDados();
	        return false;
	    },
	    select: function( event, ui ) {
	        $("#usuario_nome").val( ui.item.nome );
	        return false;
	    }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a><b>Nome: </b>" + item.nome + "<br><b>Matricula: </b>" + item.matricula + "</a><br>" )
        .appendTo( ul );
    };

    // Função para carregar os dados da consulta nos respectivos campos
    function carregarDados(){
    	var usuario_nome = $('#usuario_nome').val();

    	if(usuario_nome != "" && usuario_nome.length >= 2){
    		$.ajax({
	            url: "consulta.php",
	            dataType: "json",	
	            data: {
	            	acao: 'consulta',
	                parametro: $('#usuario_nome').val()
	            },
	            success: function( data ) {
	               $('#usuario_matricula').val(data[0].matricula);
	            }
	        });
    	}
    }

    // Função para limpar os campos caso a usuario_nome esteja vazia
    function limpaCampos(){
       var usuario_nome = $('#usuario_nome').val();

       if(usuario_nome == ""){
	       $('#usuario_matricula').val('');
       }
    }
});