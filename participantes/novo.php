<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Inserir participante"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

?>
<script type="text/javascript">
function apagar_participante(participante_id,participante_acao_id)
{
  $.ajax({
    type: 'post',
    url: 'apagar.php',
    data: {
      id:participante_id
    },
    success: function (response) {
      if(response){
        location.replace("novo.php?acao_id="+participante_acao_id);
      }else{
        document.getElementById("msg").innerHTML="Erro ao apagar o participante";
      }
    }
  });
}
</script>

<div class="container">
<?php
//
$erro = FALSE;
$msg = "";
//
if( !empty($_POST) ){

    $participante_acao_id = $_POST['acao_id'];
    $participante_rel_final_id = $_POST['rel_final_id'];
    $participante_nome = $mysqli->real_escape_string($_POST['participante_nome']);
    $participante_categoria = $_POST['participante_categorias'];
    $participante_ra = $_POST['participante_ra'];
    $participante_unidade = $_POST['participante_unidade'];
    $participante_unidade = $_POST['participante_unidade'];
    $participante_instituicao = $_POST['participante_instituicao'];
    $participante_instituicao = $_POST['participante_instituicao'];
    $participante_carga_semanal = $_POST['participante_carga_semanal'];
    $participante_carga_total = $_POST['participante_carga_total'];



    $sql = "INSERT INTO participantes (id_rel_final,
                                        nome,
                                        id_categoria,
                                        ra,
                                        unidade,
                                        instituicao,
                                        carga_horaria_semanal,
                                        carga_horaria_total) 
                              VALUES ($participante_rel_final_id,
                                      '$participante_nome',
                                      $participante_categoria,
                                      '$participante_ra',
                                      '$participante_unidade',
                                      '$participante_instituicao',
                                      $participante_carga_semanal,
                                      $participante_carga_total)";
    

    if ($mysqli->query($sql) === TRUE) {

      $msg = "Participante salvo com sucesso";
      $erro = FALSE;

    } else {
        $msg .= "Erro ao inserir o participante. Error: " . $mysqli->error . "<br>" . $sql;
        $erro = TRUE;
    }

    if($erro){
        $mysqli->rollback();
    }else{
        $mysqli->commit();
    }

    echo "<div>$msg</div>";

}

if(!$erro){

  if (isset($_GET['acao_id'])) {

    $apagar_link = "";

    $acao_id = $_GET['acao_id'];
    $rel_final_id = '';
      
    $sql = "SELECT * FROM rel_final WHERE id_projeto=".$acao_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {

        $rel_final_id = $dados['id'];
      }
    }

?>
<h3>Participantes:</h3>
<hr>
<form class="form-horizontal" role="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
  
  <input type="hidden" name="acao_id" value="<?php echo $acao_id ?>">
  <input type="hidden" name="rel_final_id" value="<?php echo $rel_final_id ?>">

  <div class="form-group">
    <label class="col-sm-2 control-label" for="participante_nome">Nome:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="participante_nome" id="participante_nome" required>
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <label class="control-label col-sm-2" for="participante_categorias">Categoria:</label>
    <div class="col-sm-10">
      <select class="form-control" name="participante_categorias" id="participante_categorias" required>
        <option value="">Selecione a categoria</option>
        <?php
            $sql = "SELECT * FROM participante_categorias";
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
              while ($dados = $query->fetch_array()) {
                $categoria_id = $dados['id'];
                $categoria_descricao = $dados['descricao'];
               echo "<option value='$categoria_id'>$categoria_descricao</option>";
              }
            }
        ?>
      </select>
    </div>
  </div><!--div form-group-->

  <div class="form-group discente">
    <label class="col-sm-2 control-label" for="participante_ra">RA:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="participante_ra" id="participante_ra">
    </div>
  </div><!--div form-group-->

  <div class="form-group discente docente">
    <label class="col-sm-2 control-label" for="participante_unidade">Unidade:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="participante_unidade" id="participante_unidade">
    </div>
  </div><!--div form-group-->

  <div class="form-group discente docente">
    <label class="col-sm-2 control-label" for="participante_instituicao">Instituição:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="participante_instituicao" id="participante_instituicao">
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <label class="col-sm-2 control-label" for="participante_carga_semanal">Carga Horária Semanal:</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" name="participante_carga_semanal" id="participante_carga_semanal" required>
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <label class="col-sm-2 control-label" for="participante_carga_total">Carga Horária Total:</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" name="participante_carga_total" id="participante_carga_total" required>
    </div>
  </div><!--div form-group-->

  <div>
    <ul class="pager">
        <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-plus"></span> Adicionar</button></li>
    </ul>
  </div>

</form>

<hr>

<?php
//
    $sql_participante_categoria = "SELECT * 
                                    FROM participante_categorias pc
                                  ORDER BY pc.id";

    if ($result_participante_categoria = $mysqli->query($sql_participante_categoria)) {
      while ($dados_participante_categoria = $result_participante_categoria->fetch_array()) {

?>
<h3><?php echo $dados_participante_categoria['descricao'];?></h3>
<table class="table table-striped">

    <thead>
      <tr class="tabela_cabecalho">
        <th>Operação</th>
        <th>Nome</th>
        <?php if ($dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {?>
        <th>RA</th>
        <?php }
              if ($dados_participante_categoria['id'] == 1 || $dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
        ?>
        <th>Unidade</th>
        <th>Instituição</th>
        <?php } ?>
        <th>Carga Semanal</th>
        <th>Carga Total</th>
      </tr>
    </thead>

    <tbody>     



<?php

        $sql_participante = "SELECT * 
                            FROM participantes p
                            WHERE p.id_rel_final = $rel_final_id
                            AND p.id_categoria = ".$dados_participante_categoria['id'];

        if ($result_participante = $mysqli->query($sql_participante)) {
          while ($dados_participante = $result_participante->fetch_array()) {


        
?>
    <tr>

      <td>
        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal" data-participante="<?php echo $dados_participante['id']; ?>" data-acao-id="<?php echo $acao_id; ?>">
          <span class="glyphicon glyphicon-trash"></span>
        </button>
      </td>
      <td><?php echo $dados_participante['nome'] ?></td>
      <?php if ($dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {?>
      <td><?php echo $dados_participante['ra'] ?></td>
      <?php }
            if ($dados_participante_categoria['id'] == 1 || $dados_participante_categoria['id'] == 2 || $dados_participante_categoria['id'] == 3) {
      ?>
      <td><?php echo $dados_participante['unidade'] ?></td>
      <td><?php echo $dados_participante['instituicao'] ?></td>
      <?php } ?>
      <td><?php echo $dados_participante['carga_horaria_semanal'] ?></td>
      <td><?php echo $dados_participante['carga_horaria_total'] ?></td>
      
    </tr>
<?php 
          }
        }
?>
  </tbody>
</table>
<hr>
<?php
      }
    }
?>
  <div>
    <ul class="pager">
        <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='../rel_final/index.php?id=<?php echo $acao_id?>';">< Voltar ao Principal</button></li>
    </ul>
  </div>

<?php
  }
}
?>

</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>Deseja apagar mesmo?</p>
                <p class="text-warning"><small>Irá apagar o participante</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnApagar">Apagar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                participante_nome: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o nome'
                        }
                    }
                },
                participante_categorias: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a categoria'
                        }
                    }
                },
                participante_carga_semanal: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a carga horária semanal'
                        },
                        lessThan: {
                            value: 'participante_carga_total',
                            message: 'A carga horária semanal deve ser menor que a total'
                        }
                    }
                },
                participante_carga_total: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a carga horária total'
                        },
                        greaterThan: {
                            value: 'participante_carga_semanal',
                            message: 'A carga horária total deve ser maior que a semanal'
                        }
                    }
                },
                participante_ra: {
                    validators: {
                        callback: {
                            message: 'Preencha a RA',
                            callback: function(value, validator, $field){
                                var discente = $('#cadastro').find('[name="participante_categorias"] option:checked').filter(":contains('Discente')").length;
                                return (discente === 0) ? true : (value !== '');
                            }
                        }
                    }
                },
                participante_unidade: {
                    validators: {
                        callback: {
                            message: 'Preencha a Unidade',
                            callback: function(value, validator, $field){
                                var docente = $('#cadastro').find('[name="participante_categorias"] option:checked').filter(":contains('Docente')").length;
                                var discente = $('#cadastro').find('[name="participante_categorias"] option:checked').filter(":contains('Discente')").length;
                                if (docente > discente) {
                                  return (docente === 0) ? true : (value !== '');
                                }
                                if (docente < discente){
                                  return (discente === 0) ? true : (value !== '');
                                }
                                return true;
                            }
                        }
                    }
                },
                participante_instituicao: {
                    validators: {
                        callback: {
                            message: 'Preencha a Instituição',
                            callback: function(value, validator, $field){
                                var docente = $('#cadastro').find('[name="participante_categorias"] option:checked').filter(":contains('Docente')").length;
                                var discente = $('#cadastro').find('[name="participante_categorias"] option:checked').filter(":contains('Discente')").length;
                                if (docente > discente) {
                                  return (docente === 0) ? true : (value !== '');
                                }
                                if (docente < discente){
                                  return (discente === 0) ? true : (value !== '');
                                }
                                return true;
                            }
                        }
                    }
                }
            }
        }).on('success.field.bv', function(e, data) {
            if (data.field === 'participante_carga_semanal' && !data.bv.isValidField('participante_carga_total')) {
                // We need to revalidate the end date
                data.bv.revalidateField('participante_carga_total');
            }          
            if (data.field === 'participante_carga_total' && !data.bv.isValidField('participante_carga_semanal')) {
                // We need to revalidate the end date
                data.bv.revalidateField('participante_carga_semanal');
            }          
        });

    

    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var participante_id = button.data('participante'); // Extract info from data-* attributes
      var participante_acao_id = button.data('acaoId'); // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('#btnApagar').click(function() {
        apagar_participante(participante_id,participante_acao_id);
      });
    });

});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

$(document).ready(function() {
    //
    $('.discente').hide(); 
    $('#participante_categorias').change(function() {
      if($('#participante_categorias option:selected').filter(":contains('Docente')").length === 1) {
        $('.discente').hide(); 
        $('.docente').show(); 
      } else if($('#participante_categorias option:selected').filter(":contains('Discente')").length === 1) {
        $('.docente').hide(); 
        $('.discente').show(); 
      } else {
        $('.discente').hide(); 
      }
      $('#cadastro').data('bootstrapValidator').resetField('participante_ra',true).validateField('participante_ra');
      $('#cadastro').data('bootstrapValidator').resetField('participante_unidade',true).validateField('participante_unidade');
      $('#cadastro').data('bootstrapValidator').resetField('participante_instituicao',true).validateField('participante_instituicao');
    });
});

</script>
