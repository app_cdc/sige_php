<?php
$liberado = TRUE;
if (!$liberado){
    echo "<script language='JavaScript'>window.location='index_em_breve.php';</script>";
}else{

// TEMPLATE
require_once('lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Home"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "layout.php";
    exit;
}
// END TEMPLATE
?>

<div class="container">

	<div class="jumbotron">
    <h1>Bem vindo ao SIGE!</h1>
    <p>Sistema de Informação Gestão de Extensão</p>
  	</div>
  	<h3>Projetos de Extensão Comunitária</h3>
  	<p>A Pró-Reitoria de Extensão e Cultura tem como missão coordenar, fomentar, estimular e produzir ações de Extensão e de Cultura pela integração dialógica, interativa e pró ativa com a sociedade, difundindo e adquirindo conhecimento por meio da comunidade universitária. “Extensão Comunitária” é a atividade acadêmica de Extensão Universitária destinada a atender a sociedade civil em comunidade externa à UNICAMP em segmentos da população ou em grupos específicos (minorias, grupos étnicos, portadores de necessidades especiais, faixas etárias, etc.), promovendo ação de natureza social, artística, cultural, desportiva ou educativa. A ação de "Extensão Comunitária" deve estar diretamente vinculada a uma atividade acadêmica regular de ensino e de pesquisa; deve ser dirigida por um docente ou pesquisador da UNICAMP; deve ter, necessariamente, a participação de alunos regularmente matriculados na UNICAMP; e deve prever a troca mútua de conhecimentos e de experiências entre os acadêmicos participantes do projeto e as pessoas da comunidade atendida.</p>  <br>

			<div class="row">
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            	<a href="/editais"><img src="../style/images/editais.jpg" alt="..."></a>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <a href="/cronograma.php"><img src="../style/images/crono_pex_19.png" alt="..."></a>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <a href="/uploads/tutorial_sige.pdf" target="_blank"><img src="../style/images/tutorial.jpg" alt="..."></a>
            </div>
            </div>
            </div>

</div>
<script type="text/javascript">
    $(document).ready( function(){
       $("#home").addClass("active");
    });
</script>
<?php
}
?>