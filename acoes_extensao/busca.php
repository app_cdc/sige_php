<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Buscar ação de extensão"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<div class="container">
<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//

$usuario_id = $_SESSION['UsuarioID'];
$sql_permissao = "SELECT * FROM permissao_usuario pu WHERE pu.usuario = $usuario_id ORDER BY pu.permissao";
$query_permissao = $mysqli->query($sql_permissao);
$usuario_permissoes = array();

if ($result_permissao = $mysqli->query($sql_permissao)) {
    while ($dados_permissao = $query_permissao->fetch_array()) {
        $usuario_permissoes[] = $dados_permissao['permissao'];
    }
}
?>
  <h1><b>Buscar Projeto</b></h1><hr>
  <form class="form-horizontal" name="buscar_projeto" id="buscar_projeto" method="post" accept-charset="utf-8">
    <input type="hidden" name="acao_extensao_id" value="<?php echo $acao_id;?>" />
    <input type="hidden" name="administrador_id" value="<?php echo $usuario_id;?>" />

    <div class="form-group">
        <label class="control-label col-sm-2" for="projeto_titulo">Título:</label>
        <div class="col-sm-10">
            <input class="form-control" name="projeto_titulo" id="projeto_titulo" size="100" maxlength="100" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="coordenador_matricula">Matricula:</label>
        <div class="col-sm-10">
            <input class="form-control" name="coordenador_matricula" id="coordenador_matricula" size="8" maxlength="8" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="coordenador_nome">Coordenador:</label>
        <div class="col-sm-10">
            <input class="form-control" name="coordenador_nome" id="coordenador_nome" size="100" maxlength="100" />
        </div>
    </div>
    <br>
    <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/acoes_extensao/';">< Voltar</button>
    <button type="submit" class="btn btn-danger" name="projeto_buscar" value="Inserir usuário">Buscar</button>

  </form>
  <br>

<?php
if (!empty($_POST)) {

    $projeto_titulo = $_POST['projeto_titulo'];
    $coordenador_matricula = $_POST['coordenador_matricula'];
    $coordenador_nome = $_POST['coordenador_nome'];

    $sql_busca = "select ae.id, ae.titulo, u.matricula, u.nome, e.titulo as edital_titulo, ae.estado_acao 
                  from acoes_extensao ae, 
                      usuarios u, 
                      editais e 
                  where ae.coordenador = u.id 
                    and ae.edital = e.id";

    $sql_busca .= (!empty($_POST['projeto_titulo'])) ? " and ae.titulo like '%$projeto_titulo%'" : "";
    $sql_busca .= (!empty($_POST['coordenador_matricula'])) ? " and u.matricula = $coordenador_matricula" : "";
    $sql_busca .= (!empty($_POST['coordenador_nome'])) ? " and u.nome like '%$coordenador_nome%'" : "";

    $texto_busca = "";
    $texto_busca .= (!empty($_POST['projeto_titulo'])) ? " Busca por Título do projeto contendo '$projeto_titulo'. <br>" : "";
    $texto_busca .= (!empty($_POST['coordenador_matricula'])) ? " Busca por Matricula igual a '$coordenador_matricula'. <br>" : "";
    $texto_busca .= (!empty($_POST['coordenador_nome'])) ? " Busca por Coordenador contendo '$coordenador_nome'. <br>" : "";

    echo "<h1>".((!empty($texto_busca)) ? $texto_busca : "Busca sem filtros")."</h1><hr>";

    if ($result_busca = $mysqli->query($sql_busca)) {
        if($result_busca->num_rows > 0){

          echo "
            <table class='table table-bordered table-striped'>
              <thead>
                <tr>
                  <th class='col-md-4'>Edital</th>
                  <th class='col-md-5'>Título</th>
                  <th class='col-md-3'>Coordenador</th>
                </tr>
              </thead>
              <tbody>
           ";

          while($dados = $result_busca->fetch_array()){

            echo "<tr>";
            echo "  <td>".$dados['edital_titulo']."</td>";
            if ($dados['estado_acao'] == 1){ // se for pendente não aparece o link
              echo "  <td>".$dados['titulo']."</td>";
            }else{
              echo "  <td><a href='show.php?id=".$dados['id']."'>".$dados['titulo']."</a></td>";
            }
            echo "  <td>".$dados['matricula']." - ".$dados['nome']."</td>";
            echo "</tr>";
          }
          echo "
              </tbody>
            </table>
          ";
        }else{
          echo "Nada encontrado";
        }
    }


}

?>
</div>
