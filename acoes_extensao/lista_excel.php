<?php
		/*
		ini_set('display_errors',1);
		ini_set('display_startup_erros',1);
		error_reporting(E_ALL);
		header('Content-Type: text/html; charset=utf-8');
		*/
		include "../includes/conecta.php";
		/*
		* Criando e exportando planilhas do Excel
		* /
		*/
		// Definimos o nome do arquivo que será exportado
		$arquivo = 'sige.xls';
		// Criamos uma tabela HTML com o formato da planilha
    $sql_busca = "select ae.id, 
                        ae.titulo, 
                        u.matricula, 
                        u.nome, 
                        e.titulo as edital_titulo, 
                        ae.estado_acao,
                        le.nome as linhas,
                        GROUP_CONCAT(t.nome SEPARATOR ', ') as areas
                  from acoes_extensao ae, 
                        usuarios u, 
                        editais e,
                        linhas_extensao le,
                        acao_tematica a,
                        areas_tematicas t
                  where ae.coordenador = u.id 
                    and ae.edital = e.id
                    and le.id = ae.linha_extensao
                    and ae.id = a.acao_extensao
                    and a.area_tematica = t.id
                  group by ae.id";

    if ($result_busca = $mysqli->query($sql_busca)) {
        if($result_busca->num_rows > 0){
        	$html = '';
          $html .= "
            <table>
              <thead>
                <tr>
                  <th>Edital</th>
                  <th>Título</th>
                  <th>Coordenador</th>
                  <th>Local</th>
                  <th>Áreas</th>
                  <th>Linha</th>
                </tr>
              </thead>
              <tbody>
           ";

          while($dados = $result_busca->fetch_array()){

            $query_dgrh = $pdo->prepare("SELECT LOCAL, EMAIL FROM siarh_sige WHERE matricula = :matricula");
            $query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
            $query_dgrh->execute();
            $dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);

            $html .= "<tr>";
            $html .= "  <td>".$dados['edital_titulo']."</td>";
            if ($dados['estado_acao'] == 1){ // se for pendente não aparece o link
              $html .= "  <td>".$dados['titulo']."</td>";
            }else{
              $html .= "  <td>".$dados['titulo']."</td>";
            }
            $html .= "  <td>".$dados['matricula']." - ".$dados['nome']."</td>";
            $html .= "  <td>".$dgrh_dados['LOCAL']."</td>";
            $html .= "  <td>".$dados['areas']."</td>";
            $html .= "  <td>".$dados['linhas']."</td>";
            $html .= "</tr>";
          }
          $html .= "
              </tbody>
            </table>
          ";
        }
    }
		// Configurações header para forçar o download
		
		//header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
		header ("Content-Description: PHP Generated Data" );
		
		// Envia o conteúdo do arquivo
		echo $html;
		exit;
?>