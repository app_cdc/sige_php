<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Criar nova ação de extensão"; // Título da Página
    //$TPL->ContentHead = "../includes/head_abas.php"; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);
//inicializa as variaveis que vão armazenar os dados da ação
$acao_id ='';
$acao_titulo ='';
$acao_edital ='';
$acao_estado_acao ='';
$acao_coordenador ='';
$acao_criacao ='';
$acao_alteracao ='';
$acao_tipo_extensao ='';
$acao_indicador1 ='';
$acao_indicador2 ='';
$acao_indicador3 ='';
$acao_indicador4 ='';
$acao_indicador5 ='';
$acao_indicador6 ='';
$acao_indicador7 ='';
$acao_indicador8 ='';
$acao_indicador9 ='';
$acao_indicador10 ='';
$acao_metodologia ='';
$acao_local ='';
$acao_ano_inicio ='';
$acao_inicio ='';
$acao_termino ='';
$acao_situacao ='';
$acao_n_pessoas_alvo_interno ='';
$acao_n_pessoas_alvo_externo ='';
$acao_caracterizacao_comunidade ='';
$acao_introducao = '';
$acao_objetivo_geral ='';
$acao_grau_envolvimento_equipe ='';
$acao_parceria_instituicao ='';
$acao_em_execucao = '';
$acao_quanto_tempo = '';
$acao_em_execucao_descricao = '';
$acao_linha_extensao = "";

//se o botão salvar foi apertado o metodo POST não estara vazio
if( !empty($_POST) ){

    // pega os dados preenchidos no formulario
    $acao_id                        = $_POST['acao_id'];
    $acao_titulo                    = $mysqli->real_escape_string($_POST['titulo']);
    $acao_edital                    = $_POST['edital_id'];
    $edital_codigo                  = $_POST['edital_codigo'];
    $acao_estado_acao               = 1;
    $acao_coordenador               = $_SESSION['UsuarioID'];
    $acao_criacao                   = date("Y-m-d H:i:s"); // data atual
    $acao_alteracao                 = date("Y-m-d H:i:s"); // data atual
    $acao_tipo_extensao             = 1; // 1 = projeto // $_POST['tipo_extensao'];
    $acao_indicador1                = $mysqli->real_escape_string($_POST['indicador1']);
    $acao_indicador2                = $mysqli->real_escape_string($_POST['indicador2']);
    $acao_indicador3                = $mysqli->real_escape_string($_POST['indicador3']);
    $acao_indicador4                = $mysqli->real_escape_string($_POST['indicador4']);
    $acao_indicador5                = $mysqli->real_escape_string($_POST['indicador5']);
    $acao_indicador6                = $mysqli->real_escape_string($_POST['indicador6']);
    $acao_indicador7                = $mysqli->real_escape_string($_POST['indicador7']);
    $acao_indicador8                = $mysqli->real_escape_string($_POST['indicador8']);
    $acao_indicador9                = $mysqli->real_escape_string($_POST['indicador9']);
    $acao_indicador10               = $mysqli->real_escape_string($_POST['indicador10']);

    $acao_metodologia               = $mysqli->real_escape_string($_POST['metodologia']);
    $acao_local                     = $_POST['cidade'];
    $acao_ano_inicio                = date("Y"); // ano atual
    $acao_inicio                    = date("Y-m-d"); // dia atual
    $acao_termino                   = date("Y-m-d"); // dia atual
    $acao_situacao                  = 1;
    $acao_n_pessoas_alvo_interno    = $_POST['n_pessoas_alvo_interno'];
    $acao_n_pessoas_alvo_externo    = $_POST['n_pessoas_alvo_externo'];
    $acao_caracterizacao_comunidade = $mysqli->real_escape_string($_POST['caracterizacao_comunidade']);
    $acao_introducao                = $mysqli->real_escape_string($_POST['introducao']);
    $acao_objetivo_geral            = $mysqli->real_escape_string($_POST['objetivo_geral']);
    $acao_grau_envolvimento_equipe  = $_POST['grau_envolvimento_equipe'];
    // se o indicador tiver um campo "Como?", verifica se foi selecionado "Sim" e pega o valor preenchido no campo "Como?"
    if ($_POST['parceria_instituicao'] == 'Sim'){
        $acao_parceria_instituicao           = $mysqli->real_escape_string($_POST['parceria_instituicao_especificar']);
    }else{
        $acao_parceria_instituicao      = $_POST['parceria_instituicao'];
    };
    $acao_em_execucao               = $mysqli->real_escape_string($_POST['projeto_em_execucao']);
    $acao_quanto_tempo              = $mysqli->real_escape_string($_POST['quanto_tempo']);
    $acao_em_execucao_descricao     = $mysqli->real_escape_string($_POST['descricao']);
    $acao_linha_extensao            = $_POST['linha_extensao'];

    if($acao_em_execucao == "Não"){
        $acao_quanto_tempo              = "";
        $acao_em_execucao_descricao     = "";
    }

    // se não tiver um id da ação então deve inserir um nova ação
    if(empty($acao_id)){

      $sql = "INSERT INTO acoes_extensao (titulo,
                                            edital,
                                            estado_acao,
                                            coordenador,
                                            criacao,
                                            alteracao,
                                            tipo_extensao,
                                            indicador1,
                                            indicador2,
                                            indicador3,
                                            indicador4,
                                            indicador5,
                                            indicador6,
                                            indicador7,
                                            indicador8,
                                            indicador9,
                                            indicador10,
                                            metodologia,
                                            local,
                                            ano_inicio,
                                            inicio,
                                            termino,
                                            situacao,
                                            n_pessoas_alvo_interno,
                                            n_pessoas_alvo_externo,
                                            caracterizacao_comunidade,
                                            introducao,
                                            objetivo_geral,
                                            grau_envolvimento_equipe,
                                            parceria_instituicao,
                                            em_execucao,
                                            quanto_tempo,
                                            em_execucao_descricao,
                                            linha_extensao) 
                                VALUES ('$acao_titulo',
                                        $acao_edital,
                                        $acao_estado_acao,
                                        $acao_coordenador,
                                        '$acao_criacao',
                                        '$acao_alteracao',
                                        $acao_tipo_extensao,
                                        '$acao_indicador1',
                                        '$acao_indicador2',
                                        '$acao_indicador3',
                                        '$acao_indicador4',
                                        '$acao_indicador5',
                                        '$acao_indicador6',
                                        '$acao_indicador7',
                                        '$acao_indicador8',
                                        '$acao_indicador9',
                                        '$acao_indicador10',
                                        '$acao_metodologia',
                                        $acao_local,
                                        '$acao_ano_inicio',
                                        '$acao_inicio',
                                        '$acao_termino',
                                        $acao_situacao,
                                        $acao_n_pessoas_alvo_interno,
                                        $acao_n_pessoas_alvo_externo,
                                        '$acao_caracterizacao_comunidade',
                                        '$acao_introducao',
                                        '$acao_objetivo_geral',
                                        $acao_grau_envolvimento_equipe,
                                        '$acao_parceria_instituicao',
                                        '$acao_em_execucao',
                                        '$acao_quanto_tempo',
                                        '$acao_em_execucao_descricao',
                                        $acao_linha_extensao)";
    }else{

       $sql = "UPDATE acoes_extensao 
                SET titulo = '$acao_titulo',
                    estado_acao = $acao_estado_acao,
                    coordenador = $acao_coordenador,
                    alteracao = '$acao_alteracao',
                    tipo_extensao = $acao_tipo_extensao,
                    indicador1 = '$acao_indicador1',
                    indicador2 = '$acao_indicador2',
                    indicador3 = '$acao_indicador3',
                    indicador4 = '$acao_indicador4',
                    indicador5 = '$acao_indicador5',
                    indicador6 = '$acao_indicador6',
                    indicador7 = '$acao_indicador7',
                    indicador8 = '$acao_indicador8',
                    indicador9 = '$acao_indicador9',
                    indicador10 = '$acao_indicador10',
                    metodologia = '$acao_metodologia',
                    local = $acao_local,
                    ano_inicio = '$acao_ano_inicio',
                    inicio = '$acao_inicio',
                    termino = '$acao_termino',
                    situacao = $acao_situacao,
                    n_pessoas_alvo_interno = $acao_n_pessoas_alvo_interno,
                    n_pessoas_alvo_externo = $acao_n_pessoas_alvo_externo,
                    caracterizacao_comunidade = '$acao_caracterizacao_comunidade',
                    introducao = '$acao_introducao',
                    objetivo_geral = '$acao_objetivo_geral',
                    grau_envolvimento_equipe = $acao_grau_envolvimento_equipe,
                    parceria_instituicao = '$acao_parceria_instituicao',
                    em_execucao = '$acao_em_execucao',
                    quanto_tempo = '$acao_quanto_tempo',
                    em_execucao_descricao = '$acao_em_execucao_descricao',
                    linha_extensao = $acao_linha_extensao
                WHERE id = $acao_id";
    }

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {

        if(empty($acao_id)){
            $acao_id = $mysqli->insert_id;
        }
        // areas e linhas de extensão

        if(isset($_POST['areas_tematicas'])) {

            $sql_area = "DELETE FROM acao_tematica WHERE acao_extensao=$acao_id";
            if ($mysqli->query($sql_area) === FALSE) {
                $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_area;
            }else{
                foreach($_POST['areas_tematicas'] as $key => $value) {

                    // insere na tabela acao_tematica
                    $sql_area = "INSERT INTO acao_tematica (acao_extensao,area_tematica) VALUES ($acao_id,$value)";
                    if ($mysqli->query($sql_area) === FALSE) {
                    $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_area;
                    }
                }
            }

        }


    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao criar a ação de extensão. '.$msg_erro;
    }else{
        $sql_rascunho = "DELETE FROM acoes_extensao_rascunho WHERE coordenador = ".$acao_coordenador." AND edital =".$acao_edital;
        $mysqli->query($sql_rascunho);
        $mysqli->commit();
        echo "Ação inserida com sucesso!\n";
        echo '<script>window.location.href = "show.php?id='.$acao_id.'"</script>';
        exit;
    }
    $mysqli->close();

}
else
{

// se for inserir uma ação nova pega o id do edital
if (isset($_GET['cod_edital'])) {

    $link_voltar = "../editais/index.php";
    
    $edital_codigo = $_GET['cod_edital'];

    $sql = "SELECT * FROM editais WHERE codigo = ".$edital_codigo;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
            $acao_edital = $dados['id'];
        }
    }

    $sql = "SELECT * FROM acoes_extensao_rascunho WHERE coordenador = ".$_SESSION["UsuarioID"]." AND edital =".$acao_edital;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {

            $acao_titulo                    = $dados['titulo'];
            $acao_edital                    = $dados['edital'];
            $acao_coordenador               = $dados['coordenador'];
            $acao_tipo_extensao             = $dados['tipo_extensao'];
            $acao_indicador1                = $dados['indicador1'];
            $acao_indicador2                = $dados['indicador2'];
            $acao_indicador3                = $dados['indicador3'];
            $acao_indicador4                = $dados['indicador4'];
            $acao_indicador5                = $dados['indicador5'];
            $acao_indicador6                = $dados['indicador6'];
            $acao_indicador7                = $dados['indicador7'];
            $acao_indicador8                = $dados['indicador8'];
            $acao_indicador9                = $dados['indicador9'];
            $acao_indicador10               = $dados['indicador10'];
            $acao_metodologia               = $dados['metodologia'];
            $acao_local                     = $dados['local'];
            $acao_n_pessoas_alvo_interno    = $dados['n_pessoas_alvo_interno'];
            $acao_n_pessoas_alvo_externo    = $dados['n_pessoas_alvo_externo'];
            $acao_caracterizacao_comunidade = $dados['caracterizacao_comunidade'];
            $acao_introducao                = $dados['introducao'];
            $acao_objetivo_geral            = $dados['objetivo_geral'];
            $acao_grau_envolvimento_equipe  = $dados['grau_envolvimento_equipe'];
            $acao_parceria_instituicao      = $dados['parceria_instituicao'];
            $acao_em_execucao               = $dados['em_execucao'];
            $acao_quanto_tempo              = $dados['quanto_tempo'];
            $acao_em_execucao_descricao     = $dados['em_execucao_descricao'];
            $acao_linha_extensao            = $dados['linha_extensao'];

            $sql = "SELECT * FROM editais WHERE id = ".$acao_edital;
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
                while ($dados = $query->fetch_array()) {
                    $edital_codigo = $dados['codigo'];
                }
            }

            $sql = "SELECT * FROM cidades WHERE cod_cidades = ".$acao_local;
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
                while ($dados = $query->fetch_array()) {
                    $acao_local_estado = $dados['estados_cod_estados'];
                }
            }
        }
    }
?>
<script type="text/javascript">
$(document).ready(function() {
  setInterval(function () {
    $.post("autosave.php", $("#cadastro").serialize());
  }, 2000);
});
</script>
<?php    
}

// se estiver alterando uma ação existente pega todos os dados dela
if (isset($_GET['acao_id'])) {
    
    $acao_id = $_GET['acao_id'];

    $link_voltar = "show.php?id=$acao_id";
    
    $sql = "SELECT * FROM acoes_extensao WHERE id = ".$acao_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {

            $acao_titulo                    = $dados['titulo'];
            $acao_edital                    = $dados['edital'];
            $acao_estado_acao               = $dados['estado_acao'];
            $acao_coordenador               = $dados['coordenador'];
            $acao_criacao                   = $dados['criacao'];
            $acao_alteracao                 = $dados['alteracao'];
            $acao_tipo_extensao             = $dados['tipo_extensao'];
            $acao_indicador1                = $dados['indicador1'];
            $acao_indicador2                = $dados['indicador2'];
            $acao_indicador3                = $dados['indicador3'];
            $acao_indicador4                = $dados['indicador4'];
            $acao_indicador5                = $dados['indicador5'];
            $acao_indicador6                = $dados['indicador6'];
            $acao_indicador7                = $dados['indicador7'];
            $acao_indicador8                = $dados['indicador8'];
            $acao_indicador9                = $dados['indicador9'];
            $acao_indicador10               = $dados['indicador10'];
            $acao_metodologia               = $dados['metodologia'];
            $acao_local                     = $dados['local'];
            $acao_ano_inicio                = $dados['ano_inicio'];
            $acao_inicio                    = $dados['inicio'];
            $acao_termino                   = $dados['termino'];
            $acao_situacao                  = $dados['situacao'];
            $acao_n_pessoas_alvo_interno    = $dados['n_pessoas_alvo_interno'];
            $acao_n_pessoas_alvo_externo    = $dados['n_pessoas_alvo_externo'];
            $acao_caracterizacao_comunidade = $dados['caracterizacao_comunidade'];
            $acao_introducao                = $dados['introducao'];
            $acao_objetivo_geral            = $dados['objetivo_geral'];
            $acao_grau_envolvimento_equipe  = $dados['grau_envolvimento_equipe'];
            $acao_parceria_instituicao      = $dados['parceria_instituicao'];
            $acao_em_execucao               = $dados['em_execucao'];
            $acao_quanto_tempo              = $dados['quanto_tempo'];
            $acao_em_execucao_descricao     = $dados['em_execucao_descricao'];
            $acao_linha_extensao            = $dados['linha_extensao'];

            $sql = "SELECT * FROM editais WHERE id = ".$acao_edital;
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
                while ($dados = $query->fetch_array()) {
                    $edital_codigo = $dados['codigo'];
                }
            }

            $sql = "SELECT * FROM cidades WHERE cod_cidades = ".$acao_local;
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
                while ($dados = $query->fetch_array()) {
                    $acao_local_estado = $dados['estados_cod_estados'];
                }
            }
        }


    }


}

//Mostra a tela para inserir uma nova ação

?>
<script type="text/javascript">
function fetch_select(val)
{
   $.ajax({
     type: 'post',
     url: '../includes/fetch_data_cidade.php',
     data: {
       estado_cod:val
     },
     success: function (response) {
       document.getElementById("cidade").innerHTML=response; 
     }
   });
}
</script>

<div class="container">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Dados">Dados <i class="fa"></i></a></li>
        <li><a href="#Areas" data-toggle="tab">Áreas <i class="fa"></i></a></li>
        <li><a href="#Linhas" data-toggle="tab">Linhas <i class="fa"></i></a></li>
        <li><a href="#Indicadores" data-toggle="tab">Dados Indicadores <i class="fa"></i></a></li>
    </ul>

    <form role="form" enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

        <div class="tab-content">
            <div class="tab-pane fade in active" id="Dados">
                <h3>Dados do projeto</h3>
                <p>Neste Item há 3 campos onde devem ser inseridos

textos descritivos: <b>Comunidade Participante</b>, <b>Objetivo Geral</b> e <b>Metodologia</b>. Além de

atender objetivamente a cada uma das 3 demandas, nestes textos deverão estar

claramente destacadas as Justificativas, Descrições ou Explicação de cada uma das

respostas dadas no item <b>Dados Indicadores</b> que aparece na sequência. Estas

Justificativas, Descrições ou Explicações relativas a cada pergunta daquele item, além

de nos ajudarem a construir indicadores qualitativos mais apropriados para Extensão

Comunitária, ajudarão aos avaliadores a ter uma noção mais precisa de sua proposta.</p>
                <hr>

                <input type="hidden" name="edital_id" value="<?php echo $acao_edital;?>" />
                <input type="hidden" name="edital_codigo" value="<?php echo $edital_codigo;?>" />
                <input type="hidden" name="acao_id" value="<?php echo $acao_id;?>" />

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="titulo">Título: <br>(máx. 100 caracteres)</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="titulo" id="titulo" size="50" maxlength="100" required autofocus value="<?php echo $acao_titulo;?>" />
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-2" for="tipo_extensao">Tipo de Extensão:</label>
                    <div class="col-sm-3">
                        <input class="form-control" name="tipo_extensao" readonly value="Projeto" />
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Local do projeto</h4>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="estado">Estado:</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="estado" id="estado" onchange="fetch_select(this.value);">
                            <option>Selecione o estado</option>
                            <?php

                            if (!empty($acao_local_estado)) {
                                $sql = "SELECT * FROM cidades WHERE cod_cidades";
                                $query = $mysqli->query($sql);
                            }

                            $sql = "SELECT * FROM estados";
                            $query = $mysqli->query($sql);

                            if ($result = $mysqli->query($sql)) {
                                while ($dados = $query->fetch_array()) {
                                    $estado_cod = $dados['cod_estados'];
                                    $estado_nome = $dados['nome'];
                                    $estado_selecionado = ($acao_local_estado == $estado_cod) ? "selected" : "" ;
                                    echo "<option value='$estado_cod' $estado_selecionado>$estado_nome</option>";
                                }
                            }

                            ?>
                        </select>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-2" for="cidade">Cidade:</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="cidade" id="cidade" required>
                            <?php
                            
                            if (!empty($acao_local_estado)) {

                                $sql = "SELECT * FROM cidades WHERE estados_cod_estados=$acao_local_estado";
                                $query = $mysqli->query($sql);

                                if ($result = $mysqli->query($sql)) {
                                    while ($dados = $query->fetch_array()) {
                                        $cidade_selecionada = ($dados['cod_cidades'] == $acao_local) ? "selected" : "" ;
                                        echo "<option value='".$dados['cod_cidades']."' $cidade_selecionada>".$dados['nome']."</option>";
                                    }
                                }
                            }

                            ?>
                        </select>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Comunidade Participante</h4>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="n_pessoas_alvo_interno">Número de pessoas da UNICAMP que serão envolvidas:</label>
                    <div class="col-sm-2">
                        <input class="form-control" type="number" name="n_pessoas_alvo_interno" required value="<?php echo $acao_n_pessoas_alvo_interno;?>" min="1">
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-3" for="n_pessoas_alvo_externo">Número de pessoas externas que serão envolvidas:</label>
                    <div class="col-sm-2">
                        <input class="form-control" type="number" name="n_pessoas_alvo_externo" required value="<?php echo $acao_n_pessoas_alvo_externo;?>" min="1">
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-3" for="caracterizacao_comunidade">Realidade social, econômica e cultural da Comunidade: <br>(máx. 3000 caracteres)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" rows="7" name="caracterizacao_comunidade" required maxlength="3000" placeholder="Apresente a realidade social, econômica e cultural que fundamente a necessidade do desenvolvimento e da importância do projeto"><?php echo $acao_caracterizacao_comunidade; ?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>O projeto já está em execução?</h4>
    
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="radio-inline"><input type="radio" name="projeto_em_execucao" <?php if($acao_em_execucao == 'Sim'){echo("checked");}?> value="Sim">Sim</input></label>
                        <label class="radio-inline"><input type="radio" name="projeto_em_execucao" <?php if(empty($acao_em_execucao) || $acao_em_execucao == 'Não'){echo("checked");}?> value="Não">Não</input></label>
                    </div>
                </div><!--div form-group-->

                <div class="form-group execucao">
                    <label class="control-label col-sm-2" for="quanto_tempo">Há quanto tempo?</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="quanto_tempo" value="<?php echo $acao_quanto_tempo ?>" min="1" placeholder="Em meses"></input>
                    </div>
                </div><!--div form-group-->

                <div class="form-group execucao">
                    <label class="control-label col-sm-2" for="descricao">Comentar: <br>(máx. 3000 caracteres)</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="7" name="descricao" maxlength="3000" placeholder="inclua uma descrição das atividades"><?php echo $acao_em_execucao_descricao ?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Tipo de envolvimento da equipe com a Comunidade</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="1" <?php if(empty($acao_grau_envolvimento_equipe) || $acao_grau_envolvimento_equipe == '1'){echo("checked");}?>>Não conhece a comunidade</input><br>
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="2" <?php if($acao_grau_envolvimento_equipe == '2'){echo("checked");}?>>Conhece a comunidade</input><br>
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="3" <?php if($acao_grau_envolvimento_equipe == '3'){echo("checked");}?>>Já houve contato com a comunidade</input><br>
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="4" <?php if($acao_grau_envolvimento_equipe == '4'){echo("checked");}?>>Já houve outro projeto da equipe junto à comunidade</input><br>
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="5" <?php if($acao_grau_envolvimento_equipe == '5'){echo("checked");}?>>Está totalmente integrada com a comunidade</input><br>
                        <input class="radio-inline col-sm-1" type="radio" name="grau_envolvimento_equipe" value="6" <?php if($acao_grau_envolvimento_equipe == '6'){echo("checked");}?>>Existe um documento de aceitação do projeto pela comunidade</input>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>Há parcerias com outras instituições (públicas ou privadas) para o desenvolvimento do projeto?</h4>

                <div class="form-group">
                    <div class="col-sm-2">
                        <label class="radio-inline "><input type="radio" name="parceria_instituicao" value="Sim" <?php if(!empty($acao_parceria_instituicao) && $acao_parceria_instituicao != 'Não'){echo("checked");} ?>>Sim</input></label>
                        <label class="radio-inline "><input type="radio" name="parceria_instituicao" value="Não" <?php if(empty($acao_parceria_instituicao) || $acao_parceria_instituicao == 'Não'){echo("checked");} ?>>Não</input></label>
                    </div>
                </div><!--div form-group-->

                <div class="form-group" id="parceria_instituicao">
                    <label class="control-label col-sm-2" for="parceria_instituicao_especificar">Especifique: <br>(máx. 1000 caracteres)</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" name="parceria_instituicao_especificar" maxlength="1000"><?php echo ($acao_parceria_instituicao != 'Não') ? $acao_parceria_instituicao : "" ; ?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>Introdução: (máx. 2000 caracteres)</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="7" name="introducao" maxlength="2000" required placeholder="Contextualizar a situação da comunidade na qual o projeto atuará, descrevendo os principais fatores e dificuldades determinantes dos problemas identificados, inclusive no âmbito nacional quando for o caso. Apresente a importância do projeto justificando o caminho tomado e as prioridades definidas. Mencione as principais teorias e autores que estudam o tema e a metodologia que serão adotadas no desenvolvimento do projeto."><?php echo $acao_introducao;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>Objetivo Geral: (máx. 1000 caracteres)</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="7" name="objetivo_geral" maxlength="1000" required placeholder="Coloque aqui somente o Objetivo Geral da proposta. Posteriormente será necessário definir os “Objetivos Específicos” da proposta, a descrição de cada um e as diferentes “Ações” a serem realizadas relativas a cada um dos “Objetivos Específicos”"><?php echo $acao_objetivo_geral;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Metodologia: (máx. 3000 caracteres)</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="7" name="metodologia" maxlength="3000" required placeholder="Indique como se espera realizar o projeto considerando seu planejamento, a equipe envolvida, o resultado a ser alcançado, inclusive apontando as possíveis dificuldades de execução, referências que fundamentem conceitualmente o projeto."><?php echo $acao_metodologia;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
    
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='<?php echo $link_voltar?>';">< Voltar ao Principal</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>
    
            </div><!--div tab-pane-->

            <div id="Areas" class="tab-pane fade">

                <h3>Áreas Temáticas</h3>
                <hr>

                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                        $sql = "SELECT * FROM areas_tematicas";
                        $query = $mysqli->query($sql);
                        $area_marcada = "";

                        if ($result = $mysqli->query($sql)) {
                          while ($dados = $query->fetch_array()) {
                            $area_id = $dados['id'];
                            $area_nome = $dados['nome'];

                            if (isset($acao_id) && !empty($acao_id)) {
                                $sql2 = "SELECT * FROM acao_tematica WHERE area_tematica = $area_id AND acao_extensao = $acao_id";
                                $query2 = $mysqli->query($sql2);
                                $teste = $query2->num_rows;
                                $area_marcada = ($teste == 1) ? "checked" : "" ;
                            }
                            
                           echo "<label class='radio-inline'><input type='checkbox' name='areas_tematicas[]' value='$area_id' $area_marcada> $area_nome</label><br>";
                          }
                        }
                        ?>
                        <br>
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

            </div><!--div tab-pane-->

            <div id="Linhas" class="tab-pane fade">
                
                <h3>Linhas de Extensão</h3>
                <hr>

                <div class="form-group">
                    <div class="col-sm-12 duas_colunas">
                        <?php
                        $sql = "SELECT * FROM linhas_extensao";
                        $query = $mysqli->query($sql);
                        $linha_marcada = "";

                        if ($result = $mysqli->query($sql)) {
                          while ($dados = $query->fetch_array()) {
                            $linha_id = $dados['id'];
                            $linha_nome = $dados['nome'];
                            $linha_descricao = $dados['descricao'];

                            $linha_marcada = (!empty($acao_linha_extensao) && $acao_linha_extensao == $linha_id) ? "checked" : "" ;

                            echo "<label class='radio-inline' data-toggle='tooltip' data-placement='bottom' title='$linha_descricao'><input type='radio' name='linha_extensao' value='$linha_id' $linha_marcada > $linha_nome</input></label><br>";
                          }
                        }
                        ?>
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>
            </div><!--div tab-pane-->

            <div id="Indicadores" class="tab-pane fade">
            
                <h3 class="titulo">Dados Indicadores (Máx. 450 caracteres)</h3>
                
                <hr>
                <h4>1. Por que o projeto deve ser enquadrado como iniciativa de extensão comunitária?</h4>
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador1" maxlength="450" required><?php echo $acao_indicador1;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
            
                <h4>2. Quais as contribuições do projeto para suas atividades de ensino, já realizadas ou potenciais?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador2" maxlength="450" required><?php echo $acao_indicador2;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>3. Quais as contribuições do projeto para suas atividades de pesquisa, já realizadas ou potenciais?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador3" maxlength="450" required><?php echo $acao_indicador3;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>4. Quais as contribuições esperadas do projeto para a formação acadêmica, profissional e cidadã dos alunos envolvidos?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador4" maxlength="450" required><?php echo $acao_indicador4;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>5. De que maneiras a comunidade externa à Unicamp será envolvida no projeto?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador5" maxlength="450" required><?php echo $acao_indicador5;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>6. Quais resultados e impactos de longo prazo o projeto deve trazer para a comunidade/sociedade?</h4>
            
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador6" maxlength="450" required><?php echo $acao_indicador6;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>7. O projeto tem potencial para gerar novos conhecimentos, técnicas, metodologias ou formas de organização do trabalho e da gestão?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador7" maxlength="450" required><?php echo $acao_indicador7;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>                
                <h4>8. De que forma será feito o acompanhamento do projeto e sua avaliação pela equipe responsável?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador8" maxlength="450" required><?php echo $acao_indicador8;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>9. Como o projeto pode contribuir para o estreitamento da relação entre a universidade e a sociedade?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador9" maxlength="450" required><?php echo $acao_indicador9;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>    
                <h4>10. O projeto contribui com a constituição de redes que envolvam a produção, difusão e uso de conhecimentos e envolve outros parceiros e apoiadores?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="indicador10" maxlength="450" required><?php echo $acao_indicador10;?></textarea>
                    </div>
                </div><!--div form-group-->

                <hr>
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button></li>
                    </ul>
                </div>
                
                <br>

            </div><!--div tab-pane-->
        </div><!--div tab-content-->
    </form>
</div><!--div container-->
  


<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

$('.btnProximo').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});

$('.btnAnterior').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});

// função para esconder e exibir os campos
$(document).ready(function() {
    // projeto_em_execucao
    if($('input[name=projeto_em_execucao]:checked').val() == 'Sim') {
        $('.execucao').show(); 
    } else {
        $('.execucao').hide(); 
    } 
    $('input:radio[name=projeto_em_execucao]').change(function() {
        if(this.value == 'Sim') {
            $('.execucao').show(); 
        } else {
            $('.execucao').hide(); 
        } 
    });

    // parceria_instituicao
    if($('input[name=parceria_instituicao]:checked').val() == 'Sim') {
        $('#parceria_instituicao').show(); 
    } else {
        $('#parceria_instituicao').hide(); 
    } 
    $('input:radio[name=parceria_instituicao]').change(function() {
        if(this.value == 'Sim') {
            $('#parceria_instituicao').show(); 
        } else {
            $('#parceria_instituicao').hide(); 
        } 
    });

});

</script>


<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                titulo: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o Título'
                        },
                        stringLength: {
                            max: 100,
                            message: 'O título deve ter no máximo 100 caracteres'
                        }
                    }
                },
                n_pessoas_alvo_interno: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o número de pessoas da UNICAMP'
                        },
                        greaterThan: {
                            value: 1,
                            message: 'O menor valor deve ser 1'
                        }
                    }
                },
                n_pessoas_alvo_externo: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o número de pessoas externas'
                        },
                        greaterThan: {
                            value: 1,
                            message: 'O menor valor deve ser 1'
                        }
                    }
                },
                caracterizacao_comunidade: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a realidade da comunidade'
                        }
                    }
                },
                quanto_tempo: {
                    validators: {
                        callback: {
                            message: 'Informe quanto tempo em meses',
                            callback: function(value, validator, $field){
                                var execucao = $('#cadastro').find('[name="projeto_em_execucao"]:checked').val();
                                return (execucao == 'Não') ? true : (value !== '');
                            }
                        },
                        numeric:{
                            message: 'Coloque somente o número de meses'
                        }
                    }
                },
                descricao: {
                    validators: {
                        callback: {
                            message: 'Preencha seu comentário',
                            callback: function(value, validator, $field){
                                var execucao = $('#cadastro').find('[name="projeto_em_execucao"]:checked').val();
                                return (execucao == 'Não') ? true : (value !== '');
                            }
                        }
                    }
                },
                parceria_instituicao_especificar: {
                    validators: {
                        callback: {
                            message: 'Especifique',
                            callback: function(value, validator, $field){
                                var execucao = $('#cadastro').find('[name="parceria_instituicao"]:checked').val();
                                return (execucao == 'Não') ? true : (value !== '');
                            }
                        }
                    }
                },
                metodologia: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a metodologia'
                        }
                    }
                },
                cidade: {
                    validators: {
                        notEmpty: {
                            message: 'Selecione o estado e depois a cidade'
                        }
                    }
                },
                introducao: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a introdução'
                        }
                    }
                },
                objetivo_geral: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o objetivo geral'
                        }
                    }
                },
                linha_extensao: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                indicador1: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador2: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador3: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador4: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador5: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador6: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador7: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador8: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador9: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                indicador10: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o campo acima'
                        }
                    }
                },
                'areas_tematicas[]': {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                }
            }
        })
        .on('change', '[name="projeto_em_execucao"]', function(e) {
            $('#cadastro').bootstrapValidator('revalidateField', 'quanto_tempo');
            $('#cadastro').bootstrapValidator('revalidateField', 'descricao');
        })
        .on('change', '[name="parceria_instituicao"]', function(e) {
            $('#cadastro').bootstrapValidator('revalidateField', 'parceria_instituicao_especificar');
        })
        .on('status.field.bv', function(e, data) {
            var $form     = $(e.target),
                validator = data.bv,
                $tabPane  = data.element.parents('.tab-pane'),
                tabId     = $tabPane.attr('id');
            
            if (tabId) {
                var $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]').parent().find('i');



                // Add custom class to tab containing the field
                if (data.status == validator.STATUS_INVALID) {
                    $icon.removeClass('fa-check').addClass('fa-times');
                    // deslocar a página para o topo das tabs
                    var deslocamento = $('.container').offset().top;
                    //$('html, body').animate({scrollTop:deslocamento}, 'slow');
                    //$('html, body').scrollTop(deslocamento);
                    // fim deslocamento
                } else if (data.status == validator.STATUS_VALID) {
                    var isValidTab = validator.isValidContainer($tabPane);
                    $icon.removeClass('fa-check fa-times')
                         .addClass(isValidTab ? 'fa-check' : 'fa-times');
                }
            }
            if (data.field === 'linha_extensao') {
                //var selecionado = $('#cadastro').find('[name="linha_extensao"]:checked').val();
                // User choose given channel
                //if (selecionado == 'Não') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');

                    // Hide the tick icon
                    data.element.data('bv.icon').hide();
                //}
            }
            if (data.field === 'areas_tematicas[]') {
                //var selecionado = $('#cadastro').find('[name="linha_extensao"]:checked').val();
                // User choose given channel
                //if (selecionado == 'Não') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');

                    // Hide the tick icon
                    data.element.data('bv.icon').hide();
                //}
            }
        })
        .on('success.field.bv', function(e, data) {
            if (data.field === 'quanto_tempo') {
                var selecionado = $('#cadastro').find('[name="projeto_em_execucao"]:checked').val();
                // User choose given channel
                if (selecionado == 'Não') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');

                    // Hide the tick icon
                    data.element.data('bv.icon').hide();
                }
            }
            if (data.field === 'descricao') {
                var selecionado = $('#cadastro').find('[name="projeto_em_execucao"]:checked').val();
                // User choose given channel
                if (selecionado == 'Não') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');

                    // Hide the tick icon
                    data.element.data('bv.icon').hide();
                }
            }
            if (data.field === 'parceria_instituicao_especificar') {
                var selecionado = $('#cadastro').find('[name="parceria_instituicao"]:checked').val();
                // User choose given channel
                if (selecionado == 'Não') {
                    // Remove the success class from the container
                    data.element.closest('.form-group').removeClass('has-success');

                    // Hide the tick icon
                    data.element.data('bv.icon').hide();
                }
            }
        });
});
</script>

<?php
}
?>