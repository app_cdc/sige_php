<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Buscar ação de extensão"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<div class="container-fluid" style="margin-right: 30px; margin-left: 30px;">
<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//

$usuario_id = $_SESSION['UsuarioID'];
$sql_permissao = "SELECT * FROM permissao_usuario pu WHERE pu.usuario = $usuario_id ORDER BY pu.permissao";
$query_permissao = $mysqli->query($sql_permissao);
$usuario_permissoes = array();

if ($result_permissao = $mysqli->query($sql_permissao)) {
    while ($dados_permissao = $query_permissao->fetch_array()) {
        $usuario_permissoes[] = $dados_permissao['permissao'];
    }
}
?>
  <h1><b>Listar Projetos</b></h1><hr>

  <div class="row" style="margin-bottom: 50px;">
  <div class="table-responsive">
    <?php
    $sql_busca = "select ae.id,
                        ae.titulo,
                        u.matricula,
                        u.nome,
                        e.titulo as edital_titulo,
                        ae.estado_acao,
                        le.nome as linhas,
                        GROUP_CONCAT(t.nome SEPARATOR ', ') as areas
                  from acoes_extensao ae,
                        usuarios u,
                        editais e,
                        linhas_extensao le,
                        acao_tematica a,
                        areas_tematicas t
                  where ae.coordenador = u.id
                    and ae.edital = e.id
                    and le.id = ae.linha_extensao
                    and ae.id = a.acao_extensao
                    and a.area_tematica = t.id
                  group by ae.id";

    if ($result_busca = $mysqli->query($sql_busca)) {
        if($result_busca->num_rows > 0){

          echo "
            <table id='table_id' class='table table-bordered table-striped'>
              <thead>
                <tr>
                  <th class='col-md-4'>Edital</th>
                  <th class='col-md-5'>Título</th>
                  <th class='col-md-3'>Coordenador</th>
                  <th class='col-md-3'>Local</th>
                  <th class='col-md-3'>Áreas</th>
                  <th class='col-md-3'>Linha</th>
                  <th class='col-md-3'>Estado</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Edital</th>
                  <th>Título</th>
                  <th>Coordenador</th>
                  <th>Local</th>
                  <th>Áreas</th>
                  <th>Linha</th>
                  <th>Estado</th>
                </tr>
              </tfoot>
              <tbody>
           ";

          while($dados = $result_busca->fetch_array()){

            $query_dgrh = $pdo->prepare("SELECT LOCAL, EMAIL FROM siarh_sige WHERE matricula = :matricula");
            $query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
            $query_dgrh->execute();
            $dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);

            echo "<tr>";
            echo "  <td>".$dados['edital_titulo']."</td>";
            if ($dados['estado_acao'] == 1){ // se for pendente não aparece o link
              echo "  <td>".$dados['titulo']."</td>";
            }else{
              echo "  <td><a href='show.php?id=".$dados['id']."'>".$dados['titulo']."</a></td>";
            }
            echo "  <td>".$dados['matricula']." - ".$dados['nome']."</td>";
            echo "  <td>".$dgrh_dados['LOCAL']."</td>";
            echo "  <td>".$dados['areas']."</td>";
            echo "  <td>".$dados['linhas']."</td>";
            switch ($dados['estado_acao']) {
              case 1:
                echo "  <td>Rascunho</td>";
                break;
              case 2:
                echo "  <td>Submetido</td>";
                break;
              case 3:
                echo "  <td>Em Análise</td>";
                break;
              case 4:
                echo "  <td>Em Avaliação</td>";
                break;
              case 5:
                echo "  <td>Avaliado</td>";
                break;
              case 6:
                echo "  <td>Desclassificado</td>";
                break;
              case 7:
                echo "  <td>Aprovado</td>";
                break;
            }

            echo "</tr>";
          }
          echo "
              </tbody>
            </table>
          ";
        ?>
        <script type="text/javascript">
          $(document).ready( function () {
            var table = $('#table_id').DataTable( {
              "dom": '<"col-sm-4"l><"col-sm-4 text-center"B><"col-sm-4"f><"col-sm-6"i><"col-sm-6"p><rt><"col-sm-6"i><"col-sm-6"p>',
              "buttons": [
                { extend: 'excel', text: 'Gerar Excel' },
                { extend: 'print', text: 'Imprimir' },
              ],
              "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
              "language": {
                "url": "../style/Portuguese-Brasil.json"
              }
            } );
            $("#table_id tfoot th").each( function ( i ) {

              if ( $(this).text() !== '' ) {
                var select = $('<select style="width: 200px;"><option value=""></option></select>')
                  .appendTo( $(this).empty() )
                  .on( 'change', function () {
                    var val = $(this).val();

                    table.column( i )
                      .search( val ? '^'+$(this).val()+'$' : val, true, false )
                      .draw();
                    } );

                table.column( i ).data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );

              }
            } );
          } );
        </script>
        <?php
        }
    }
    ?>
  </div>
  </div>
</div>
