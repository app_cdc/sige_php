<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
  $TPL = new PageTemplate();
  $TPL->PageTitle = "Alterar ação de extensão"; // Título da Página
  //$TPL->ContentHead = ""; // Header da Página
  $TPL->ContentBody = __FILE__;
  include "../layout.php";
  exit;
}
?>
<script type="text/javascript">
function alterar_estado(acao_id,acao_estado)
{
  $.ajax({
    type: 'post',
    url: 'alterar_estado.php',
    data: {
      id:acao_id,
      estado:acao_estado
    },
    success: function (response) {
      $("#myModal").modal('hide');
      if(response){
        //alert("Enviado com sucesso!");
        location.replace("/acoes_extensao/");
      }else{
        //document.getElementById("msg").innerHTML="Erro ao enviar";
        $("#myModalErro").modal().find('.modal-body p').text("Erro ao concluir");
        $("#myModalErro").modal('show');
      }
    }
  });
}
</script>

<div class="container">
<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Atenção!</h4>
      </div>
      <div class="modal-body">
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO, AVALIADOR, COORDENADOR);
protegePagina($permissoes);
//

$usuario_id = $_SESSION['UsuarioID'];
$sql_permissao = "SELECT * FROM permissao_usuario pu WHERE pu.usuario = $usuario_id ORDER BY pu.permissao";
$query_permissao = $mysqli->query($sql_permissao);
$usuario_permissoes = array();

if ($result_permissao = $mysqli->query($sql_permissao)) {
  while ($dados_permissao = $query_permissao->fetch_array()) {
    $usuario_permissoes[] = $dados_permissao['permissao'];
  }
}

if( !empty($_POST) ){

  // se estiver desclassificando o projeto
  if(isset($_POST['motivo']) && !empty($_POST['motivo'])) {

    $msg_erro = "";
    $acao_extensao_id = $_POST['acao_extensao_id'];
    $administrador_id = $_POST['administrador_id'];
    $motivo_id = $_POST['motivo'];
    $data = date("Y-m-d H:i:s");

    $sql_acao_extensao = "SELECT * FROM acoes_extensao WHERE id = $acao_extensao_id";

    if ($result_acao_extensao = $mysqli->query($sql_acao_extensao)) {
      while ($dados_acao_extensao = $result_acao_extensao->fetch_array()) {

        $acao_extensao_estado_antigo = $dados_acao_extensao['estado_acao'];

        $sql_acoes_desclassificacao = "INSERT INTO acoes_desclassificacao (motivo_id,
                                                          acao_extensao_id,
                                                          administrador_id,
                                                          data)
                                        VALUES ($motivo_id,
                                                $acao_extensao_id,
                                                $administrador_id,
                                                '$data')";

        if ($mysqli->query($sql_acoes_desclassificacao) === TRUE) {

          $sql_acao_extensao_update = "UPDATE acoes_extensao
                                        SET estado_acao = 6
                                      WHERE id = $acao_extensao_id";

          if ($mysqli->query($sql_acao_extensao_update) === TRUE) {

            if ($acao_extensao_estado_antigo == 3) {
                $sql_acao_edicao_delete = "DELETE FROM acoes_edicao WHERE id_acao_extensao = $acao_extensao_id";

              if ($mysqli->query($sql_acao_edicao_delete) === FALSE) {
                $msg_erro .= "Error: " . $sql_acao_edicao_delete . "<br>" . $mysqli->error;
              }
            }
          }else{
            $msg_erro .= "Error: " . $sql_acao_extensao_update . "<br>" . $mysqli->error;
          }

        } else {
          $msg_erro .= "Error: " . $sql_acoes_desclassificacao . "<br>" . $mysqli->error;
        }

        if(!empty($msg_erro)){
          $mysqli->rollback();
          echo 'Erro ao desclassificar o projeto. '.$msg_erro;
        }else{
          $mysqli->commit();
          echo "<p>Projeto desclassificado com sucesso!</p>\n";
        }
      }
    }
  }

  // se estiver enviando para o editor
  if(isset($_POST['editor']) && !empty($_POST['editor'])) {

    $msg_erro = "";

    $acao_extensao_id = $_POST['acao_extensao_id'];
    $administrador_id = $_POST['administrador_id'];
    $editor_id = $_POST['editor'];
    $data_envio = date("Y-m-d H:i:s");

    $sql_edicao = "SELECT * FROM acoes_edicao WHERE id_acao_extensao = $acao_extensao_id";
    $query_edicao = $mysqli->query($sql_edicao);
    $teste = $query_edicao->num_rows;

    if ($teste == 1){
      $sql_editor = "UPDATE acoes_edicao SET id_administrador = $administrador_id, id_editor = $editor_id, data_envio = '$data_envio' WHERE id_acao_extensao = $acao_extensao_id";

      if ($mysqli->query($sql_editor) === FALSE) {
        $msg_erro .= "Error: " . $sql_editor . "<br>" . $mysqli->error;
      }

      if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao enviar para o conselheiro. '.$msg_erro;
      }else{
        $mysqli->commit();
        echo "<p>Enviado para o conselheiro com sucesso!</p>\n";
      }
    }else{
      $sql_estado = "SELECT * FROM acoes_extensao WHERE id = $acao_extensao_id AND estado_acao = 2";
      $query_estado = $mysqli->query($sql_estado);
      $teste = $query_estado->num_rows;

      if ($teste == 1){

        $sql_editor = "INSERT INTO acoes_edicao (id_acao_extensao,id_administrador,id_editor,data_envio) VALUES ($acao_extensao_id, $administrador_id, $editor_id, '$data_envio')";

        if ($mysqli->query($sql_editor) === TRUE) {

          $sql_edital_update = "UPDATE acoes_extensao SET estado_acao = 3 WHERE id = $acao_extensao_id";
          if ($mysqli->query($sql_edital_update) === FALSE) {
            $msg_erro .= "Error: " . $sql_edital_update . "<br>" . $mysqli->error;
          }

        } else {
          $msg_erro .= "Error: " . $sql_editor . "<br>" . $mysqli->error;
        }

        if(!empty($msg_erro)){
          $mysqli->rollback();
          echo 'Erro ao enviar para o conselheiro. '.$msg_erro;
        }else{
          $mysqli->commit();
          echo "<p>Enviado para o conselheiro com sucesso!</p>\n";
        }
      }else{
        echo "<p>Já foi enviado para o conselheiro!</p>\n";
      }
    }
  }

  // se estiver avaliando o projeto
  if(isset($_POST['avaliador_id']) && !empty($_POST['avaliador_id'])) {

    $msg_erro = "";
    $acao_extensao_id = $_POST['acao_extensao_id'];
    $avaliador_id = $_POST['avaliador_id'];

    $sql_avaliacao = "SELECT * FROM acoes_avaliacao a WHERE id_acao_extensao = $acao_extensao_id AND id_avaliador = $avaliador_id";
    $query_avaliacao = $mysqli->query($sql_avaliacao);

    if ($result_avaliacao = $mysqli->query($sql_avaliacao)) {
      while ($dados_avaliacao = $query_avaliacao->fetch_array()) {

        $avaliacao_id = $dados_avaliacao['id'];

        $sql = "SELECT * FROM perguntas p ";
        $query = $mysqli->query($sql);

        if ($result = $mysqli->query($sql)) {
          while ($dados = $query->fetch_array()) {
            if (isset($_POST['pergunta_'.$dados['id']]) && !empty($_POST['pergunta_'.$dados['id']]) && empty($msg_erro)) {

              $pergunta_id = $dados['id'];
              $nota = $_POST['pergunta_'.$pergunta_id];

              $sql_pergunta_avaliacao = "INSERT INTO pergunta_avaliacao (id_pergunta,id_avaliacao,nota) VALUES ($pergunta_id, $avaliacao_id, $nota)";

              if ($mysqli->query($sql_pergunta_avaliacao) === FALSE) {
                $msg_erro .= "Error: " . $sql_pergunta_avaliacao . "<br>" . $mysqli->error;
              }

            }
          }
        }

        if(empty($msg_erro)){

          $data_avaliacao = date("Y-m-d H:i:s");
          $observacao = "";

          if(isset($_POST['observacao']) && !empty($_POST['observacao'])) {
            $observacao = $_POST['observacao'];
          }

          // atualiza a avaliação com a data e a observação
          $sql_acao_avaliacao = "UPDATE acoes_avaliacao SET data_avaliacao = '$data_avaliacao' , observacao = '$observacao' WHERE id = $avaliacao_id";

          if ($mysqli->query($sql_acao_avaliacao) === FALSE) {
            $msg_erro .= "Error: " . $sql_acao_avaliacao . "<br>" . $mysqli->error;
          }else{
            $sql_acao = "UPDATE acoes_extensao SET estado_acao = 5 WHERE id = $acao_extensao_id";

            if ($mysqli->query($sql_acao) === FALSE) {
              $msg_erro .= "Error: " . $sql_acao . "<br>" . $mysqli->error;
            }
          }
        }

        if(!empty($msg_erro)){
          $mysqli->rollback();
          echo 'Erro ao salvar a avaliação. '.$msg_erro;
        }else{
          $mysqli->commit();
          echo "<p>Avaliação salva com sucesso!</p>\n";
        }
      }
    }

  }


?>
  <br>
  <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/acoes_extensao/';">< Voltar</button>
<?php

}
else
{

  if (isset($_GET['id'])) {

    $acao_id = $_GET['id'];
    $acao_estado_acao = "";

    $sql = "SELECT * FROM acoes_extensao WHERE id=$acao_id ORDER BY id";
    $query = $mysqli->query($sql);
    $count_acoes = $query->num_rows;

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {

        $acao_titulo                    = $dados['titulo'];
        $acao_edital                    = $dados['edital'];
        $acao_estado_acao               = $dados['estado_acao'];
        $acao_coordenador               = $dados['coordenador'];
        $acao_criacao                   = $dados['criacao'];
        $acao_alteracao                 = $dados['alteracao'];
        $acao_tipo_extensao             = $dados['tipo_extensao'];
        $acao_indicador1                = $dados['indicador1'];
        $acao_indicador2                = $dados['indicador2'];
        $acao_indicador3                = $dados['indicador3'];
        $acao_indicador4                = $dados['indicador4'];
        $acao_indicador5                = $dados['indicador5'];
        $acao_indicador6                = $dados['indicador6'];
        $acao_indicador7                = $dados['indicador7'];
        $acao_indicador8                = $dados['indicador8'];
        $acao_indicador9                = $dados['indicador9'];
        $acao_indicador10               = $dados['indicador10'];
        $acao_metodologia               = $dados['metodologia'];
        $acao_local                     = $dados['local'];
        $acao_ano_inicio                = $dados['ano_inicio'];
        $acao_inicio                    = $dados['inicio'];
        $acao_termino                   = $dados['termino'];
        $acao_situacao                  = $dados['situacao'];
        $acao_n_pessoas_alvo_interno    = $dados['n_pessoas_alvo_interno'];
        $acao_n_pessoas_alvo_externo    = $dados['n_pessoas_alvo_externo'];
        $acao_caracterizacao_comunidade = $dados['caracterizacao_comunidade'];
        $acao_introducao                = $dados['introducao'];
        $acao_objetivo_geral            = $dados['objetivo_geral'];
        $acao_grau_envolvimento_equipe  = $dados['grau_envolvimento_equipe'];
        $acao_parceria_instituicao      = $dados['parceria_instituicao'];
        $acao_em_execucao               = $dados['em_execucao'];
        $acao_quanto_tempo              = $dados['quanto_tempo'];
        $acao_em_execucao_descricao     = $dados['em_execucao_descricao'];
        $acao_linha_extensao            = $dados['linha_extensao'];

        $sql = "SELECT c.nome AS cidade, e.nome AS estado FROM cidades c, estados e WHERE c.estados_cod_estados = e.cod_estados AND c.cod_cidades = ".$acao_local;
        $query = $mysqli->query($sql);

        if ($result = $mysqli->query($sql)) {
          while ($dados = $query->fetch_array()) {
            $acao_local_estado = $dados['estado'];
            $acao_local_cidade = $dados['cidade'];
          }
        }
      }
    }

    if ($acao_estado_acao == 1){ // não terminou de incluir, ainda pode alterar

      //se for alterar os dados mas não é o dono do projeto, expulsa o usuário
      if($acao_coordenador <> $usuario_id){
        //
        echo "<script language='JavaScript'>window.location='../logout.php?erro=RESTRITO';</script><noscript>Se não for direcionado automaticamente, clique <a href='../logout.php?erro=RESTRITO'>aqui</a>.</noscript>";
        exit;
      }

      echo "<h3>Ação de Extensão: $acao_titulo</h3><hr>";

      $sql = "SELECT * FROM objetivos_especificos WHERE acao_extensao=".$acao_id;
      $query = $mysqli->query($sql);
      $count_obj_esp = $query->num_rows; // conta quantos objetivos especificos tem

      $sql = "SELECT ao.objetivo_especifico FROM acoes_objetivos ao, objetivos_especificos oe WHERE ao.objetivo_especifico = oe.id AND oe.acao_extensao =$acao_id GROUP BY ao.objetivo_especifico";
      $query = $mysqli->query($sql);
      $count_acoes_obj_esp = $query->num_rows; // conta quantos objetivos especificos tem alguma ação e resultado vinculado
/*
      $sql = "SELECT * FROM orcamentos WHERE acao_extensao=".$acao_id;
      $query = $mysqli->query($sql);
      $count_orcamentos = $query->num_rows;
*/
      $sql = "SELECT SUM(valor_solicitado) AS valor FROM orcamentos WHERE acao_extensao=".$acao_id;
      $query = $mysqli->query($sql);
      if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
          $orcamento_valor_total = $dados["valor"];
        }
      }
      $sql = "SELECT valor, data_validade FROM editais WHERE id=".$acao_edital;
      $query = $mysqli->query($sql);
      if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
          $edital_valor = $dados["valor"];
          $edital_data_validade = $dados["data_validade"];
          $data_atual = date("Y-m-d");
        }
      }

      $orcamento_diferenca_edital = $edital_valor - $orcamento_valor_total;

      $dados_iniciais_ok = FALSE;
      $objetivos_ok = FALSE;
      $orcamentos_ok = FALSE;

      if($count_acoes > 0){
        $dados_iniciais_ok = TRUE;
      }

      if($count_obj_esp > 0 && $count_acoes_obj_esp >= $count_obj_esp){
        $objetivos_ok = TRUE;
      }

      if($orcamento_valor_total > 0 && $orcamento_diferenca_edital >= 0){
        $orcamentos_ok = TRUE;
      }

      //Mostra a tela para inserir um novo edital
      $iconObjetivos = ($objetivos_ok) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ;
      $styleObjetivos = ($objetivos_ok) ? 'btn btn-primary btn-lg' : 'btn btn-warning btn-lg' ;

      $iconOrcamento = ($orcamentos_ok) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ;
      $styleOrcamento = ($orcamentos_ok) ? 'btn btn-primary btn-lg' : 'btn btn-warning btn-lg' ;
?>

<button onclick="window.location.href='editar.php?acao_id=<?php echo $acao_id; ?>'" class="btn btn-primary btn-lg" <?php echo ($acao_estado_acao == 1)?"":"disabled"?> >
  <span class="glyphicon glyphicon-map-marker"></span> Dados Iniciais <span class="glyphicon glyphicon-ok"></span>
</button>&nbsp

<button onclick="window.location.href='../objetivos/novo.php?acao_id=<?php echo $acao_id; ?>'" class="<?php echo $styleObjetivos; ?>" <?php echo ($acao_estado_acao == 1)?"":"disabled"?> >
  <span class="glyphicon glyphicon-screenshot"></span> Objetivos Específicos  <?php echo $iconObjetivos; ?>
</button>&nbsp

<button onclick="window.location.href='../orcamentos/editar.php?id=<?php echo $acao_id; ?>'" class="<?php echo $styleOrcamento; ?>" <?php echo ($acao_estado_acao == 1)?"":"disabled"?> >
  <span class="glyphicon glyphicon-usd"></span> Orçamento  <?php echo $iconOrcamento; ?>
</button>&nbsp


<?php
  if ($dados_iniciais_ok && $objetivos_ok && $orcamentos_ok) {
    echo "<a href='impressao_pdf.php?id=".$acao_id."' target='_blank' class='btn btn-danger btn-lg' role='button'><span class='glyphicon glyphicon-file'></span> Gerar PDF</a> ";
  }
?>
<hr>
<?php
  if ($data_atual <= $edital_data_validade) {
?>
<button class="btn btn-success btn-lg" <?php echo ($dados_iniciais_ok && $objetivos_ok && $orcamentos_ok) ? "" : "disabled" ; ?> data-toggle="modal" data-target="#myModal">
  <span class="glyphicon glyphicon-ok"></span>  Concluir Proposta
</button>
<?php
  }else{
?>
<button class="btn btn-warning btn-lg">Encerrado o prazo para envio de propostas</button>
<?php
  }
?>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Atenção!</h4>
      </div>
      <div class="modal-body">
        <p>Deseja concluir mesmo?</p>
        <p class="text-warning"><small>Se você concluir, não poderá fazer mais alterações</small></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="alterar_estado(<?php echo $acao_id ?>,2)">Concluir</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal HTML -->

<?php
    }else{ // exibe os dados que foram emviados

      if (!function_exists('convertCoin')) {
        function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
          $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
          $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
          $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
          return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
        }
      }

      foreach ($usuario_permissoes as $usuario_permissao) {

        switch ($usuario_permissao) {
          case ADMINISTRADOR: //Visão ADMINISTRADOR
            if($acao_estado_acao == 2 || $acao_estado_acao == 3){ // trabalhos com o estado ENVIADO ou EM ANALISE
?>
<h1><b>Desclassificar Projeto</b></h1><hr>
<form class="form-horizontal" name="desclassificar_projeto" id="desclassificar_projeto" method="post" accept-charset="utf-8">
  <input type="hidden" name="acao_extensao_id" value="<?php echo $acao_id;?>" />
  <input type="hidden" name="administrador_id" value="<?php echo $usuario_id;?>" />
  <div class="form-group">
    <div class="row col-sm-12">
      <label class="control-label col-sm-2" for="motivo">Motivo:</label>
      <div class="col-sm-8">
        <select class="form-control" name="motivo" id="motivo">
            <option value="">Selecione o motivo</option>
            <?php

            $sql = "SELECT *
                    FROM motivos_desclassificacao md
                    ORDER BY md.id ";
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
              while ($dados = $query->fetch_array()) {
                $motivo_id = $dados['id'];
                $motivo_nome = $dados['motivo'];
                echo "<option value='$motivo_id'>$motivo_nome</option>";
              }
            }

            ?>
        </select>
      </div>
      <button type="submit" class="btn btn-warning col-sm-2" id="btnEnviarEditor" value="Enviar editor">Desclassificar</button>
    </div><!--div row-->
  </div><!--div form-group-->
  <br>

</form>
<br>
<h1><b>Enviar para Conselheiro</b></h1><hr>
<form class="form-horizontal" name="enviar_editor" id="enviar_editor" method="post" accept-charset="utf-8">
  <input type="hidden" name="acao_extensao_id" value="<?php echo $acao_id;?>" />
  <input type="hidden" name="administrador_id" value="<?php echo $usuario_id;?>" />
  <div class="form-group">
    <div class="row col-sm-12">
      <label class="control-label col-sm-2" for="editor">Conselheiro:</label>
      <div class="col-sm-8">
        <select class="form-control" name="editor" id="editor">
            <option value="">Selecione o conselheiro</option>
            <?php

            $sql = "SELECT u.id, u.nome, u.matricula
                    FROM usuarios u, permissao_usuario pu
                    WHERE pu.usuario = u.id
                      AND pu.permissao = ".COMISSAO."
                      ORDER BY u.nome";
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
              while ($dados = $query->fetch_array()) {
                $editor_id = $dados['id'];
                $editor_nome = $dados['nome'];

                //mssql begin
                $query_dgrh = $pdo->prepare("SELECT local FROM siarh_sige WHERE matricula = :matricula");
                $query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
                $query_dgrh->execute();
                $dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
                $editor_local = $dgrh_dados['local'];
                //mssql end

                $sql_contador = "SELECT * FROM acoes_edicao WHERE id_editor = $editor_id";
                $query_contador = $mysqli->query($sql_contador);
                $count_acoes_editor = $query_contador->num_rows;

                echo "<option value='$editor_id'>($count_acoes_editor) $editor_nome - $editor_local</option>";
              }
            }

            ?>
        </select>
      </div>
      <button type="submit" class="btn btn-success col-sm-2" id="btnEnviarEditor" value="Enviar editor"><span class="glyphicon glyphicon-floppy-disk"></span> Enviar</button>
    </div><!--div row-->
  </div><!--div form-group-->
  <br>

</form>
<br>
<?php
            }
            if($acao_estado_acao == 5){ // trabalhos com o estado CLASSIFICADA
              //
              $sql_nota_total = "SELECT SUM(pa.nota) nota_total, SUM(10) nota_max FROM pergunta_avaliacao pa, acoes_avaliacao a WHERE pa.id_avaliacao = a.id AND a.id_acao_extensao = $acao_id";
              //$query_nota_total = $mysqli->query($sql_nota_total);
              if ($result_nota_total = $mysqli->query($sql_nota_total)) {
                while ($dados_nota_total = $result_nota_total->fetch_array()) {
?>
<h1><b>Nota Total: <?php echo $dados_nota_total['nota_total']."/".$dados_nota_total['nota_max']; ?></b></h1><hr>
<?php
                }
              }
?>
<form class="form-horizontal" name="avaliacao" id="avaliacao" method="post" accept-charset="utf-8">
  <input type="hidden" name="acao_extensao_id" value="<?php echo $acao_id;?>" />
  <input type="hidden" name="avaliador_id" value="<?php echo $usuario_id;?>" />

<?php

$sql = "SELECT p.pergunta, pa.nota, a.observacao FROM perguntas p, pergunta_avaliacao pa, acoes_avaliacao a WHERE p.id = pa.id_pergunta AND a.id = pa.id_avaliacao AND a.id_acao_extensao = $acao_id ORDER BY p.id ";
$query = $mysqli->query($sql);

$acao_avaliacao_observacao = "";

if ($result = $mysqli->query($sql)) {
  while ($dados = $query->fetch_array()) {
    $acao_avaliacao_observacao = $dados['observacao'];
?>
  <div class="form-group">
    <div class="col-sm-12">
      <h4><?php echo $dados['pergunta']; ?></h4>
      <p class="show_acao_p">Nota: <?php echo $dados['nota']; ?></p>
    </div>
  </div><!--div form-group-->
  <hr>
<?php
    }
}

?>
  <div class="form-group">
    <div class="col-sm-12">
      <h4 for="observacao">Observação</h4>
      <p class="show_acao_p"><?php echo $acao_avaliacao_observacao;?></p>
    </div>
    <br>
  </div><!--div form-group-->
  <hr>
  <!--<button type="submit" class="btn btn-success" id="btnSalvarAvaliacao" value="Salvar Avaliação"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>-->

</form>
<br>
<?php
            }
            break;
          case AVALIADOR: //Visão AVALIADOR
            if($acao_estado_acao == 4){ // trabalhos com o estado EM AVALIACAO
?>
<h1><b>Avaliar</b></h1><hr>
<form class="form-horizontal" name="avaliacao" id="avaliacao" method="post" accept-charset="utf-8">
  <input type="hidden" name="acao_extensao_id" value="<?php echo $acao_id;?>" />
  <input type="hidden" name="avaliador_id" value="<?php echo $usuario_id;?>" />

<?php

$sql = "SELECT * FROM perguntas p ORDER BY p.id ";
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
  while ($dados = $query->fetch_array()) {
?>
  <div class="form-group">
    <div class="col-sm-12">
      <h4><?php echo $dados['pergunta']; ?></h4>
      <label class="radio-inline"><input type="radio"  value="1" name="pergunta_<?php echo $dados['id']; ?>" > 1</input></label>
      <label class="radio-inline"><input type="radio"  value="2" name="pergunta_<?php echo $dados['id']; ?>" > 2</input></label>
      <label class="radio-inline"><input type="radio"  value="3" name="pergunta_<?php echo $dados['id']; ?>" > 3</input></label>
      <label class="radio-inline"><input type="radio"  value="4" name="pergunta_<?php echo $dados['id']; ?>" > 4</input></label>
      <label class="radio-inline"><input type="radio"  value="5" name="pergunta_<?php echo $dados['id']; ?>" > 5</input></label>
      <label class="radio-inline"><input type="radio"  value="6" name="pergunta_<?php echo $dados['id']; ?>" > 6</input></label>
      <label class="radio-inline"><input type="radio"  value="7" name="pergunta_<?php echo $dados['id']; ?>" > 7</input></label>
      <label class="radio-inline"><input type="radio"  value="8" name="pergunta_<?php echo $dados['id']; ?>" > 8</input></label>
      <label class="radio-inline"><input type="radio"  value="9" name="pergunta_<?php echo $dados['id']; ?>" > 9</input></label>
      <label class="radio-inline"><input type="radio"  value="10" name="pergunta_<?php echo $dados['id']; ?>" > 10</input></label>
    </div>
  </div><!--div form-group-->
  <hr>
<?php
  }
}

?>
  <div class="form-group">
    <div class="col-sm-12">
      <h4 for="observacao">Observação</h4>
      <textarea class="form-control" rows="3" name="observacao" maxlength="500" ></textarea>
    </div>
    <br>
  </div><!--div form-group-->
  <hr>
  <button type="submit" class="btn btn-success form-control" id="btnSalvarAvaliacao" value="Salvar Avaliação"><span class="glyphicon glyphicon-floppy-disk"></span> Enviar</button>
</form>
<br>
<br>
<?php
            }
            break;
          }
      }
?>

<!--<div class="container">-->

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#Dados">Dados <i class="fa"></i></a></li>
        <li><a href="#Areas" data-toggle="tab">Áreas <i class="fa"></i></a></li>
        <li><a href="#Linhas" data-toggle="tab">Linhas <i class="fa"></i></a></li>
        <li><a href="#Indicadores" data-toggle="tab">Dados Ind. <i class="fa"></i></a></li>
        <li><a href="#ObjEsp" data-toggle="tab">Objetivos especificos <i class="fa"></i></a></li>
        <li><a href="#Orcamento" data-toggle="tab">Orçamento <i class="fa"></i></a></li>
    </ul>

    <form role="form" enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

        <div class="tab-content">
            <div class="tab-pane fade in active" id="Dados">
                <h3>Dados do projeto</h3>
                <hr>
<?php
      $ver_nome_coordenador = FALSE;
      $ver_motivo_desclassificacao = FALSE;
      foreach ($usuario_permissoes as $usuario_permissao) {
        if($usuario_permissao == ADMINISTRADOR || $usuario_permissao == COMISSAO || $usuario_permissao == COORDENADOR){
          $ver_nome_coordenador = TRUE;
        }
        if($usuario_permissao == ADMINISTRADOR && $acao_estado_acao == 6){
          $ver_motivo_desclassificacao = TRUE;
        }
      }
      if($ver_motivo_desclassificacao){
        $sql_motivo_desclassificacao = "SELECT * FROM motivos_desclassificacao md, acoes_desclassificacao ad WHERE ad.motivo_id = md.id AND ad.acao_extensao_id = $acao_id ";

        if ($query_motivo_desclassificacao = $mysqli->query($sql_motivo_desclassificacao)) {
          while ($dados_motivo_desclassificacao = $query_motivo_desclassificacao->fetch_array()) {
            $acao_motivo_desclassificacao = $dados_motivo_desclassificacao['motivo'];
          }
        }
?>
                <div class="form-group bg-danger">
                    <label class="col-sm-2 control-label" for="motivo">Motivo desclassificação:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_motivo_desclassificacao;?></p>
                    </div>
                </div><!--div form-group-->
<?php
      }
      if($ver_nome_coordenador){
        $sql_nome_coordenador = "SELECT * FROM usuarios u WHERE id = $acao_coordenador ";
        $query_nome_coordenador = $mysqli->query($sql_nome_coordenador);

        if ($result_nome_coordenador = $mysqli->query($sql_nome_coordenador)) {
          while ($dados_nome_coordenador = $query_nome_coordenador->fetch_array()) {
            $acao_coordenador_nome = $dados_nome_coordenador['nome'];
          }
        }
?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="coordenador">Coordenador:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_coordenador_nome;?></p>
                    </div>
                </div><!--div form-group-->
<?php
      }
?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="titulo">Título:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_titulo;?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-2" for="tipo_extensao">Tipo de Extensão:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p">Projeto</p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Local</h4>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="estado">Estado:</label>
                    <div class="col-sm-3">
                        <p class='show_acao_p'><?php echo $acao_local_estado ?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-2" for="cidade">Cidade:</label>
                    <div class="col-sm-3">
                        <p class='show_acao_p'><?php echo $acao_local_cidade ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Comunidade Participante</h4>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="n_pessoas_alvo_interno">Número de pessoas da UNICAMP que serão envolvidas:</label>
                    <div class="col-sm-2">
                        <p class="show_acao_p"><?php echo $acao_n_pessoas_alvo_interno;?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-3" for="n_pessoas_alvo_externo">Número de pessoas externas que serão envolvidas:</label>
                    <div class="col-sm-2">
                        <p class="show_acao_p"><?php echo $acao_n_pessoas_alvo_externo;?></p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group">
                    <label class="control-label col-sm-3" for="caracterizacao_comunidade">Realidade social, econômica e cultural da Comunidade</label>
                    <div class="col-sm-9">
                        <p class="show_acao_p"><?php echo $acao_caracterizacao_comunidade;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>O projeto já está em execução?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo ($acao_em_execucao == 'Sim')? "Sim" : "Não"; ?></p>
                    </div>
                </div><!--div form-group-->
              <?php if ($acao_em_execucao == 'Sim'){ ?>
                <div class="form-group execucao">
                    <label class="control-label col-sm-2" for="quanto_tempo">Há quanto tempo?</label>
                    <div class="col-sm-3">
                        <p class="show_acao_p"><?php echo $acao_quanto_tempo ?> meses</p>
                    </div>
                </div><!--div form-group-->

                <div class="form-group execucao">
                    <label class="control-label col-sm-2" for="descricao">Comentário:</label>
                    <div class="col-sm-10">
                        <p class="show_acao_p"><?php echo $acao_em_execucao_descricao ?></p>
                    </div>
                </div><!--div form-group-->
              <?php } ?>
                <hr>

                <h4>Grau de envolvimento da equipe com a Comunidade</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                    <?php
                      $sql = "SELECT descricao FROM graus_envolvimento_equipe WHERE id = $acao_grau_envolvimento_equipe";
                      $query = $mysqli->query($sql);

                      if ($result = $mysqli->query($sql)) {
                        while ($dados = $query->fetch_array()) {
                    ?>
                        <p class="show_acao_p"><?php echo $dados['descricao'] ?></p>
                    <?php
                        }
                      }
                    ?>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>Há parcerias com outras instituições (públicas ou privadas) para o desenvolvimento do projeto?</h4>

                <div class="form-group">
                    <div class="row col-sm-12">
                        <div class="col-sm-2">
                            <p class="show_acao_p"><?php echo (!empty($acao_parceria_instituicao) && $acao_parceria_instituicao != 'Não')?"Sim":"Não" ?></p>
                        </div>
                    <?php if (!empty($acao_parceria_instituicao) && $acao_parceria_instituicao != 'Não'){ ?>
                        <div id="parceria_instituicao">
                            <label class="control-label col-sm-1" for="indicador10_como">Especifique</label>
                            <div class="col-sm-9">
                                <p class="show_acao_p"><?php echo $acao_parceria_instituicao?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div><!--div row-->
                </div><!--div form-group-->

                <hr>

                <h4>Introdução</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class='show_acao_p'><?php echo $acao_introducao;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Objetivo Geral</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class='show_acao_p'><?php echo $acao_objetivo_geral;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>Metodologia</h4>

                <div class="form-group">
                        <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_metodologia;?></p>
                    </div>

                </div><!--div form-group-->

                <hr>
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

            </div><!--div tab-pane-->

            <div id="Areas" class="tab-pane fade">

                <h3>Áreas Temáticas</h3>
                <hr>

                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                        $sql = "SELECT * FROM areas_tematicas at, acao_tematica a WHERE at.id = a.area_tematica AND a.acao_extensao = $acao_id";
                        $query = $mysqli->query($sql);

                        if ($result = $mysqli->query($sql)) {
                          while ($dados = $query->fetch_array()) {
                            $area_nome = $dados['nome'];

                            echo "<p class='show_acao_p'> $area_nome</p>";

                          }
                        }
                        ?>
                        <br>
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

            </div><!--div tab-pane-->

            <div id="Linhas" class="tab-pane fade">

                <h3>Linhas de Extensão</h3>
                <hr>

                <div class="form-group">
                    <div class="col-sm-12">
                        <?php
                        $sql = "SELECT * FROM linhas_extensao WHERE id = $acao_linha_extensao";
                        $query = $mysqli->query($sql);

                        if ($result = $mysqli->query($sql)) {
                          while ($dados = $query->fetch_array()) {
                            $linha_nome = $dados['nome'];
                            $linha_descricao = $dados['descricao'];
                            echo "<p class='show_acao_p' data-toggle='tooltip' data-placement='bottom' title='$linha_descricao'>$linha_nome</p>";
                          }
                        }
                        ?>
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>
            </div><!--div tab-pane-->

            <div id="Indicadores" class="tab-pane fade">

                <h3 class="titulo">Dados Indicadores</h3>
                <hr>

                <h4>1. Por que o projeto deve ser enquadrado como iniciativa de extensão comunitária?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador1;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>2. Quais as contribuições do projeto para suas atividades de ensino, já realizadas ou potenciais?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador2 ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>

                <h4>3. Quais as contribuições do projeto para suas atividades de pesquisa, já realizadas ou potenciais?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador3;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>4. Quais as contribuições esperadas do projeto para a formação acadêmica, profissional e cidadã dos alunos envolvidos?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador4;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>5. De que maneiras a comunidade externa à Unicamp será envolvida no projeto?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador5 ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>6. Quais resultados e impactos de longo prazo o projeto deve trazer para a comunidade/sociedade?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador6;?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>7. O projeto tem potencial para gerar novos conhecimentos, técnicas, metodologias ou formas de organização do trabalho e da gestão?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador7 ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>8. De que forma será feito o acompanhamento do projeto e sua avaliação pela equipe responsável?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador8 ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>9. Como o projeto pode contribuir para o estreitamento da relação entre a universidade e a sociedade?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador9 ?></p>
                    </div>
                </div><!--div form-group-->

                <hr>
                <h4>10. O projeto contribui com a constituição de redes que envolvam a produção, difusão e uso de conhecimentos e envolve outros parceiros e apoiadores?</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="show_acao_p"><?php echo $acao_indicador10;?></p>
                    </div>
                </div><!--div form-group-->

                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

                <br>

            </div><!--div tab-pane-->

            <div id="ObjEsp" class="tab-pane fade">

                <h3>Objetivos Específicos</h3>

                <hr>

                <table class="table table-striped">

                    <thead>
                      <tr class="tabela_cabecalho">
                        <th>Objetivo Específico</th>
                        <th>Ação</th>
                        <th>Resultado</th>
                      </tr>
                    </thead>

                    <tbody>



                <?php

                    $sql = "SELECT oe.id as obj_esp_id, oe.descricao as obj_esp_descricao, ao.id as acao_esp_id, ao.descricao as acao, ao.resultado as resultado FROM objetivos_especificos oe LEFT JOIN acoes_objetivos ao ON oe.id = ao.objetivo_especifico WHERE oe.acao_extensao=".$acao_id;

                    $query = $mysqli->query($sql);

                    $verifica_descricao_diferente = "";

                    if ($result = $mysqli->query($sql)) {
                      while ($dados = $query->fetch_array()) {

                        $sql_acao = "SELECT * FROM acoes_objetivos WHERE objetivo_especifico=".$dados['obj_esp_id'];
                        $query_acao = $mysqli->query($sql_acao);
                        $count_acoes_esp = $query_acao->num_rows;


                ?>
                    <tr>
                <?php
                        if ($verifica_descricao_diferente != $dados['obj_esp_descricao']){
                          $verifica_descricao_diferente = $dados['obj_esp_descricao'];

                          $apagar_link =  $dados['obj_esp_id'].",".$acao_id;
                ?>
                      <td rowspan="<?php echo ($count_acoes_esp == 0) ? 1 : $count_acoes_esp ; ?>"><?php echo $dados['obj_esp_descricao'] ?></td>
                <?php
                        }
                ?>
                      <td><?php echo $dados['acao'] ?></td>
                      <td><?php echo $dados['resultado'] ?></td>

                    </tr>
                <?php
                      }
                    }
                ?>
                  </tbody>
                </table>

                <hr>
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                        <li ><button type="button" class="btn btn-success btnProximo btn-lg">Próximo ></button></li>
                    </ul>
                </div>

                <br>

            </div><!--div tab-pane-->

            <div id="Orcamento" class="tab-pane fade">

                <h3>Orçamento</h3>

                <hr>

                <table class="table table-striped">

                    <thead>
                      <tr class="tabela_cabecalho">
                        <th>Nome do Item</th>
                        <th>Valor</th>
                      </tr>
                    </thead>

                    <tbody>

                <?php

                    $sub_total_orcamento = 0;
                    $total_orcamento = 0;

                    $sql_tipo_orcamento = "SELECT * FROM tipo_orcamento ORDER by id";
                    $query_tipo_orcamento = $mysqli->query($sql_tipo_orcamento);

                    if ($result_tipo_orcamento = $mysqli->query($sql_tipo_orcamento)) {
                      while ($dados_tipo_orcamento = $query_tipo_orcamento->fetch_array()) {

                        $tipo_orcamento_id = $dados_tipo_orcamento['id'];
                        $tipo_orcamento_nome = $dados_tipo_orcamento['nome'];
                        echo "<tr><th COLSPAN=2 class='tabela_subtitulo'>$tipo_orcamento_nome</th></tr>";

                        $sql_item_orcamento = "SELECT o.id, io.nome, o.valor_solicitado FROM orcamentos o, item_orcamento io WHERE o.item_orcamento = io.id AND o.acao_extensao = $acao_id AND io.tipo = $tipo_orcamento_id";

                        $query_item_orcamento = $mysqli->query($sql_item_orcamento);

                        if ($result_item_orcamento = $mysqli->query($sql_item_orcamento)) {
                          while ($dados_item_orcamento = $query_item_orcamento->fetch_array()) {

                            $sub_total_orcamento += floatval($dados_item_orcamento['valor_solicitado']);
                            $item_orcamento_nome = $dados_item_orcamento['nome'];
                            $item_orcamento_valor_solicitado = convertCoin("BR",2,$dados_item_orcamento['valor_solicitado']);

                            echo "<tr><td>$item_orcamento_nome</td><td>$item_orcamento_valor_solicitado</td></tr>";

                          }
                          echo "<tr><th>Sub total: </th><th>".number_format($sub_total_orcamento,2,",",".")."</th></tr>";
                        }
                        $total_orcamento += $sub_total_orcamento;
                        $sub_total_orcamento = 0;
                      }
                    echo '<tr class="tabela_total"><th class="tabela_total">Total:</th><th class="tabela_total">'.number_format($total_orcamento,2,",","."). '</th></tr>';
                    }

                ?>
                </tbody>
                </table>
                <hr>
                <div>
                    <ul class="pager">
                        <li ><button type="button" class="btn btn-default btnAnterior">< Anterior</button></li>
                    </ul>
                </div>

                <br>

            </div><!--div tab-pane-->

        </div><!--div tab-content-->
    </form>
<!--</div>--> <!--div container-->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$('.btnProximo').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});

$('.btnAnterior').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  // deslocar a página para o topo das tabs
  var deslocamento = $('.container').offset().top;
  $('html, body').scrollTop(deslocamento);
  // fim deslocamento
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#avaliacao')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
<?php
$sql = "SELECT * FROM perguntas p ";
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
    while ($dados = $query->fetch_array()) {
?>
                pergunta_<?php echo $dados['id'] ;?>: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha uma nota'
                        }
                    }
                },
<?php
    }
  }
?>
                observacao: {
                    validators: {
                        stringLength: {
                            max: 500,
                            message: 'A observação deve ter no máximo 500 caracteres'
                        }
                    }
                }
            }
        }).on('status.field.bv', function(e, data) {
<?php
$sql = "SELECT * FROM perguntas p ";
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
    while ($dados = $query->fetch_array()) {
?>
            if (data.field === 'pergunta_<?php echo $dados['id'] ;?>') {
                // Remove the success class from the container
                data.element.closest('.form-group').removeClass('has-success');

                // Hide the tick icon
                data.element.data('bv.icon').hide();
            }
<?php
    }
  }
?>
        });

    $('#desclassificar_projeto')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                motivo: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha um motivo'
                        }
                    }
                }
            }
        });
    $('#enviar_editor')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                editor: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha um conselheiro'
                        }
                    }
                }
            }
        });
    $('#enviar_avaliador')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                avaliador: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha um avaliador'
                        }
                    }
                }
            }
        });
});
</script>

<?php
    }
  }
}
?>
</div>
