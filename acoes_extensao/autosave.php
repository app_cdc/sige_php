<?php
include "../includes/conecta.php";

if (!isset($_SESSION)){
  session_cache_expire(43200);
  session_start();
} 

$acao_id ='';
$acao_titulo ='';
$acao_edital ='';
$acao_estado_acao ='';
$acao_coordenador ='';
$acao_criacao ='';
$acao_alteracao ='';
$acao_tipo_extensao ='';
$acao_indicador1 ='';
$acao_indicador2 ='';
$acao_indicador3 ='';
$acao_indicador4 ='';
$acao_indicador5 ='';
$acao_indicador6 ='';
$acao_indicador7 ='';
$acao_indicador8 ='';
$acao_indicador9 ='';
$acao_indicador10 ='';
$acao_metodologia ='';
$acao_local ='';
$acao_ano_inicio ='';
$acao_inicio ='';
$acao_termino ='';
$acao_situacao ='';
$acao_n_pessoas_alvo_interno ='';
$acao_n_pessoas_alvo_externo ='';
$acao_caracterizacao_comunidade ='';
$acao_introducao = '';
$acao_objetivo_geral ='';
$acao_grau_envolvimento_equipe ='';
$acao_parceria_instituicao ='';
$acao_em_execucao = '';
$acao_quanto_tempo = '';
$acao_em_execucao_descricao = '';
$acao_linha_extensao = "";

// pega os dados preenchidos no formulario
$acao_id                        = $_POST['acao_id'];
$acao_titulo                    = $mysqli->real_escape_string($_POST['titulo']);
$acao_edital                    = $_POST['edital_id'];
$edital_codigo                  = $_POST['edital_codigo'];
$acao_coordenador               = $_SESSION['UsuarioID'];
$acao_tipo_extensao             = 1; // 1 = projeto // $_POST['tipo_extensao'];
$acao_indicador1                = $mysqli->real_escape_string($_POST['indicador1']);
$acao_indicador2                = $mysqli->real_escape_string($_POST['indicador2']);
$acao_indicador3                = $mysqli->real_escape_string($_POST['indicador3']);
$acao_indicador4                = $mysqli->real_escape_string($_POST['indicador4']);
$acao_indicador5                = $mysqli->real_escape_string($_POST['indicador5']);
$acao_indicador6                = $mysqli->real_escape_string($_POST['indicador6']);
$acao_indicador7                = $mysqli->real_escape_string($_POST['indicador7']);
$acao_indicador8                = $mysqli->real_escape_string($_POST['indicador8']);
$acao_indicador9                = $mysqli->real_escape_string($_POST['indicador9']);
$acao_indicador10               = $mysqli->real_escape_string($_POST['indicador10']);

$acao_metodologia               = $mysqli->real_escape_string($_POST['metodologia']);
$acao_local                     = $_POST['cidade'];
$acao_n_pessoas_alvo_interno    = $_POST['n_pessoas_alvo_interno'];
$acao_n_pessoas_alvo_externo    = $_POST['n_pessoas_alvo_externo'];
$acao_caracterizacao_comunidade = $mysqli->real_escape_string($_POST['caracterizacao_comunidade']);
$acao_introducao                = $mysqli->real_escape_string($_POST['introducao']);
$acao_objetivo_geral            = $mysqli->real_escape_string($_POST['objetivo_geral']);
$acao_grau_envolvimento_equipe  = $_POST['grau_envolvimento_equipe'];
// se o indicador tiver um campo "Como?", verifica se foi selecionado "Sim" e pega o valor preenchido no campo "Como?"
if ($_POST['parceria_instituicao'] == 'Sim'){
    $acao_parceria_instituicao           = $mysqli->real_escape_string($_POST['parceria_instituicao_especificar']);
}else{
    $acao_parceria_instituicao      = $_POST['parceria_instituicao'];
};
$acao_em_execucao               = $mysqli->real_escape_string($_POST['projeto_em_execucao']);
$acao_quanto_tempo              = $mysqli->real_escape_string($_POST['quanto_tempo']);
$acao_em_execucao_descricao     = $mysqli->real_escape_string($_POST['descricao']);
$acao_linha_extensao            = $_POST['linha_extensao'];

if($acao_em_execucao == "Não"){
    $acao_quanto_tempo              = "";
    $acao_em_execucao_descricao     = "";
}


$sql = "SELECT * FROM acoes_extensao_rascunho WHERE coordenador = ".$acao_coordenador." AND edital =".$acao_edital;
$query = $mysqli->query($sql);
if($query->num_rows > 0)
{
  $sql = "UPDATE acoes_extensao_rascunho 
          SET titulo = '$acao_titulo',
              coordenador = '$acao_coordenador',
              tipo_extensao = '$acao_tipo_extensao',
              indicador1 = '$acao_indicador1',
              indicador2 = '$acao_indicador2',
              indicador3 = '$acao_indicador3',
              indicador4 = '$acao_indicador4',
              indicador5 = '$acao_indicador5',
              indicador6 = '$acao_indicador6',
              indicador7 = '$acao_indicador7',
              indicador8 = '$acao_indicador8',
              indicador9 = '$acao_indicador9',
              indicador10 = '$acao_indicador10',
              metodologia = '$acao_metodologia',
              local = '$acao_local',
              n_pessoas_alvo_interno = '$acao_n_pessoas_alvo_interno',
              n_pessoas_alvo_externo = '$acao_n_pessoas_alvo_externo',
              caracterizacao_comunidade = '$acao_caracterizacao_comunidade',
              introducao = '$acao_introducao',
              objetivo_geral = '$acao_objetivo_geral',
              grau_envolvimento_equipe = '$acao_grau_envolvimento_equipe',
              parceria_instituicao = '$acao_parceria_instituicao',
              em_execucao = '$acao_em_execucao',
              quanto_tempo = '$acao_quanto_tempo',
              em_execucao_descricao = '$acao_em_execucao_descricao',
              linha_extensao = '$acao_linha_extensao'
          WHERE coordenador = $acao_coordenador 
            AND edital = $acao_edital";
} 
else 
{ 
/*Insert the variables into the database*/ 
  $sql = "INSERT INTO acoes_extensao_rascunho 
                                        (titulo,
                                        edital,
                                        coordenador,
                                        tipo_extensao,
                                        indicador1,
                                        indicador2,
                                        indicador3,
                                        indicador4,
                                        indicador5,
                                        indicador6,
                                        indicador7,
                                        indicador8,
                                        indicador9,
                                        indicador10,
                                        metodologia,
                                        local,
                                        n_pessoas_alvo_interno,
                                        n_pessoas_alvo_externo,
                                        caracterizacao_comunidade,
                                        introducao,
                                        objetivo_geral,
                                        grau_envolvimento_equipe,
                                        parceria_instituicao,
                                        em_execucao,
                                        quanto_tempo,
                                        em_execucao_descricao,
                                        linha_extensao) 
                            VALUES ('$acao_titulo',
                                    '$acao_edital',
                                    '$acao_coordenador',
                                    '$acao_tipo_extensao',
                                    '$acao_indicador1',
                                    '$acao_indicador2',
                                    '$acao_indicador3',
                                    '$acao_indicador4',
                                    '$acao_indicador5',
                                    '$acao_indicador6',
                                    '$acao_indicador7',
                                    '$acao_indicador8',
                                    '$acao_indicador9',
                                    '$acao_indicador10',
                                    '$acao_metodologia',
                                    '$acao_local',
                                    '$acao_n_pessoas_alvo_interno',
                                    '$acao_n_pessoas_alvo_externo',
                                    '$acao_caracterizacao_comunidade',
                                    '$acao_introducao',
                                    '$acao_objetivo_geral',
                                    '$acao_grau_envolvimento_equipe',
                                    '$acao_parceria_instituicao',
                                    '$acao_em_execucao',
                                    '$acao_quanto_tempo',
                                    '$acao_em_execucao_descricao',
                                    '$acao_linha_extensao')";
}

if ($mysqli->query($sql) === FALSE) {
  $mysqli->rollback();
}else{
  $mysqli->commit();
}
$mysqli->close();

?>

