<?php
include "../includes/conecta.php";
include('../vendor/mpdf/mpdf/mpdf.php');

ini_set('display_errors', '');
error_reporting(0);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
     $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
     $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
     $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
     return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}

if (isset($_GET['id'])) {

	$html = "";

	$id_acao = "";

	$titulo = "";

	$acao_id = $_GET['id'];

	$sql = "SELECT * FROM acoes_extensao WHERE id=".$acao_id;
	$query = $mysqli->query($sql);

	if ($result = $mysqli->query($sql)) {
		while ($dados = $query->fetch_array()) {

		$id_acao = $dados['id'];
		$titulo = $mysqli->real_escape_string($dados['titulo']);
		$edital = $dados['edital'];

		$html .= "<style type='text/css'>
					body{
						font-family: Arial;
					}
					fieldset{
						width: 700px;
						margin: -1px auto;
						color: #444;
						border: 1px solid #ccc;
						padding: 15px;
						padding-bottom: 5px;
						padding-top: 5px;
						font-size: 12px;
					}

					h1{
						text-align: center;
					}
					body{
						display: inline;
					}

					p.sub-titulo{
						color: #444;
						font-size: 16px;
						font-weight: bold;
						display: inline;
					}

					.direita{
						text-align: right;
					}

					.center{
						text-align: center;
					}
					.objetivos {
							width:718px;
							margin: -10px -16px -6px -16px;
							border:1px solid #C0C0C0;
							border-collapse:collapse;
							padding:5px;
							color: #444;
							font-size: 12px;
						}
						.objetivos th {
							border:1px solid #C0C0C0;
							padding:5px;
							background:#555;
							color: #ffffff;
						}
						.objetivos td {
							font-size: 12px;
							border:1px solid #C0C0C0;
							padding:7px;
						}
					.orcamento {
							width:718px;
							margin: -10px -16px -6px -16px;
							border:1px solid #C0C0C0;
							border-collapse:collapse;
							padding:5px;
							color: #444;
							font-size: 12px;
						}
						.orcamento th {
							border:1px solid #C0C0C0;
							padding:5px;
							text-align: left;
						}
						.orcamento td {
							border:1px solid #C0C0C0;
							padding:5px;
						}

					</style>";

			$sql_edital = "SELECT * FROM editais e WHERE e.id = $edital";

			if ($result_edital = $mysqli->query($sql_edital)) {
				while ($dados_edital = $result_edital->fetch_array()) {
					$html .= "<fieldset><img src='../style/images/".$dados_edital['cabecalho_relatorio']."' style='width: 100%'></fieldset>";
				}
			}




			$html .= "<fieldset><b>I.	Protocolo de inscrição nº:</b> ".$dados["id"]."</fieldset>";
			$html .= "<fieldset><b>II.	Título do projeto:</b> ".$dados['titulo']."</fieldset>";

			$html .= "<fieldset><b>III.	Área temática: </b>";

			$sql_area = "SELECT t.nome FROM areas_tematicas t, acao_tematica a WHERE a.area_tematica = t.id AND a.acao_extensao = ".$dados['id'];
			$query_area = $mysqli->query($sql_area);

			if ($result_area = $mysqli->query($sql_area)) {
				while ($dados_area = $query_area->fetch_array()) {
					$html .= $dados_area['nome']."; ";
				}
			}
			$html .= "</fieldset>";

			$html .= "<fieldset><b>IV.	Linha de Extensão: </b>";

			$sql_linha = "SELECT * FROM linhas_extensao WHERE id = ".$dados['linha_extensao'];
			$query_linha = $mysqli->query($sql_linha);

			if ($result_linha = $mysqli->query($sql_linha)) {
				while ($dados_linha = $query_linha->fetch_array()) {
					$html .= $dados_linha['nome'];
				}
			}
			$html .= "</fieldset>";

			$html .= "<fieldset><b>V.	Introdução: </b>";
			$html .= $dados['introducao']."</fieldset>";

			$html .= "<fieldset><b>VI.	Objetivo Geral: </b>";
			$html .= $dados['objetivo_geral']."<br>&nbsp;<br>";

			$sql_objetivo_especifico = "SELECT oe.id as obj_esp_id, oe.descricao as obj_esp_descricao, ao.id as acao_esp_id, ao.descricao as acao, ao.resultado as resultado FROM objetivos_especificos oe LEFT JOIN acoes_objetivos ao ON oe.id = ao.objetivo_especifico WHERE oe.acao_extensao=".$dados['id'];
			//$sql_objetivo_especifico = "SELECT * FROM objetivos_especificos oe WHERE oe.acao_extensao=".$dados['id'];
			$query_objetivo_especifico = $mysqli->query($sql_objetivo_especifico);

			$verifica_descricao_diferente = "";

			$html .= "";
			$html .= 	"<table class='objetivos'>";
			$html .= 	"<thead>";
			$html .= 		"<tr>";
			$html .= 			"<th>VII.	Objetivos Específicos</th>";
			$html .= 			"<th>Ação</th>";
			$html .= 			"<th>Resultados esperados</th>";
			$html .= 		"</tr>";
			$html .= 	"</thead>";
			$html .= 	"<tbody>";
			if ($result_objetivo_especifico = $mysqli->query($sql_objetivo_especifico)) {
				while ($dados_objetivo_especifico = $query_objetivo_especifico->fetch_array()) {

   					$sql_acao_especifico = "SELECT * FROM acoes_objetivos WHERE objetivo_especifico =".$dados_objetivo_especifico['obj_esp_id'];
					$query_acao_especifico = $mysqli->query($sql_acao_especifico);

					$count_acoes_esp = $query_acao_especifico->num_rows;

			$html .= 		"<tr>";
					if ($verifica_descricao_diferente != $dados_objetivo_especifico['obj_esp_descricao']){
          				$verifica_descricao_diferente = $dados_objetivo_especifico['obj_esp_descricao'];

			$html .= 			"<td rowspan='$count_acoes_esp'>".$dados_objetivo_especifico['obj_esp_descricao']."</td>";
					}

			$html .= 			"<td>".$dados_objetivo_especifico['acao']."</td>";
			$html .= 			"<td>".$dados_objetivo_especifico['resultado']."</td>";
			$html .= 		"</tr>";
				}
			}
			$html .= 	"</tbody>";
			$html .= 	"</table>";
			$html .= "</fieldset>";

			$html .= "<fieldset><b>VIII.	Comunidade Participante:</b><br>";
			$html .= "<b>Número de pessoas da UNICAMP que serão envolvidas:</b> ".$dados['n_pessoas_alvo_interno']."&nbsp;<br>";
			$html .= "<b>Número de pessoas externas que serão envolvidas:</b> ".$dados['n_pessoas_alvo_externo']."&nbsp;<br>";
			$html .= "<b>Local:</b> ";

			$sql_local = "SELECT c.nome AS cidade, e.nome AS estado FROM cidades c, estados e WHERE c.estados_cod_estados = e.cod_estados AND c.cod_cidades = ".$dados['local'];
			$query_local = $mysqli->query($sql_local);

			if ($result_local = $mysqli->query($sql_local)) {
				while ($dados_local = $query_local->fetch_array()) {
					$html .= $dados_local['cidade']." - ".$dados_local['estado']."<br>";
				}
			}
			$html .= "<b>Realidade social, econômica e cultural:</b> ".$dados['caracterizacao_comunidade']."</b></fieldset>";

			$html .= "<fieldset><b>IX.	Metodologia</b>: ".$dados['metodologia']."</fieldset>";

			$html .= "<fieldset><b>X.	O projeto já está em execução? </b>";
			$html .= $dados['em_execucao'].". ";
			if ($dados['em_execucao'] == 'Sim') {
				$html .= "<b>Há quanto tempo?</b> ".$dados['quanto_tempo']." meses. ";
				$html .= "<b>Descrição:</b> ".$dados['em_execucao_descricao'];
			}
			$html .= "</fieldset>";

			$html .= "<fieldset><b>XI.	Grau de envolvimento da equipe com a Comunidade: </b>";
			$sql_grau_envolvimento = "SELECT * FROM graus_envolvimento_equipe WHERE id=".$dados['grau_envolvimento_equipe'];
			$query_grau_envolvimento = $mysqli->query($sql_grau_envolvimento);

			if ($result_grau_envolvimento = $mysqli->query($sql_grau_envolvimento)) {
				while ($dados_grau_envolvimento = $query_grau_envolvimento->fetch_array()) {
					$html .= $dados_grau_envolvimento['descricao'];
				}
			}
			$html .= "</fieldset>";



			$html .= "<fieldset><b>XII.	Dados para Indicadores</b><br>";
			$html .= "<b>1. Por que o projeto deve ser enquadrado como iniciativa de extensão comunitária? </b>".$dados['indicador1']."<br><br>";
			$html .= "<b>2. Quais as contribuições do projeto para suas atividades de ensino, já realizadas ou potenciais? </b>".$dados['indicador2']."<br><br>";
			$html .= "<b>3. Quais as contribuições do projeto para suas atividades de pesquisa, já realizadas ou potenciais? </b>".$dados['indicador3']."<br><br>";
			$html .= "<b>4. Quais as contribuições esperadas do projeto para a formação acadêmica, profissional e cidadã dos alunos envolvidos? </b>".$dados['indicador4']."<br><br>";
			$html .= "<b>5. De que maneiras a comunidade externa à Unicamp será envolvida no projeto? </b>".$dados['indicador5']."<br><br>";
			$html .= "<b>6. Quais resultados e impactos de longo prazo o projeto deve trazer para a comunidade/sociedade? </b>".$dados['indicador6']."<br><br>";
			$html .= "<b>7. O projeto tem potencial para gerar novos conhecimentos, técnicas, metodologias ou formas de organização do trabalho e da gestão? </b>".$dados['indicador7']."<br><br>";
			$html .= "<b>8. De que forma será feito o acompanhamento do projeto e sua avaliação pela equipe responsável? </b>".$dados['indicador8']."<br><br>";
			$html .= "<b>9. Como o projeto pode contribuir para o estreitamento da relação entre a universidade e a sociedade? </b>".$dados['indicador9']."<br><br>";
			$html .= "<b>10. O projeto contribui com a constituição de redes que envolvam a produção, difusão e uso de conhecimentos e envolve outros parceiros e apoiadores? </b>".$dados['indicador10']."<br><br>";

			$html .= "</fieldset>";
			$html .= "<fieldset><b>XIII.	Orçamento Financeiro</b><br>&nbsp;<br>";

			$html .= "<table class='orcamento'>";
			$html .= 	"<thead>";
			$html .= 		"<tr>";
        	$html .= 			"<th style='background:#555; color:#ffffff'>Nome do Item</th>";
        	$html .= 			"<th style='background:#555; color:#ffffff'>Valor</th>";
      		$html .= 		"</tr>";
    		$html .= 	"</thead>";
			$html .= 	"<tbody>";

		    $sub_total_orcamento = 0;
		    $total_orcamento = 0;

		    $sql_tipo_orcamento = "SELECT * FROM tipo_orcamento ORDER by id";
		    $query_tipo_orcamento = $mysqli->query($sql_tipo_orcamento);

		    if ($result_tipo_orcamento = $mysqli->query($sql_tipo_orcamento)) {
		      while ($dados_tipo_orcamento = $query_tipo_orcamento->fetch_array()) {

		        $tipo_orcamento_id = $dados_tipo_orcamento['id'];
		        $tipo_orcamento_nome = $dados_tipo_orcamento['nome'];
		        $html .= "<tr><th COLSPAN='2'>$tipo_orcamento_nome</th></tr>";

		        $sql_item_orcamento = "SELECT o.id, io.nome, o.valor_solicitado FROM orcamentos o, item_orcamento io WHERE o.item_orcamento = io.id AND o.acao_extensao = $acao_id AND io.tipo = $tipo_orcamento_id";

		        $query_item_orcamento = $mysqli->query($sql_item_orcamento);

		        if ($result_item_orcamento = $mysqli->query($sql_item_orcamento)) {
		          while ($dados_item_orcamento = $query_item_orcamento->fetch_array()) {

		            $sub_total_orcamento += floatval($dados_item_orcamento['valor_solicitado']);
		            $item_orcamento_nome = $dados_item_orcamento['nome'];
		            $item_orcamento_valor_solicitado = convertCoin("BR",2,$dados_item_orcamento['valor_solicitado']);

		            $html .= "<tr><td>$item_orcamento_nome</td>";
		            $html .= "<td>$item_orcamento_valor_solicitado</td>";
		            $html .= "</tr>";

		          }
		          $html .= "<tr><th>Sub total: </th><th>".number_format($sub_total_orcamento,2,",",".")."</th></tr>";
		        }
		        $total_orcamento += $sub_total_orcamento;
		        $sub_total_orcamento = 0;
		      }
		    $html .= '<tr><th>Total:</th><th>'.number_format($total_orcamento,2,",","."). '</th></tr>';
		    }

			$html .= 	"</tbody>";
			$html .= "</table>";
			$html .= "</fieldset>";





		}
	}

$mpdf=new mPDF();

$mpdf->SetDisplayMode('fullpage');

        $mpdf->AddPage('P', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            5, // margin top
            20, // margin bottom
            10, // margin header
            10);
            $footer = "<table width=\"1000\">
                   <tr>
                   	<td tyle='font-size: 18px;' align=\"left\">Protocolo de inscrição nº: ".$id_acao. "</td>
                     <td style='font-size: 18px;' align=\"right\">Página. {PAGENO}</td>
                   </tr>
                 </table>";

$mpdf->SetHTMLFooter($footer);

//echo $html;

$mpdf->WriteHTML($html);

$mpdf->Output($id_acao."_".$titulo.".pdf",'I');

exit;

}
//echo $html;
?>

