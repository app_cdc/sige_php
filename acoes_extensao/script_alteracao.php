<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Criar nova ação de extensão"; // Título da Página
    //$TPL->ContentHead = "../includes/head_abas.php"; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}

$sql = "UPDATE acoes_extensao SET linha_extensao = 25 WHERE id = 1;";

$mysqli->query($sql);

$mysqli->commit();

$mysqli->close();


?>