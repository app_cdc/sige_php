<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Ações de Extensão"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<style type="text/css">

    a.btn-info {
        width: 100%;
        text-align: left;
        background-color: #1f8e6a;
        border-color: #196e53;
    }

    a.btn-info:active {
        background-color: #196e53;
    }

    a.btn-info:hover {
        background-color: #196e53;
    }
    a.btn-info:focus {
        background-color: #196e53;
    }

    a.btn-info:after {
        /* symbol for "opening" panels */
        font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
        content: "\e114";    /* adjust as needed, taken from bootstrap.css */
        float: right;        /* adjust as needed */
        /*color: grey; */        /* adjust as needed */
    }
    a.btn-info.collapsed:after {
        /* symbol for "collapsed" panels */
        font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
        content: "\e113";    /* adjust as needed, taken from bootstrap.css */
        float: right;        /* adjust as needed */
    }

</style>

<div class="container">
<?php
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO, AVALIADOR, COORDENADOR);
protegePagina($permissoes);

$usuario_id = $_SESSION['UsuarioID'];
$sql_permissao = "SELECT * FROM permissao_usuario pu WHERE pu.usuario = $usuario_id ORDER BY pu.permissao";
$query_permissao = $mysqli->query($sql_permissao);
$usuario_permissoes = array();

// para cada permissão exibe os projetos que podem ser vistos
if ($result_permissao = $mysqli->query($sql_permissao)) {
    while ($dados_permissao = $query_permissao->fetch_array()) {
        $usuario_permissoes[] = $dados_permissao['permissao'];
    }
}

$data_atual = date("Y-m-d");

foreach ($usuario_permissoes as $usuario_permissao) {

    switch ($usuario_permissao) {
        case ADMINISTRADOR: //Visão ADMINISTRADOR
            echo "<a href='busca.php' class='btn btn-danger' role='button'>Buscar Projeto</a>&nbsp;<a href='lista.php' class='btn btn-warning' role='button'>Listar Projetos</a>
            <h1><b>Projetos a enviar para Conselheiros</b></h1><hr><br>";
            break;
        case COMISSAO: //Visão COMISSAO
            echo "<h1><b>Projetos a enviar para Avaliadores</b></h1><hr><br>";
            break;
        case AVALIADOR: //Visão AVALIADOR
            echo "<h1><b>Projetos a avaliar</b></h1><hr><br>";
            break;
        case COORDENADOR: //Visão COORDENADOR
            echo "<h1><b>Seus projetos</b></h1><hr><br>";
            break;
    }

    $tabs = array('Pendentes','Relatório Final','Todos');
?>
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#<?php echo '0'.$usuario_permissao ?>" data-toggle="tab"><?php echo $tabs[0] ?> <i class="fa"></i></a>
        </li>
        <li>
            <a href="#<?php echo '1'.$usuario_permissao ?>" data-toggle="tab"><?php echo $tabs[1] ?> <i class="fa"></i></a>
        </li>
        <li>
            <a href="#<?php echo '2'.$usuario_permissao ?>" data-toggle="tab"><?php echo $tabs[2] ?> <i class="fa"></i></a>
        </li>
    </ul>

    <div class="tab-content"><!-- <?php echo "Permissao ".$usuario_permissao;?> -->
<?php

    foreach ($tabs as $i => $tab) {
        if ($tab == 'Pendentes') {
            echo "<div class='tab-pane fade in active' id='$i"."$usuario_permissao'>";
        }else{
            echo "<div class='tab-pane fade' id='$i"."$usuario_permissao'>";
        }
        $sql_edital = "SELECT * FROM editais e ORDER BY e.id DESC";
        $query_edital = $mysqli->query($sql_edital);

        if ($result_edital = $mysqli->query($sql_edital)) {
            while ($dados_edital = $query_edital->fetch_array()) {

                $edital_id = $dados_edital['id'];
                $edital_dt_fim_projeto = $dados_edital['data_fim_projeto'];

                echo "<h2>
                        <a class='btn btn-info' data-toggle='collapse' href='#collapse".$i.$usuario_permissao.$edital_id."'>Edital: ".$dados_edital['codigo']." - ".$dados_edital['titulo']."</a>
                    </h2>
                    <hr>";

                switch ($usuario_permissao) {
                    case ADMINISTRADOR:
                        // o administrador tem acesso aos projetos que já foram enviados para o sistema (estado_acao = 2)
                        if ($tab == 'Pendentes') {
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao ae,
                                                 linhas_extensao le
                                            WHERE ae.edital = $edital_id
                                              AND ae.estado_acao = 2
                                              AND le.id = ae.linha_extensao
                                            ORDER BY le.nome,
                                                     ae.titulo,
                                                     ae.estado_acao";
                        }elseif ($tab == 'Relatório Final') {
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                               FROM acoes_extensao ae,
                                                    rel_final r,
                                                    linhas_extensao le
                                              WHERE ae.edital = $edital_id
                                                AND ae.estado_acao = 7
                                                AND ae.id = r.id_projeto
                                                AND r.estado = 2
                                                AND le.id = ae.linha_extensao
                                            ORDER BY le.nome,
                                                     ae.titulo,
                                                     ae.estado_acao";
                        }else{
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao ae,
                                                 linhas_extensao le
                                            WHERE ae.edital = $edital_id
                                              AND ae.estado_acao > 1
                                              AND le.id = ae.linha_extensao
                                            ORDER BY le.nome,
                                                     ae.titulo,
                                                     ae.estado_acao";
                        }
                        break;
                    case COMISSAO:
                        if ($tab == 'Pendentes') {
                            $sql_acao_ext = "SELECT a.titulo,
                                                    a.id,
                                                    a.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao a,
                                                 acoes_edicao ae,
                                                 linhas_extensao le
                                            WHERE a.edital = $edital_id
                                              AND a.estado_acao = 3
                                              AND ae.id_acao_extensao = a.id
                                              AND ae.id_editor = $usuario_id
                                              AND le.id = a.linha_extensao
                                            ORDER BY le.nome,
                                                     a.titulo";
                        }elseif ($tab == 'Relatório Final') {
                            $sql_acao_ext = "SELECT 1 FROM acoes_extensao a WHERE 1 = 2";
                        }else{
                            $sql_acao_ext = "SELECT a.titulo,
                                                    a.id,
                                                    a.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao a,
                                                 acoes_edicao ae,
                                                 linhas_extensao le
                                            WHERE a.edital = $edital_id
                                              AND a.estado_acao > 1
                                              AND ae.id_acao_extensao = a.id
                                              AND ae.id_editor = $usuario_id
                                              AND le.id = a.linha_extensao
                                            ORDER BY le.nome,
                                                     a.titulo";
                        }
                        break;
                    case AVALIADOR:
                        if ($tab == 'Pendentes') {
                            $sql_acao_ext = "SELECT a.titulo,
                                                    a.id,
                                                    a.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao a,
                                                 acoes_avaliacao aa,
                                                 linhas_extensao le
                                            WHERE a.edital = $edital_id
                                              AND a.estado_acao = 4
                                              AND aa.id_acao_extensao = a.id
                                              AND aa.id_avaliador = $usuario_id
                                              AND le.id = a.linha_extensao
                                            ORDER BY le.nome,
                                                     a.titulo";
                        }elseif ($tab == 'Relatório Final') {
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                               FROM acoes_extensao ae,
                                                    rel_final r,
                                                    acoes_avaliacao aa,
                                                    linhas_extensao le
                                              WHERE ae.edital = $edital_id
                                                AND ae.estado_acao = 7
                                                AND ae.id = r.id_projeto
                                                AND r.estado = 2
                                                AND aa.id_acao_extensao = ae.id
                                                AND aa.id_avaliador = $usuario_id
                                                AND le.id = ae.linha_extensao
                                           ORDER BY ae.estado_acao,
                                                    le.nome,
                                                    ae.titulo";
                        }else{
                            $sql_acao_ext = "SELECT a.titulo,
                                                    a.id,
                                                    a.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao a,
                                                 acoes_avaliacao aa,
                                                 linhas_extensao le
                                            WHERE a.edital = $edital_id
                                              AND a.estado_acao > 1
                                              AND aa.id_acao_extensao = a.id
                                              AND aa.id_avaliador = $usuario_id
                                              AND le.id = a.linha_extensao
                                            ORDER BY le.nome,
                                                     a.titulo";
                        }
                        break;
                    case COORDENADOR:
                        if ($tab == 'Pendentes') {
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao ae,
                                                 linhas_extensao le
                                            WHERE ae.edital = $edital_id
                                              AND ae.estado_acao = 1
                                              AND ae.coordenador = $usuario_id
                                              AND le.id = ae.linha_extensao
                                            ORDER BY le.nome,
                                                     ae.titulo";
                        }elseif ($tab == 'Relatório Final') {
                            if ($data_atual >= $edital_dt_fim_projeto) {
                                $sql_acao_ext = "SELECT ae.titulo,
                                                        ae.id,
                                                        ae.estado_acao,
                                                        le.nome as linha_extensao
                                                   FROM acoes_extensao ae,
                                                        linhas_extensao le
                                                  WHERE ae.edital = $edital_id
                                                    AND ae.estado_acao = 7
                                                    AND ae.coordenador = $usuario_id
                                                    AND le.id = ae.linha_extensao
                                               ORDER BY ae.estado_acao,
                                                        le.nome,
                                                        ae.titulo";
                            }else{
                                $sql_acao_ext = "SELECT 1 FROM acoes_extensao a WHERE 1 = 2";
                            }
                        }else{
                            $sql_acao_ext = "SELECT ae.titulo,
                                                    ae.id,
                                                    ae.estado_acao,
                                                    le.nome as linha_extensao
                                            FROM acoes_extensao ae,
                                                 linhas_extensao le
                                            WHERE ae.edital = $edital_id
                                              AND ae.coordenador = $usuario_id
                                              AND le.id = ae.linha_extensao
                                            ORDER BY le.nome,
                                                     ae.titulo";
                        }
                        break;
                }
                $query_acao_ext = $mysqli->query($sql_acao_ext);

                echo "<div id='collapse".$i.$usuario_permissao.$edital_id."' class='collapse'>";
                if($query_acao_ext->num_rows > 0){
                    if ($result_acao_ext = $mysqli->query($sql_acao_ext)) {
                        while ($dados_acao_ext = $query_acao_ext->fetch_array()) {
                            //
                            $acao_extensao_id = $dados_acao_ext['id'];
                            $acao_extensao_titulo = $dados_acao_ext['titulo'];
                            $acao_extensao_estado = $dados_acao_ext['estado_acao'];
                            $acao_extensao_linha = $dados_acao_ext['linha_extensao'];

                            ?>
                            <div class="row <?php echo ($acao_extensao_estado == 6 && $usuario_permissao != COORDENADOR) ? 'bg-danger' : '' ;?>"><!--div row-->
                                <div class="col-sm-5"><!--div titulo-->
                                    <h5><?php echo $acao_extensao_linha; ?></h5>
                                    <h3><a href="show.php?id=<?php echo $acao_extensao_id ?>" ><?php echo $acao_extensao_titulo; ?></a></h3>
                                </div><!--div titulo-->
                                <div class="col-sm-2"><!--div estado-->
                                    <?php
                                    if ($usuario_permissao == COORDENADOR) {
                                        if ($acao_extensao_estado == 1) {
                                            $acao_estado = "Pendente";
                                        }elseif ($acao_extensao_estado == 7) {
                                            if($data_atual >= $edital_dt_fim_projeto){
                                                $rel_final_estado = "";
                                                $sql_rel_final = "SELECT estado
                                                                FROM rel_final r
                                                                WHERE r.id_projeto = $acao_extensao_id";

                                                if ($result_rel_final = $mysqli->query($sql_rel_final)) {
                                                    while ($dados_rel_final = $result_rel_final->fetch_array()) {
                                                        $rel_final_estado = $dados_rel_final['estado'];
                                                    }
                                                }

                                                if (empty($rel_final_estado) || $rel_final_estado == 1) {
                                                    $acao_estado = "Relatório Técnico Final Pendente";
                                                }else{
                                                    $acao_estado = "Relatório Técnico Final Entregue";
                                                }

                                            }else{
                                                $acao_estado = "Submetido";
                                            }
                                        }else{
                                            $acao_estado = "Submetido";
                                        }
                                    }else{
                                        switch ($acao_extensao_estado) {
                                            case 1:
                                                $acao_estado = "Pendente";
                                                break;
                                            case 2:
                                                $acao_estado = "Submetido";
                                                break;
                                            case 3:
                                                $acao_estado = "Em análise";
                                                if ($usuario_permissao == ADMINISTRADOR || $usuario_permissao == COMISSAO) {
                                                    $sql_editor = "SELECT e.*, u.nome FROM acoes_edicao e, usuarios u WHERE e.id_editor = u.id AND id_acao_extensao = $acao_extensao_id ";
                                                    $query_editor = $mysqli->query($sql_editor);

                                                    if ($result_editor = $mysqli->query($sql_editor)) {
                                                        while ($dados_editor = $query_editor->fetch_array()) {
                                                            $acao_estado .= " por <br>".$dados_editor['nome']." <br>desde ".date_format(date_create($dados_editor['data_envio']),'d/m/y - H:i');
                                                        }
                                                    }
                                                }
                                                break;
                                            case 4:
                                                $acao_estado = "Em avaliação";
                                                if ($usuario_permissao == ADMINISTRADOR || $usuario_permissao == COMISSAO) {
                                                    $sql_avaliador = "SELECT a.*, u.nome FROM acoes_avaliacao a, usuarios u WHERE a.id_avaliador = u.id AND id_acao_extensao = $acao_extensao_id ";
                                                    $query_avaliador = $mysqli->query($sql_avaliador);

                                                    if ($result_avaliador = $mysqli->query($sql_avaliador)) {
                                                        while ($dados_avaliador = $query_avaliador->fetch_array()) {
                                                            $acao_estado .= " por <br>".$dados_avaliador['nome']." <br>desde ".date_format(date_create($dados_avaliador['data_envio']),'d/m/y - H:i');
                                                        }
                                                    }
                                                }
                                                break;
                                            case 5:
                                                $acao_estado = "Avaliado";
                                                if ($usuario_permissao == ADMINISTRADOR || $usuario_permissao == COMISSAO) {
                                                    $sql_avaliador = "SELECT a.*, u.nome FROM acoes_avaliacao a, usuarios u WHERE a.id_avaliador = u.id AND id_acao_extensao = $acao_extensao_id ";
                                                    $query_avaliador = $mysqli->query($sql_avaliador);

                                                    if ($result_avaliador = $mysqli->query($sql_avaliador)) {
                                                        while ($dados_avaliador = $query_avaliador->fetch_array()) {
                                                            $acao_estado .= " por <br>".$dados_avaliador['nome']. " <br>em ".date_format(date_create($dados_avaliador['data_avaliacao']),'d/m/y - H:i');
                                                        }
                                                    }
                                                }
                                                break;
                                            case 6:
                                                $acao_estado = "Desclassificado";
                                                if ($usuario_permissao == ADMINISTRADOR || $usuario_permissao == COMISSAO) {
                                                    $sql_administrador = "SELECT d.*, u.nome FROM acoes_desclassificacao d, usuarios u WHERE d.administrador_id = u.id AND acao_extensao_id = $acao_extensao_id ";
                                                    $query_administrador = $mysqli->query($sql_administrador);

                                                    if ($result_avaliador = $mysqli->query($sql_administrador)) {
                                                        while ($dados_administrador = $query_administrador->fetch_array()) {
                                                            $acao_estado .= " por <br>".$dados_administrador['nome']. " <br>em ".date_format(date_create($dados_administrador['data']),'d/m/y - H:i');
                                                        }
                                                    }
                                                }
                                                break;
                                            case 7:
                                                $acao_estado = "Aprovado";
                                                break;
                                        }
                                    }
                                    echo $acao_estado;
                                    ?>
                                </div><!--div estado-->
                                <div class="col-sm-5"><!--div botoes-->
                                    <?php

                                    $gerar_pdf = TRUE; // para saber se aparece o botão de gerar pdf

                                    // exibe botão para gerar o pdf se o projeto não está mais pendente
                                    if ($acao_extensao_estado > 1 && $gerar_pdf){
                                        echo "<a href='impressao_pdf.php?id=".$acao_extensao_id."' target='_blank' class='btn btn-danger' role='button'><span class='glyphicon glyphicon-file'></span> Gerar PDF</a><br><br> ";
                                    }
                                    switch ($usuario_permissao) {
                                        case ADMINISTRADOR:
                                            if ($acao_extensao_estado == 2){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-send'></span> Enviar Conselheiro</a><br><br> ";
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-warning' role='button'>Desclassificar</a><br><br> ";
                                            }
                                            if ($acao_extensao_estado == 3){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-primary' role='button'><span class='glyphicon glyphicon-send'></span> Trocar Conselheiro</a><br><br> ";
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-warning' role='button'>Desclassificar</a><br><br> ";
                                            }
                                            if ($acao_extensao_estado == 5){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-search'></span> Ver notas</a><br><br> ";
                                            }
                                            if ($acao_extensao_estado == 7){
                                                if($data_atual >= $edital_dt_fim_projeto){
                                                    $rel_final_estado = "";
                                                    $sql_rel_final = "SELECT estado
                                                                    FROM rel_final r
                                                                    WHERE r.id_projeto = $acao_extensao_id";

                                                    if ($result_rel_final = $mysqli->query($sql_rel_final)) {
                                                        while ($dados_rel_final = $result_rel_final->fetch_array()) {
                                                            $rel_final_estado = $dados_rel_final['estado'];
                                                        }
                                                    }

                                                    if (!empty($rel_final_estado) && $rel_final_estado == 2) {
                                                        echo "<a href='/rel_final/index.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-pencil'></span> Relatório Técnico Final</a><br><br> ";
                                                        echo "<a href='/rel_final/impressao_pdf.php?id=".$acao_extensao_id."' target='_blank' class='btn btn-danger' role='button'><span class='glyphicon glyphicon-file'></span> Gerar PDF Relatório</a><br><br> ";
                                                    }
                                                }
                                            }
                                            break;
                                        case COMISSAO:
                                            if ($acao_extensao_estado == 3){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-send'></span> Enviar Avaliador</a><br><br> ";
                                            }
                                            if ($acao_extensao_estado == 4){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-primary' role='button'><span class='glyphicon glyphicon-send'></span> Trocar Avaliador</a><br><br> ";
                                            }
                                            if ($acao_extensao_estado == 7){
                                                if($data_atual >= $edital_dt_fim_projeto){
                                                    $rel_final_estado = "";
                                                    $sql_rel_final = "SELECT estado
                                                                    FROM rel_final r
                                                                    WHERE r.id_projeto = $acao_extensao_id";

                                                    if ($result_rel_final = $mysqli->query($sql_rel_final)) {
                                                        while ($dados_rel_final = $result_rel_final->fetch_array()) {
                                                            $rel_final_estado = $dados_rel_final['estado'];
                                                        }
                                                    }

                                                    if (!empty($rel_final_estado) && $rel_final_estado == 2) {
                                                        echo "<a href='/rel_final/index.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-pencil'></span> Relatório Técnico Final</a><br><br> ";
                                                    }
                                                }
                                            }
                                            break;
                                        case AVALIADOR:
                                            if ($acao_extensao_estado == 4){
                                                echo "<a href='show.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-pencil'></span> Avaliar</a><br><br> ";
                                            }
                                            break;
                                        case COORDENADOR:
                                            if ($acao_extensao_estado == 7){
                                                if($data_atual >= $edital_dt_fim_projeto){
                                                    echo "<a href='/rel_final/index.php?id=".$acao_extensao_id."' class='btn btn-success' role='button'><span class='glyphicon glyphicon-pencil'></span> Relatório Técnico Final</a><br><br> ";
                                                }
                                            }
                                            break;
                                    }
                                    ?>
                                </div><!--div botoes-->
                            </div><!--div row-->
                            <hr>

                            <?php

                        }
                    }
                }else{
                    echo "<h3>Nenhum projeto para esse edital</h3>";
                }// end if sql acao extensao
                echo "</div>";
            }
        }// end if sql editais
        echo "</div><!--div tab-pane-->";
    }
    echo "<br></div><!--div tab-content Permissao $usuario_permissao-->";

}

?>
    </div><!--div 2-->
    </div><!--div 3-->
</div> <!--div 4-->

<script type="text/javascript">
    $(document).ready( function(){
       $("#projetos").addClass("active");
    });
</script>