<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Editais"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE

?>

<div class="container">

<h1>
    <b>Editais</b>
    <?php if (isset($_SESSION['UsuarioID']) && temPermissao($_SESSION['UsuarioID'],ADMINISTRADOR)){ ?>
    <a href="/editais/novo.php" class="btn btn-primary btn-lg" role="button" data-toggle="tooltip" data-placement="right" title="Novo">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
    <?php } ?>
</h1>

<hr>

<?php
if (isset($_SESSION['UsuarioID']) && temPermissao($_SESSION['UsuarioID'],ADMINISTRADOR)){
    $sql = "SELECT * FROM editais order by id desc";
}else{
    $sql = "SELECT * FROM editais where ativo = 'S' order by id desc";
}
$query = $mysqli->query($sql);

if ($result = $mysqli->query($sql)) {
    while ($dados = $query->fetch_array()) {

        $edital_ativo = $dados['ativo'];

        $edital_imagem = $dados['imagem'];

        $edital_data_divulgacao = $dados['data_divulgacao'];
        $edital_data_validade = $dados['data_validade'];

        $data_atual = date("Y-m-d");

?>
        <edital>

            <div class="row">
            <div class="col-md-6">
            <div class="thumbnail">
                    <img src="../style/images/<?php echo $edital_imagem; ?>" alt="...">
                <div class="caption">
            <h3>
                <?php if (isset($_SESSION['UsuarioID']) && temPermissao($_SESSION['UsuarioID'],ADMINISTRADOR)){ ?>
                    <span class="glyphicon glyphicon-eye-<?php echo $ativo = ($edital_ativo=='S') ? 'open status-green' : 'close status-red';?>" data-toggle="tooltip" data-placement="left" title="<?php echo $ativo = ($edital_ativo=='S') ? 'Ativo' : 'Inativo';?>"></span>
                <?php } ?>
                <?php echo ' '.$dados['codigo'].' - '.$dados['titulo'] ?>
            </h3>

            <p><?php echo $dados['resumo'] ?></p>
            <p><a href="/uploads/<?php echo 'EDITAL_'.$dados['codigo'].'.pdf' ?>" target="_blank" class="btn btn-danger" role="button"><span class="glyphicon glyphicon-file"></span> Edital em PDF</a> <?php

            if (isset($_SESSION['UsuarioID'])){
                if ($data_atual >= $edital_data_divulgacao) {
                    if ($data_atual <= $edital_data_validade) {
                        // verifica se tem projeto cadastrado e concluido
                        $sql_pendencia_1 = "SELECT ae.*
                                            FROM acoes_extensao ae,
                                            editais e
                                            WHERE ae.edital = e.id
                                            AND e.data_fim_projeto < now()
                                            AND ae.estado_acao = 7
                                            AND ae.coordenador = ".$_SESSION['UsuarioID'];
                        $query_pendencia_1 = $mysqli->query($sql_pendencia_1);
                        // verifica se está devendo relatório final
                        $sql_pendencia_2 = "SELECT rf.estado, ae.*
                                            FROM acoes_extensao ae,
                                            rel_final rf,
                                            editais e
                                            WHERE ae.edital = e.id
                                            AND e.data_fim_projeto < now()
                                            AND ae.estado_acao = 7
                                            AND rf.id_projeto = ae.id
                                            AND rf.estado = 2
                                            AND ae.coordenador = ".$_SESSION['UsuarioID'];
                        $query_pendencia_2 = $mysqli->query($sql_pendencia_2);

                        if($query_pendencia_1->num_rows > 0 && $query_pendencia_2->num_rows == 0){ // tem projeto finalizado e está devendo relatorio final
?>
                            <button class="btn btn-warning btn-lg">Para enviar proposta é preciso enviar o Relatório Final que está pendente</button>
<?php
                        } else {
                            if (temPermissao($_SESSION['UsuarioID'],COORDENADOR)){
?>
                            <a href="/acoes_extensao/editar.php?cod_edital=<?php echo $dados['codigo']?>" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-open-file"></span> Enviar proposta</a>
<?php
                            }else{
?>

                            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#naoCoordenadorModal" > Enviar proposta</button>
<?php
                            }
                        }
                    }else{
?>
                    <button class="btn btn-warning btn-lg">Encerrado o prazo para envio de propostas</button>
<?php
                    }
                }
                if (temPermissao($_SESSION['UsuarioID'],ADMINISTRADOR)){
                    ?>
                    <a href="/editais/exportar_sape.php?id_edital=<?php echo $dados['id']?>" class="btn btn-success" role="button">Exportar para SAPE</a>
                    <a href="/editais/importar_sape.php?id_edital=<?php echo $dados['id']?>" class="btn btn-success" role="button">Importar classificação do SAPE</a>
                    <a href="/classificacao/index.php?id_edital=<?php echo $dados['id']?>" class="btn btn-warning" role="button">Classificação</a>
                    <a href="/editais/editar.php?id_edital=<?php echo $dados['id'];?>" class="btn btn-primary btn-lg" role="button" data-toggle="tooltip" data-placement="right" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                    <?php
                }
            }
?>
            </p>
            </div>
            </div>
            </div>
            </div>


        </edital>



<?php
    }
}

?>

</div>

<script type="text/javascript">
    $(document).ready( function(){
       $("#editais").addClass("active");
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<div class="modal fade" id="naoCoordenadorModal" tabindex="-1" role="dialog" aria-labelledby="naoCoordenadorModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="naoCoordenadorModalLabel">Atenção!</h4>
      </div>
      <div class="modal-body">
        <p>Entre em contato com a administração do Edital pois você não está com permissão de "Coordenador" no sistema para enviar projetos.</p>
      </div>
    </div>
  </div>
</div>
