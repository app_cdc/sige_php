<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Criar novo Edital"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
     $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
     $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
     $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
     return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}
//
if( !empty($_POST) ){

    $sql = "SELECT * FROM editais";
    $query = $mysqli->query($sql);
    $count_editais = $query->num_rows;

    $data_atual = new DateTime();
    $ano_base = $data_atual->format('Y');
    $edital_codigo = $data_atual->format('Y');
    $edital_codigo .= sprintf("%02d", $count_editais+1);

    $edital_titulo = $_POST['titulo'];
    $edital_resumo = $_POST['resumo'];
    $edital_valor  = convertCoin("EN",2,$_POST['valor']);
    $edital_data_validade = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_validade'])));
    $edital_data_divulgacao = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_divulgacao'])));
    $edital_data_fim_projeto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_fim_projeto'])));

    $uploaddir = '../uploads/';
    $uploadfile = $uploaddir . 'EDITAL_' . $edital_codigo . '.pdf';

    $sql = "INSERT INTO editais (codigo,ano_base,titulo,resumo,anexo,valor,data_validade,data_divulgacao,data_fim_projeto) VALUES ($edital_codigo,$ano_base,'$edital_titulo','$edital_resumo','$uploadfile',$edital_valor,'$edital_data_validade','$edital_data_divulgacao','$edital_data_fim_projeto')";

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {

        if (move_uploaded_file($_FILES['pdf']['tmp_name'], $uploadfile)) {
            $mysqli->commit();
            echo "Edital criado com sucesso!\n";
        } else {
            $msg_erro .= "Possível ataque de upload de arquivo!\n";
        }

    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao criar o edital. '.$msg_erro;
    }
}
else
{
    //Mostra a tela para inserir um novo edital

?>

<div class="container">

    <h1><b>Novo Edital</b></h1>

    <form enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

        <div class="form-group">
            <label class="col-sm-3 control-label" for="titulo">Titulo:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="titulo" id="titulo" size="50" required>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="col-sm-3 control-label" for="resumo">Resumo:</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="resumo" id="resumo" rows="3" maxlength="400" required></textarea>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="col-sm-3 control-label" for="valor">Valor:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="valor" id="valor" required>
                <script type="text/javascript">$("#valor").maskMoney({prefix:'R$ ', showSymbol:true, thousands:'.', decimal:',', affixesStay: false});</script>
            </div>
        </div><!--div form-group-->

        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_divulgacao">Data Divulgação:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_divulgacao" name="data_divulgacao" placeholder="DD/MM/YYY" type="date"/>
            </div>
        </div>

        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_validade">Data Término das Inscrições:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_validade" name="data_validade" placeholder="DD/MM/YYY" type="date"/>
            </div>
        </div>

        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_fim_projeto">Data Término dos projetos:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_fim_projeto" name="data_fim_projeto" placeholder="DD/MM/YYY" type="date"/>
            </div>
        </div>

        <hr>
        <h4>Upload do Edital em pdf</h4>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="pdf">Selecione o arquivo:</label>
            <div class="col-sm-8">
                <input type="file" name="pdf" id="pdf" accept="application/pdf" required/>
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <div class="col-sm-3">
                <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/editais';">< Voltar</button>
            </div>
            <div class="col-sm-3">
                <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
            </div>
        </div><!--div form-group-->

    </form>

</div> <!-- div container -->

<?php
}
?>