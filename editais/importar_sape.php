<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
  $TPL = new PageTemplate();
  $TPL->PageTitle = "Criar novo Edital"; // Título da Página
  //$TPL->ContentHead = ""; // Header da Página
  $TPL->ContentBody = __FILE__;
  include "../layout.php";
  exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
    $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
    $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
    $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
    return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}
//
$sape_api_key = "89ae6ddd-60c5-4c26-9010-0a5a2bcd49b7";
$usuario_cadastrado = false;
$edital_cadastrado = false;
$propostas_cadastradas = false;
$count_propostas = 0;
$count_propostas_cadastradas = 0;
$count_propostas_ja_cadastradas = 0;

$msg_erro = '';
$msg_salvo = '';
// se passou o id do edital e um arquivo via upload
if ( isset($_GET['id_edital']) && $_FILES ) {

  // pega o id do usuário logado
  $usuario_logado_id = $_SESSION['UsuarioID'];

  // verirfica se o arquivo passado é do tipo válido (csv)
  $mimes = array('application/vnd.ms-excel','text/csv');
  if(in_array($_FILES['csv']['type'],$mimes)){

    // busca os dados do edital passado
    $sql_edital = "SELECT * FROM editais WHERE id = " . $_GET['id_edital'];
    $query_edital = $mysqli->query($sql_edital);

    // se encontrou o edital passado
    if ($dados_edital = $query_edital->fetch_array()) {

      // define o número da linha inicial do arquivo
      $row = 1;
      // se conseguiu abrir o arquivo
      if (($handle = fopen($_FILES['csv']['tmp_name'], "r")) !== FALSE) {

        // percorre as linhas do arquivo
        while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
          // pula a primeira linha pois é o título das colunas
          if ($row > 1) {
            // continua para a próxima linha se não teve erro
            if (empty($msg_erro)) {
              // pega os dados da linha
              $cod_edital = $data[0];
              $id_acao_extensao = $data[1];
              $titulo = $data[3];

              $source = array('.', ',');
              $replace = array('', '.');
              $nota = str_replace($source, $replace, $data[2]);

              // verifica se o código do edital é igual o do edital da classificação
              if ($dados_edital['codigo'] <> $cod_edital) {
                $msg_erro = "Classificação de outro edital";
              } else {

                // busca os dados da ação de extensão
                $sql_acao_extensao = "SELECT * FROM acoes_extensao WHERE id = " . $id_acao_extensao;
                $query_acao_extensao = $mysqli->query($sql_acao_extensao);

                // se encontrou a ação de extensão
                if ($dados_acao_extensao = $query_acao_extensao->fetch_array()) {

                  // verifica se o titulo da ação é igual, para evitar colocar nota na ação errada
                  if (trim($dados_acao_extensao['titulo']) <> trim($titulo)) {
                    $msg_erro = "Ação com ID $id_acao_extensao está com título diferente, favor verificar.<br>'$titulo'<br>'".$dados_acao_extensao['titulo']."'";
                  } else {

                    if (in_array($dados_acao_extensao['estado_acao'],array(1,5,6,7))) {
                      switch ($acao_extensao_estado) {
                        case 1:
                          $msg_erro = "Ação '$titulo' está com status Pendente.";
                          break;
                        case 5:
                          $msg_erro = "Ação '$titulo' já está com status Avaliada";
                          break;
                        case 6:
                          $msg_erro = "Ação '$titulo' já está com status Desclassificada";
                          break;
                        case 7:
                          $msg_erro = "Ação '$titulo' já está com status Aprovada";
                          break;
                        default:
                          $msg_erro = "Ação '$titulo' está com status diferente de Submetido";
                          break;
                      }

                    } else {

                      // verifica se já tem uma avaliação
                      $sql_acao_avaliacao = "SELECT * FROM acoes_avaliacao WHERE id_acao_extensao = " . $id_acao_extensao;
                      $query_acao_avaliacao = $mysqli->query($sql_acao_avaliacao);

                      if ($dados_acao_avaliacao = $query_acao_avaliacao->fetch_array()) {
                        $msg_erro = "Ação '$titulo' já tem uma avaliação.";
                      } else {

                        $hoje = date("Y-m-d H:i:s");

                        $insert_acao_avaliacao = "INSERT INTO acoes_avaliacao (id_acao_extensao,id_editor,id_avaliador,data_envio,data_avaliacao) VALUES (".$id_acao_extensao.",1,1,'".$hoje."','".$hoje."')";
                        if ($mysqli->query($insert_acao_avaliacao) === TRUE) {

                          $id_acao_avaliacao = $mysqli->insert_id;

                          $insert_pergunta_avaliacao = "INSERT INTO pergunta_avaliacao (id_pergunta,id_avaliacao,nota) VALUES (1,".$id_acao_avaliacao.",".$nota.")";
                          if ($mysqli->query($insert_pergunta_avaliacao) === TRUE) {

                            $update_acao_extensao = "UPDATE acoes_extensao SET estado_acao = 5 WHERE id = " . $id_acao_extensao;
                            if ($mysqli->query($update_acao_extensao) === TRUE) {

                              //echo "Estado da ação alterado";

                            } else {
                              $msg_erro .= "Error: " . $mysqli->error . "<br>" . $update_acao_extensao;
                            }

                          } else {
                            $msg_erro .= "Error: " . $mysqli->error . "<br>" . $insert_pergunta_avaliacao;
                          }

                        } else {
                          $msg_erro .= "Error: " . $mysqli->error . "<br>" . $insert_acao_avaliacao;
                        }

                        if(empty($msg_erro)){
                          $mysqli->commit();
                          $msg_salvo .= "A classificação do projeto '" . $titulo . "' foi importada com sucesso! <br>";
                        } else {
                          $mysqli->rollback();
                        }

                      }
                    }

                    // insere a avaliação em acoes_avaliacao

                    // insere a nota em pergunta_avaliacao
                  }
                }
              }
            }


          }
          $row++;
        }
        fclose($handle);
      }

    } else {
      $msg_erro = "Edital não encontrado";
    }

  } else {
    $msg_erro = "Tipo de arquivo inválido";
  }

}

//Mostra a tela para exportar edital para o sape
?>

<div class="container">

  <h1><b>Importar classificação do edital para o SAPE</b></h1>
  <hr>

  <?php if ($msg_erro) { ?>
  <p style="color: red;">
    Verifique os erros abaixo e tente novamente. <br>
    <?php echo $msg_erro; ?>
  </p>
  <?php } ?>

  <form enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

    <h4>Upload da classificação do Edital em csv</h4>

    <div class="form-group">
      <label class="col-sm-4 control-label" for="pdf">Selecione o arquivo:</label>
      <div class="col-sm-8">
        <input type="file" name="csv" id="csv" accept=".csv" required/>
      </div>
    </div><!--div form-group-->

    <hr>
    <div class="form-group">
      <div class="col-sm-3">
        <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/editais';">< Voltar</button>
      </div>
      <div class="col-sm-3">
        <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Importar</button>
      </div>
    </div><!--div form-group-->

  </form>
  <?php if ($msg_salvo) { ?>
  <pre>
    <p style="color: green;"><?php echo $msg_salvo; ?></p>
  </pre>
  <?php } ?>
  <br>
  <br>
  <br>
</div> <!-- div container -->

