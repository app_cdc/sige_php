<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Alterar Edital"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
     $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
     $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
     $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
     return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}
//
$edital_id = '';
$edital_titulo = '';
$edital_resumo = '';
$edital_valor = '';
$edital_data_divulgacao = '';
$edital_data_validade = '';
$edital_data_fim_projeto = '';
$edital_anexo = '';
$edital_ativo = '';
//
if( !empty($_POST) ){

    $edital_id = $_POST['edital_id'];

    $edital_titulo = $_POST['titulo'];
    $edital_resumo = $_POST['resumo'];
    $edital_valor  = convertCoin("EN",2,$_POST['valor']);
    $edital_data_validade = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_validade'])));
    $edital_data_divulgacao = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_divulgacao'])));
    $edital_data_fim_projeto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['data_fim_projeto'])));

    $edital_anexo = $_POST['edital_anexo'];

    if (!empty($_POST['ativo'])){
        $edital_ativo = 'S';
    }else{
        $edital_ativo = 'N';
    }
        
    

    $sql = "UPDATE editais SET titulo = '$edital_titulo',
                   resumo = '$edital_resumo',
                   anexo = '$edital_anexo',
                   valor = $edital_valor,
                   data_validade = '$edital_data_validade',
                   data_divulgacao ='$edital_data_divulgacao',
                   data_fim_projeto = '$edital_data_fim_projeto',
                   ativo = '$edital_ativo'
            WHERE id = $edital_id";

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {
        if( !empty($_FILES['pdf']['tmp_name']) ){
            if (move_uploaded_file($_FILES['pdf']['tmp_name'], $edital_anexo)) {
                $mysqli->commit();
                echo "Edital alterado com sucesso!\n";
            } else {
                $msg_erro .= "Possível ataque de upload de arquivo!\n";
            }
        }else{
            $mysqli->commit();
            echo "Edital alterado com sucesso!\n";
        }

    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao criar o edital. '.$msg_erro;
    }
}

$ativo = '';

if (isset($_GET['id_edital'])) {

    $edital_id = $_GET['id_edital'];

    $sql = "SELECT * FROM editais WHERE id = ".$edital_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
        while ($dados = $query->fetch_array()) {
            $edital_titulo = $dados['titulo'];
            $edital_resumo = $dados['resumo'];
            $edital_valor = $dados['valor'];
            $edital_data_divulgacao = $dados['data_divulgacao'];
            $edital_data_validade = $dados['data_validade'];
            $edital_data_fim_projeto = $dados['data_fim_projeto'];
            $edital_anexo = $dados['anexo'];
            $edital_ativo = $dados['ativo'];
        }
    }

    if ($edital_ativo == 'S') {
        $ativo = 'checked';
    }else
    {
        $ativo = '';
    }
}
?>

<div class="container">

    <h1><b>Alterar Edital</b></h1>

    <form enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
        
        <input type="hidden" name="edital_id" value="<?php echo $edital_id;?>" />
        <input type="hidden" name="edital_anexo" value="<?php echo $edital_anexo;?>" />
        
        <div class="form-group">
            <label class="col-sm-3 control-label" for="titulo">Titulo:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="titulo" id="titulo" size="50" required value="<?php echo $edital_titulo;?>">
            </div>
        </div><!--div form-group-->

        <div class="form-group">
            <label class="col-sm-3 control-label" for="resumo">Resumo:</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="resumo" id="resumo" rows="3" maxlength="400" required><?php echo $edital_resumo;?></textarea>
            </div>
        </div><!--div form-group-->
        
        <div class="form-group">
            <label class="col-sm-3 control-label" for="valor">Valor:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="valor" id="valor" required value="<?php echo $edital_valor;?>">
                <script type="text/javascript">$("#valor").maskMoney({prefix:'R$ ', showSymbol:true, thousands:'.', decimal:',', affixesStay: false});</script>
            </div>
        </div><!--div form-group-->

        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_divulgacao">Data Divulgação:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_divulgacao" name="data_divulgacao" placeholder="DD/MM/YYY" type="date" value="<?php echo $edital_data_divulgacao;?>"/>
            </div>
        </div>

        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_validade">Data Vencimento:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_validade" name="data_validade" placeholder="DD/MM/YYY" type="date" value="<?php echo $edital_data_validade;?>"/>
            </div>
        </div>
        
        <div class="form-group"> <!-- Date input -->
            <label class="col-sm-3 control-label" for="data_fim_projeto">Data Término dos projetos:</label>
            <div class="col-sm-9">
                <input class="form-control" id="data_fim_projeto" name="data_fim_projeto" placeholder="DD/MM/YYY" type="date" value="<?php echo $edital_data_fim_projeto;?>"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <label class='radio-inline'><input type='checkbox' name='ativo' value='S' <?php echo $ativo;?> > Ativo? </label>
            </div>
        </div>


        <hr>
        <h4>Upload do Edital em pdf</h4>

        <div class="form-group">
            <a href="<?php echo $edital_anexo ?>" target="_blank" class="btn btn-danger" role="button"><span class="glyphicon glyphicon-file"></span>Visualizar</a>
        </div>
        <div class="form-group">

            <label class="col-sm-4 control-label" for="pdf">Caso queira alterar o arquivo selecione o novo:</label>
            <div class="col-sm-8">
                <input type="file" name="pdf" id="pdf" accept="application/pdf"/>
            </div>
        </div><!--div form-group-->
        
        <div class="form-group">
            <div class="col-sm-3">
                <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/editais';">< Voltar</button>
            </div>
            <div class="col-sm-3">
                <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
            </div>
        </div><!--div form-group-->

    </form>

</div> <!-- div container -->

