<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
  $TPL = new PageTemplate();
  $TPL->PageTitle = "Criar novo Edital"; // Título da Página
  //$TPL->ContentHead = ""; // Header da Página
  $TPL->ContentBody = __FILE__;
  include "../layout.php";
  exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR, COMISSAO);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
    $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
    $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
    $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
    return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}
//
$sape_api_key = "89ae6ddd-60c5-4c26-9010-0a5a2bcd49b7";
$usuario_cadastrado = false;
$edital_cadastrado = false;
$propostas_cadastradas = false;
$count_propostas = 0;
$count_propostas_cadastradas = 0;
$count_propostas_ja_cadastradas = 0;

if ( $_POST ) {

  // exporatar usuario
  $usuario_logado_id = $_SESSION['UsuarioID'];

  if (empty($_SESSION['UsuarioCPF']) && $_POST['usuario_logado_cpf']) {
    // insere na tabela local
    $formulario_cpf = $_POST['usuario_logado_cpf'];
    $sql_update_cpf = " UPDATE usuarios SET cpf = '" . $formulario_cpf . "' WHERE id = " . $usuario_logado_id;

    if ($mysqli->query($sql_update_cpf) === TRUE) {
      $mysqli->commit();
      $_SESSION['UsuarioCPF'] = $formulario_cpf;


    } else {
      $mysqli->rollback();
    }
  }

}

if ($_SESSION['UsuarioCPF']) {

  $usuario_logado_cpf = $_SESSION['UsuarioCPF'];
  $usuario_logado_nome = $_SESSION['UsuarioNome'];
  $usuario_logado_email = $_SESSION['UsuarioEmail'];

  $data = array ('cpf' => $usuario_logado_cpf, 'nome' => $usuario_logado_nome, 'email' => $usuario_logado_email);
  $data = http_build_query($data);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,"http://sistemas.proec.unicamp.br/sape/api/usuarios");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('SAPE-API-KEY: ' . $sape_api_key));

  $server_output = curl_exec($ch);

  if ($server_output === false) {
    echo 'Curl error: ' . curl_error($ch);
  }

  curl_close ($ch);

  if ($server_output) {

    $array_data = json_decode($server_output);

    if ($array_data->status === false) {
      if ($array_data->message == "Usuário já cadastrado") {
        $usuario_cadastrado = true;
      }
    }
    if ($array_data->status === true) {
      $usuario_cadastrado = true;
    }
  }


  if ($usuario_cadastrado) {

    $usuario_logado_cpf = $_SESSION['UsuarioCPF'];

    if ($_GET['id_edital']) {
      // pega os dados do edital
      $sql = "SELECT * FROM editais e WHERE e.id = " . $_GET['id_edital'];
      if ($result = $mysqli->query($sql)) {
        while ($dados = $result->fetch_array()) {

          $edital_id = $dados['id'];
          $edital_cod = $dados['codigo'];
          $edital_titulo = $dados['titulo'];
          list($doispontos, $upload, $arquivo) = explode("/", $dados['anexo']);
          $edital_pdf = "";
          if ($arquivo) {
            $edital_pdf = "https://sige.unicamp.br/uploads/".$arquivo;
          }

          $data = array ('cpf_gerente' => $usuario_logado_cpf,
                          'cod_edital' => $edital_cod,
                          'titulo' => $edital_titulo,
                          'link_pdf' => $edital_pdf);
          $data = http_build_query($data);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,"http://sistemas.proec.unicamp.br/sape/api/editais");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('SAPE-API-KEY: ' . $sape_api_key));

          $server_output = curl_exec($ch);

          if ($server_output === false) {
            echo 'Curl error: ' . curl_error($ch);
          }

          curl_close ($ch);

          if ($server_output) {

            $array_data = json_decode($server_output);

            if ($array_data->status === false) {
              if ($array_data->message == "Edital já cadastrado") {
                $edital_cadastrado = true;
              }
            }
            if ($array_data->status === true) {
              $edital_cadastrado = true;
            }
          }

        }
      }
    }
  }

  if ($edital_cadastrado) {

    if ($_GET['id_edital']) {
      // pega os dados do edital
      $sql = "SELECT e.codigo AS cod_edital, a.* FROM acoes_extensao a, editais e WHERE e.id = a.edital AND a.estado_acao NOT IN (1,6) AND a.edital = " . $_GET['id_edital'];
      if ($result = $mysqli->query($sql)) {

        while ($dados = $result->fetch_array()) {

          $count_propostas += 1;

          $edital_cod = $dados['cod_edital'];
          $proposta_cod = $dados['id'];
          $proposta_titulo = $dados['titulo'];
          $proposta_pdf = "https://sige.unicamp.br/acoes_extensao/impressao_pdf_sape.php?id=".$dados['id'];

          $data = array ('cod_edital' => $edital_cod,
                          'cod_proposta' => $proposta_cod,
                          'titulo' => $proposta_titulo,
                          'pdf' => $proposta_pdf);
          $data = http_build_query($data);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,"http://sistemas.proec.unicamp.br/sape/api/propostas");
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('SAPE-API-KEY: ' . $sape_api_key));

          $server_output = curl_exec($ch);

          if ($server_output === false) {
            echo 'Curl error: ' . curl_error($ch);
          }

          curl_close ($ch);

          if ($server_output) {

            $array_data = json_decode($server_output);

            if ($array_data->status === false) {
              if ($array_data->message == "Proposta já cadastrada") {
                $count_propostas_ja_cadastradas += 1;
                $edital_cadastrado = true;
              }
            }
            if ($array_data->status === true) {
              $count_propostas_cadastradas += 1;
              $edital_cadastrado = true;
            }
          }

        }
      }
    }
  }
}

//Mostra a tela para exportar edital para o sape
?>

<div class="container">

  <h1><b>Exportar edital para o SAPE</b></h1>
  <hr>

  <form enctype="multipart/form-data" class="form-horizontal" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

    <?php if (empty($_SESSION['UsuarioCPF'])) { ?>
      <h4><?php echo $_SESSION['UsuarioNome']; ?> é necessário cadastrar seu CPF para continuar.</h4>
      <div class="form-group">
        <label class="col-sm-6 control-label" for="usuario_logado_cpf">CPF (somente números)</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="usuario_logado_cpf" id="usuario_logado_cpf" size="50" required>
        </div>
      </div><!--div form-group-->
    <?php } ?>

    <div class="form-group">
      <label class="col-sm-6 control-label">Usuário cadastrado</label>
      <div class="col-sm-6">
        <?php if ($usuario_cadastrado) { ?>
          <span class="glyphicon glyphicon-ok">
        <?php } else { ?>
          <span class="glyphicon glyphicon-remove">
        <?php } ?>
      </div>
    </div><!--div form-group-->

    <div class="form-group">
      <label class="col-sm-6 control-label">Edital cadastrado</label>
      <div class="col-sm-6">
        <?php if ($edital_cadastrado) { ?>
          <span class="glyphicon glyphicon-ok">
        <?php } else { ?>
          <span class="glyphicon glyphicon-remove">
        <?php } ?>
      </div>
    </div><!--div form-group-->

    <div class="form-group">
      <label class="col-sm-6 control-label"><?php echo $count_propostas_cadastradas+$count_propostas_ja_cadastradas; ?> Propostas cadastradas de <?php echo $count_propostas; ?> existentes no sistema.</label>
      <div class="col-sm-6">
        <?php if ($count_propostas == $count_propostas_cadastradas+$count_propostas_ja_cadastradas) { ?>
          <span class="glyphicon glyphicon-ok">
        <?php } else { ?>
          <span class="glyphicon glyphicon-remove">
        <?php } ?>
      </div>
    </div><!--div form-group-->

    <hr>
    <div class="form-group">
      <div class="col-sm-3">
        <button type="button" class="btn btn-default btnAnterior" onclick="location.href='/editais';">< Voltar</button>
      </div>
      <?php if (empty($_SESSION['UsuarioCPF'])) { ?>
      <div class="col-sm-3">
        <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
      </div>
      <?php } ?>
    </div><!--div form-group-->

  </form>

</div> <!-- div container -->

