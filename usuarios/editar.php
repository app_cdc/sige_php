<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Usuários"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//
$erro = FALSE;
$msg = "";
$usuario_matricula = "";
//
if (!empty($_POST['usuario_buscar']) && $_POST['usuario_buscar'] == "BUSCAR") {
    if (isset($_POST['usuario_matricula']) && !empty($_POST['usuario_matricula'])){
        $usuario_matricula = $_POST['usuario_matricula'];
    }else{
        if (isset($_POST['usuario_nome']) && !empty($_POST['usuario_nome'])) {

            $usuario_nome = $_POST['usuario_nome'];

            $sql = "SELECT * FROM usuarios WHERE nome LIKE '%$usuario_nome%' LIMIT 1";
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
              while ($dados = $query->fetch_array()) {
                    $usuario_matricula = $dados["matricula"];
                }
            }
        }
    }


}
if (!empty($_POST['usuario_atualizar']) && $_POST['usuario_atualizar'] == "ALTERAR") {


    $msg_erro = "";
    $usuario_id = $_POST['usuario_id'];
    $usuario_matricula = $_POST['usuario_matricula'];
    $usuario_email_novo = $_POST['usuario_email_novo'];
    $usuario_telefone_novo_1 = $_POST['usuario_telefone_novo_1'];
    $usuario_telefone_novo_2 = $_POST['usuario_telefone_novo_2'];

    $sql_update = "UPDATE usuarios SET email_alternativo = '$usuario_email_novo', telefone_alternativo_1 = '$usuario_telefone_novo_1', telefone_alternativo_2 = '$usuario_telefone_novo_2' WHERE id = ". $usuario_id;
    if ($mysqli->query($sql_update) === FALSE) {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_usuario . "<br>";
    }else{
        $sql_area = "DELETE FROM permissao_usuario WHERE usuario=$usuario_id";
        if ($mysqli->query($sql_area) === FALSE) {
            $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_area;
        }else{
            if(isset($_POST['permissoes'])) {
                foreach($_POST['permissoes'] as $key => $value) {
                    // insere na tabela acao_linhas
                    $sql_linha = "INSERT INTO permissao_usuario (usuario,permissao) VALUES ($usuario_id,$value)";
                    if ($mysqli->query($sql_linha) === FALSE) {
                        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_linha;
                    }
                }
            }
        }
    }

    if(empty($msg_erro)){
        $mysqli->commit();
        $msg = "Alterado com sucesso!";
    }else{
        $mysqli->rollback();
        $erro = TRUE;
        $msg = "Erro ao alterar permissão.";
    }


?>
<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $msg ?></p>
                <p class="text-warning"><small><?php echo $msg_erro ?></small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#myModalErro").modal('show');
});
</script>
<!-- Modal HTML -->
<?php
}
?>
<div class="container">
<!--<script type="text/javascript" src="../style/js/autocomplete_usuario_nome.js"></script>-->
<h1><b>Buscar usuário: </b></h1>
<hr>
<form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
  <!--
    <br>
    <div class="form-group">
        <label class="control-label col-sm-2" for="usuario_nome">Nome:</label>
        <div class="col-sm-10">
            <input class="form-control" name="usuario_nome" id="usuario_nome" size="100" />
        </div>
    </div>
-->
    <br>
    <div class="form-group">
        <label class="control-label col-sm-2" for="usuario_matricula">Matricula:</label>
        <div class="col-sm-8">
            <input class="form-control" name="usuario_matricula" id="usuario_matricula" size="8" required />
        </div>
    </div>
    <button type="submit" name="usuario_buscar" value="BUSCAR" class="btn btn-default">Buscar</button>

</form>
<?php
    if (!empty($usuario_matricula)){
?>
<hr>
<form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
<?php

        $sql_usuarios = "SELECT * FROM usuarios WHERE matricula = $usuario_matricula ORDER by id";
        $query_usuarios = $mysqli->query($sql_usuarios);

        if ($result_usuarios = $mysqli->query($sql_usuarios)) {
          while ($dados_usuarios = $query_usuarios->fetch_array()) {

                $usuario_nome = $dados_usuarios['nome'];
                $usuario_id = $dados_usuarios['id'];
                $usuario_matricula = $dados_usuarios['matricula'];

                echo "<input type='hidden' name='usuario_id' value='$usuario_id' />";
                echo "<input type='hidden' name='usuario_matricula' value='$usuario_matricula' />";
?>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_nome_dgrh">Nome completo:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_nome_dgrh" id="usuario_nome_dgrh" value="<?php echo $dados_usuarios['nome']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_matricula_dgrh">Matricula:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_matricula_dgrh" id="usuario_matricula_dgrh" value="<?php echo $dados_usuarios['matricula']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_local_dgrh">Local:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_local_dgrh" id="usuario_local_dgrh" value="<?php echo $dados_usuarios['local_dgrh']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_email_dgrh">E-mail DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_email_dgrh" id="usuario_email_dgrh" value="<?php echo $dados_usuarios['email_dgrh']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_email_novo">E-mail Alternativo:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_email_novo" id="usuario_email_novo" value="<?php echo $dados_usuarios['email_alternativo']; ?>"/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_ramal_dgrh">Ramal DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_ramal_dgrh" id="usuario_ramal_dgrh" value="<?php echo $dados_usuarios['ramal_dgrh']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_dgrh">Telefone DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_dgrh" id="usuario_telefone_dgrh" value="<?php echo $dados_usuarios['telefone_dgrh']; ?>" disabled/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_novo_1">Telefone Alternativo 1:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_novo_1" id="usuario_telefone_novo_1" value="<?php echo $dados_usuarios['telefone_alternativo_1']; ?>"/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_novo_2">Telefone Alternativo 2:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_novo_2" id="usuario_telefone_novo_2" value="<?php echo $dados_usuarios['telefone_alternativo_2']; ?>"/>
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="permissoes">Permissões:</label>
            <div class="col-sm-10">
                <?php
                    
                    
                    $sql_permissoes = "SELECT * FROM permissoes";
                    $query_permissoes = $mysqli->query($sql_permissoes);

                    if ($result_permissoes = $mysqli->query($sql_permissoes)) {
                      while ($dados_permissoes = $query_permissoes->fetch_array()) {
                        $permissao_id = $dados_permissoes['id'];
                        $permissao_nome = $dados_permissoes['nome'];
                        
                        $sql_permissao_usuario = "SELECT * FROM permissao_usuario WHERE usuario = $usuario_id AND permissao = $permissao_id";
                        //echo $sql_permissao_usuario;
                        $query_permissao_usuario = $mysqli->query($sql_permissao_usuario);
                        $permissao_marcada = ($query_permissao_usuario->num_rows == 0) ? "" : "checked" ;
                       echo "<label><input type='checkbox' name='permissoes[]' id='permissoes' value='$permissao_id' $permissao_marcada> $permissao_nome</label><br>";
                      }
                    }
                    


                ?>
            </div>
        </div>
        <?php

            }
        }
        ?>
        <button type='submit' name='usuario_atualizar' value='ALTERAR' class="btn btn-default">Alterar</button><br><br>
    </form>

<?php } ?>

<button type="button" class="btn btn-default" onclick="location.href='/usuarios';">< Voltar</button>
</div>