<?php
include '../vendor/autoload.php';
include "../includes/conecta.php";
include('../vendor/mpdf/mpdf/mpdf.php');

ini_set('display_errors', '');
error_reporting(0);

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese'); 
date_default_timezone_set('America/Sao_Paulo');

if (isset($_GET['id'])) {

	$sql = "SELECT u.*
					FROM usuarios u, 
							permissao_usuario pu, 
							permissoes p
					WHERE pu.usuario = u.id 
						and pu.permissao = p.id 
						and p.nome = 'Avaliador'
						and u.id = ".$_GET['id']."
					ORDER BY u.nome";

	if ($query = $mysqli->query($sql)) {
		while ($dados = $query->fetch_array()) {
			$query_dgrh = $pdo->prepare("SELECT LOCAL, EMAIL FROM siarh_sige WHERE matricula = :matricula");
			$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
			$query_dgrh->execute();
			$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
			
			$certificado_nome = $dados['nome'];
			$certificado_email_dgrh = $dgrh_dados['EMAIL'];
			$certificado_email_alternativo = $dados['email_alternativo'];

			$mpdf=new mPDF('utf-8','A4-L');

			$mpdf->SetImportUse();
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->SetDocTemplate('modelo_certificado.pdf');
			$mpdf->AddPage('L');

			$data_hoje = strftime('%d de %B de %Y', strtotime('today'));

			ob_start();

			$html='
				<html>
				<head>
				<style>
				.center {
		  		text-align: center;
				}
				.borda {
		  		border: 1px solid;
				}
				#nome {
					top: 290px;
					font-family: greatvibes;
					font-size: 60px;
					color: #6A1C1D;
					text-align: center;
				}
				.conteudo {
					position: absolute;
					left: -1px;
					width: 100%;
				}
				#texto {
					font-family: sourcesanspro;
					font-size: 20px;
					color: #4F4E4F;
					text-align: center;
					padding: 0px 80px;
				}
				.assinatura {
					font-family: sourcesanspro;
					font-size: 20px;
					color: #4F4E4F;
					padding: 0px 80px;
				}
				</style>
				</head>
				<body>		
					<p class="conteudo center" style="top: 222px; font-size: 20px; color: #4F4E4F;" ><b>Certificamos que</b></p>
					<p class="conteudo" style="top: 290px;" id="nome">'.$certificado_nome.'</p>
					<div class="conteudo" style="top: 380px;">
						<p id="texto">participou como Avaliador no</p>
						<p id="texto">12o. Edital PEC - Projeto de Extensão Comunitária de 2018.</p>
		  			<p id="texto" style="padding-top: 20px;">Campinas, '.$data_hoje.'.</p>
					</div>
					<div class="conteudo" style="top: 595px;">
						<div style="width: 45%; text-align: center;">
							<img style="width: 150px; height: 128px; margin-bottom: -75px;" src="assinaturafernandohashimoto_transp.png">
							<p class="assinatura"><span style="font-size: 19px;"></span>Fernando Hashimoto</p>
							<p class="assinatura"><span style="font-size: 19px;">Pró-Reitor de Extensão e Cultura</span></p>
						</div>
					</div>
				</body>
				</html>
			';
			echo $html;
			$html = ob_get_contents();
			ob_end_clean();

			$mpdf->WriteHTML($html);
			
			$content = $mpdf->Output('', 'S');


			// Create the Transport
			$transport = (new Swift_SmtpTransport('192.168.0.122', 25))
			  ->setUsername('sistemas')
			  ->setPassword('*Pro@SQ')
			;

			$attachment = new Swift_Attachment($content, 'certificado.pdf', 'application/pdf');

			// Create the Mailer using your created Transport
			$mailer = new Swift_Mailer($transport);

			// Create a message
			$message = (new Swift_Message('Seu de certificado de avaliador do SIGE'));
			$message->setFrom(['nao-responda@mailing.extecamp.unicamp.br' => 'SIGE']);
			if (!empty($certificado_email_dgrh)) {
				$message->addTo($certificado_email_dgrh);
			}
			if (!empty($certificado_email_alternativo)) {
				$message->addTo($certificado_email_alternativo);
			}
			$message->setBody('Segue seu certificado em anexo.');
			$message->attach($attachment);

			// Send the message
			$result = $mailer->send($message);	

			$mpdf->Output($filename ,'I');
		}
	}	


	exit;

}
//echo $html;
?>

