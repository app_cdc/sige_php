<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Usuários"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
?>
<div class="container">
	<h1><b>Usuários</b></h1>
	<hr>
	<button onclick="window.location.href='novo.php'" class="btn btn-primary btn-lg" >
			<span class="glyphicon glyphicon-plus"></span> Inserir 
	</button>&nbsp;
	<button onclick="window.location.href='editar.php'" class="btn btn-primary btn-lg" >
			<span class="glyphicon glyphicon-edit"></span> Alterar 
	</button>&nbsp;
	<button onclick="window.location.href='../pendentes'" class="btn btn-primary btn-lg" >
			Pendentes 
	</button>
  <br>
  <hr>

  <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#Administradores">Administradores <i class="fa"></i></a></li>
      <li><a href="#Conselheiros" data-toggle="tab">Conselheiros <i class="fa"></i></a></li>
      <li><a href="#Avaliadores" data-toggle="tab">Avaliadores <i class="fa"></i></a></li>
      <li><a href="#Coordenadores" data-toggle="tab">Coordenadores <i class="fa"></i></a></li>
      <li><a href="#Todos" data-toggle="tab">Todos <i class="fa"></i></a></li>
  </ul>
  <div class="tab-content">
      <div id="Administradores" class="tab-pane fade in active" >
		  	<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Email</th>
			        <th class="col-md-3">Email Alternativo</th>
			        <th class="col-md-3">Local</th>
			        <th class="col-md-3">Ramal</th>
			        <th class="col-md-3">Telefone</th>
			        <th class="col-md-3">Telefone Alternativo 1</th>
			        <th class="col-md-3">Telefone Alternativo 2</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT u.*
										FROM usuarios u, 
												permissao_usuario pu, 
												permissoes p
										WHERE pu.usuario = u.id 
											and pu.permissao = p.id 
											and p.nome = 'Administrador'
										ORDER BY u.nome";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
							$query_dgrh = $pdo->prepare("SELECT * FROM siarh_sige WHERE matricula = :matricula");
							$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
							$query_dgrh->execute();
							$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>
			      <tr>
			        <td><?php echo $dados['matricula'];?></td>
			        <td><?php echo $dados['nome'];?></td>
			        <td><?php echo (empty($dados['email_dgrh'])) ? $dgrh_dados['EMAIL'] : $dados['email_dgrh'];?></td>
			        <td><?php echo $dados['email_alternativo'];?></td>
			        <td><?php echo (empty($dados['local_dgrh'])) ? $dgrh_dados['LOCAL'] : $dados['local_dgrh'];?></td>
			        <td><?php echo (empty($dados['ramal_dgrh'])) ? $dgrh_dados['RAMAL'] : $dados['ramal_dgrh'];?></td>
			        <td><?php echo (empty($dados['telefone_dgrh'])) ? $dgrh_dados['TELEFONE'] : $dados['telefone_dgrh'];?></td>
			        <td><?php echo $dados['telefone_alternativo_1'];?></td>
			        <td><?php echo $dados['telefone_alternativo_2'];?></td>
			    	</tr>
					<?php
							}
						}
					?>    
			    </tbody>
			  </table>
		  </div><!--div tab-pane-->

		  <div id="Conselheiros" class="tab-pane fade">
		  	<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Email</th>
			        <th class="col-md-3">Email Alternativo</th>
			        <th class="col-md-3">Local</th>
			        <th class="col-md-3">Ramal</th>
			        <th class="col-md-3">Telefone</th>
			        <th class="col-md-3">Telefone Alternativo 1</th>
			        <th class="col-md-3">Telefone Alternativo 2</th>
							<th class="col-md-3">Certificado</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT u.*
										FROM usuarios u, 
												permissao_usuario pu, 
												permissoes p
										WHERE pu.usuario = u.id 
											and pu.permissao = p.id 
											and p.nome = 'Conselheiro'
										ORDER BY u.nome";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
							$query_dgrh = $pdo->prepare("SELECT * FROM siarh_sige WHERE matricula = :matricula");
							$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
							$query_dgrh->execute();
							$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>    
			      <tr>
			        <td><?php echo $dados['matricula'];?></td>
			        <td><?php echo $dados['nome'];?></td>
			        <td><?php echo (empty($dados['email_dgrh'])) ? $dgrh_dados['EMAIL'] : $dados['email_dgrh'];?></td>
			        <td><?php echo $dados['email_alternativo'];?></td>
			        <td><?php echo (empty($dados['local_dgrh'])) ? $dgrh_dados['LOCAL'] : $dados['local_dgrh'];?></td>
			        <td><?php echo (empty($dados['ramal_dgrh'])) ? $dgrh_dados['RAMAL'] : $dados['ramal_dgrh'];?></td>
			        <td><?php echo (empty($dados['telefone_dgrh'])) ? $dgrh_dados['TELEFONE'] : $dados['telefone_dgrh'];?></td>
			        <td><?php echo $dados['telefone_alternativo_1'];?></td>
			        <td><?php echo $dados['telefone_alternativo_2'];?></td>
			        <td class="text-center">
			        	<a href="certificado_conselheiro.php?id=<?php echo $dados['id']; ?>" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-send"></span></a>
							</td>
			    	</tr>
					<?php
							}
						}
					?>    
			    </tbody>
			  </table>
      </div><!--div tab-pane-->

		  <div id="Avaliadores" class="tab-pane fade">
		  	<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Email</th>
			        <th class="col-md-3">Email Alternativo</th>
			        <th class="col-md-3">Local</th>
			        <th class="col-md-3">Ramal</th>
			        <th class="col-md-3">Telefone</th>
			        <th class="col-md-3">Telefone Alternativo 1</th>
			        <th class="col-md-3">Telefone Alternativo 2</th>
			        <th class="col-md-3">Certificado</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT u.*
										FROM usuarios u, 
												permissao_usuario pu, 
												permissoes p
										WHERE pu.usuario = u.id 
											and pu.permissao = p.id 
											and p.nome = 'Avaliador'
										ORDER BY u.nome";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
							$query_dgrh = $pdo->prepare("SELECT * FROM siarh_sige WHERE matricula = :matricula");
							$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
							$query_dgrh->execute();
							$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>    
			      <tr>
			        <td><?php echo $dados['matricula'];?></td>
			        <td><?php echo $dados['nome'];?></td>
			        <td><?php echo (empty($dados['email_dgrh'])) ? $dgrh_dados['EMAIL'] : $dados['email_dgrh'];?></td>
			        <td><?php echo $dados['email_alternativo'];?></td>
			        <td><?php echo (empty($dados['local_dgrh'])) ? $dgrh_dados['LOCAL'] : $dados['local_dgrh'];?></td>
			        <td><?php echo (empty($dados['ramal_dgrh'])) ? $dgrh_dados['RAMAL'] : $dados['ramal_dgrh'];?></td>
			        <td><?php echo (empty($dados['telefone_dgrh'])) ? $dgrh_dados['TELEFONE'] : $dados['telefone_dgrh'];?></td>
			        <td><?php echo $dados['telefone_alternativo_1'];?></td>
			        <td><?php echo $dados['telefone_alternativo_2'];?></td>
			        <td class="text-center">
			        	<a href="certificado_avaliador.php?id=<?php echo $dados['id']; ?>" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-send"></span></a>
							</td>
			    	</tr>
					<?php
							}
						}
					?>    
			    </tbody>
			  </table>
      </div><!--div tab-pane-->

		  <div id="Coordenadores" class="tab-pane fade">
		  	<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Email</th>
			        <th class="col-md-3">Local</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT u.*
										FROM usuarios u, 
												permissao_usuario pu, 
												permissoes p 
										WHERE pu.usuario = u.id 
											and pu.permissao = p.id 
											and p.nome = 'Coordenador'
										ORDER BY u.nome";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
							$query_dgrh = $pdo->prepare("SELECT * FROM siarh_sige WHERE matricula = :matricula");
							$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
							$query_dgrh->execute();
							$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>    
			      <tr>
			        <td><?php echo $dados['matricula'];?></td>
			        <td><?php echo $dados['nome'];?></td>
			        <td><?php echo $dgrh_dados['EMAIL'];?></td>
			        <td><?php echo $dgrh_dados['LOCAL'];?></td>
			    	</tr>
					<?php
							}
						}
					?>    
			    </tbody>
			  </table>
      </div><!--div tab-pane-->

			<div id="Todos" class="tab-pane fade">
				<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Email</th>
			        <th class="col-md-3">Email Alternativo</th>
			        <th class="col-md-3">Local</th>
			        <th class="col-md-3">Ramal</th>
			        <th class="col-md-3">Telefone</th>
			        <th class="col-md-3">Telefone Alternativo 1</th>
			        <th class="col-md-3">Telefone Alternativo 2</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT u.* 
											FROM usuarios u 
										ORDER BY u.nome";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
							$query_dgrh = $pdo->prepare("SELECT * FROM siarh_sige WHERE matricula = :matricula");
							$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
							$query_dgrh->execute();
							$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>    
			      <tr>
			        <td><?php echo $dados['matricula'];?></td>
			        <td><?php echo $dados['nome'];?></td>
			        <td><?php echo (empty($dados['email_dgrh'])) ? $dgrh_dados['EMAIL'] : $dados['email_dgrh'];?></td>
			        <td><?php echo $dados['email_alternativo'];?></td>
			        <td><?php echo (empty($dados['local_dgrh'])) ? $dgrh_dados['LOCAL'] : $dados['local_dgrh'];?></td>
			        <td><?php echo (empty($dados['ramal_dgrh'])) ? $dgrh_dados['RAMAL'] : $dados['ramal_dgrh'];?></td>
			        <td><?php echo (empty($dados['telefone_dgrh'])) ? $dgrh_dados['TELEFONE'] : $dados['telefone_dgrh'];?></td>
			        <td><?php echo $dados['telefone_alternativo_1'];?></td>
			        <td><?php echo $dados['telefone_alternativo_2'];?></td>
			    	</tr>
					<?php
							}
						}
					?>    
			    </tbody>
			  </table>
      </div><!--div tab-pane-->
  </div><!--div tab-content-->



</div>

<script type="text/javascript">
    $(document).ready( function(){
       $("#usuarios").addClass("active");
    });
</script>