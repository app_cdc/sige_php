<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Usuários"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//
$erro = FALSE;
//
if (!empty($_POST['usuario_inserir'])) {

    $msg = "";
    $msg_erro = "";

    $usuario_nome_dgrh = $_POST['usuario_nome_dgrh'];
    $usuario_matricula_dgrh = $_POST['usuario_matricula_dgrh'];
    $usuario_local_dgrh = $_POST['usuario_local_dgrh'];
    $usuario_email_dgrh = $_POST['usuario_email_dgrh'];
    $usuario_email_novo = $_POST['usuario_email_novo'];
    $usuario_ramal_dgrh = $_POST['usuario_ramal_dgrh'];
    $usuario_telefone_dgrh = $_POST['usuario_telefone_dgrh'];
    $usuario_telefone_novo_1 = $_POST['usuario_telefone_novo_1'];
    $usuario_telefone_novo_2 = $_POST['usuario_telefone_novo_2'];

    $sql_usuario_existente = "SELECT * FROM usuarios u WHERE u.matricula = $usuario_matricula_dgrh";
    if ($result_usuario_existente = $mysqli->query($sql_usuario_existente)) {
        if($result_usuario_existente->num_rows > 0){
            $erro = TRUE;
            $msg = "Usuário já cadastrado no sistema";
        }else{
            $sql_usuario = "INSERT INTO usuarios (nome,matricula,email_dgrh,email_alternativo,telefone_dgrh,telefone_alternativo_1,telefone_alternativo_2,ramal_dgrh,local_dgrh) VALUES ('$usuario_nome_dgrh',$usuario_matricula_dgrh,'$usuario_email_dgrh','$usuario_email_novo','$usuario_telefone_dgrh','$usuario_telefone_novo_1','$usuario_telefone_novo_2','$usuario_ramal_dgrh','$usuario_local_dgrh')";
            if ($mysqli->query($sql_usuario) === FALSE) {
                $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_usuario . "<br>";
            }else{
                if(isset($_POST['permissoes'])) {

                    $usuario_id = $mysqli->insert_id;

                    foreach($_POST['permissoes'] as $key => $value) {

                        // insere na tabela acao_linhas
                        $sql_linha = "INSERT INTO permissao_usuario (usuario,permissao) VALUES ($usuario_id,$value)";
                        if ($mysqli->query($sql_linha) === FALSE) {
                            $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_linha . "<br>";
                        }
                    }
                }
            }

            if(empty($msg_erro)){
                $mysqli->commit();
                $msg = "Inserido com sucesso!";
            }else{
                $mysqli->rollback();
                $erro = TRUE;
                $msg = "Erro ao inserir.";
            }
        }
    }



?>

<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $msg ?></p>
                <p class="text-warning"><small><?php echo $msg_erro ?></small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#myModalErro").modal('show');
});
</script>
<!-- Modal HTML -->

<?php
}

?>
<div class="container">
    <h1><b>Novo usuário: </b></h1>
    <hr>
    <form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
      
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_nome_dgrh">Nome completo:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_nome_dgrh" id="usuario_nome_dgrh" size="100" required />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_matricula_dgrh">Matricula:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_matricula_dgrh" id="usuario_matricula_dgrh" size="8" required />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_local_dgrh">Local:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_local_dgrh" id="usuario_local_dgrh" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_email_dgrh">E-mail DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_email_dgrh" id="usuario_email_dgrh" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_email_novo">E-mail Alternativo:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_email_novo" id="usuario_email_novo" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_ramal_dgrh">Ramal DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_ramal_dgrh" id="usuario_ramal_dgrh" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_dgrh">Telefone DGRH:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_dgrh" id="usuario_telefone_dgrh" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_novo_1">Telefone Alternativo 1:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_novo_1" id="usuario_telefone_novo_1" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_telefone_novo_2">Telefone Alternativo 2:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_telefone_novo_2" id="usuario_telefone_novo_2" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-2" for="permissoes">Permissões:</label>
            <div class="col-sm-10">
                <?php
                $sql = "SELECT * FROM permissoes";
                $query = $mysqli->query($sql);

                if ($result = $mysqli->query($sql)) {
                  while ($dados = $query->fetch_array()) {
                    $permissao_id = $dados['id'];
                    $permissao_nome = $dados['nome'];
                    
                   echo "<label><input type='checkbox' name='permissoes[]' id='permissoes' value='$permissao_id'> $permissao_nome </label><br>";
                  }
                }
                ?>
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="location.href='/usuarios';">< Voltar</button>
        <button type="submit" class="btn btn-default" name="usuario_inserir" value="Inserir usuário">Inserir</button>

    </form>
</div>
<script type="text/javascript">
  $(function() {

      // Atribui evento e função para limpeza dos campos
      $('#usuario_nome_dgrh').on('input', limpaCampos);

      // Dispara o Autocomplete a partir do segundo caracter
      $( "#usuario_nome_dgrh" ).autocomplete({
        minLength: 2,
        source: function( request, response ) {
            $.ajax({
                url: "consulta.php",
                dataType: "json",
                data: {
                  acao: 'autocomplete',
                    parametro: $('#usuario_nome_dgrh').val()
                },
                success: function(data) {
                   response(data);
                }
            });
        },
        focus: function( event, ui ) {
            $("#usuario_nome_dgrh").val( ui.item.NOME );
            $("#usuario_matricula_dgrh").removeAttr('readonly');
            $("#usuario_matricula_dgrh").val( ui.item.MATRICULA );
            $("#usuario_matricula_dgrh").attr('readonly','true');
            $("#usuario_local_dgrh").removeAttr('readonly');
            $("#usuario_local_dgrh").val( ui.item.LOCAL );
            $("#usuario_local_dgrh").attr('readonly','true');
            $("#usuario_email_dgrh").removeAttr('readonly');
            $("#usuario_email_dgrh").val( ui.item.EMAIL );
            $("#usuario_email_dgrh").attr('readonly','true');
            $("#usuario_ramal_dgrh").removeAttr('readonly');
            $("#usuario_ramal_dgrh").val( ui.item.RAMAL );
            $("#usuario_ramal_dgrh").attr('readonly','true');
            $("#usuario_telefone_dgrh").removeAttr('readonly');
            $("#usuario_telefone_dgrh").val( ui.item.TELEFONE );
            $("#usuario_telefone_dgrh").attr('readonly','true');
            return false;
        },
        select: function( event, ui ) {
            $("#usuario_nome_dgrh").val( ui.item.NOME );
            $("#usuario_matricula_dgrh").removeAttr('readonly');
            $("#usuario_matricula_dgrh").val( ui.item.MATRICULA );
            $("#usuario_matricula_dgrh").attr('readonly','true');
            $("#usuario_local_dgrh").removeAttr('readonly');
            $("#usuario_local_dgrh").val( ui.item.LOCAL );
            $("#usuario_local_dgrh").attr('readonly','true');
            $("#usuario_email_dgrh").removeAttr('readonly');
            $("#usuario_email_dgrh").val( ui.item.EMAIL );
            $("#usuario_email_dgrh").attr('readonly','true');
            $("#usuario_ramal_dgrh").removeAttr('readonly');
            $("#usuario_ramal_dgrh").val( ui.item.RAMAL );
            $("#usuario_ramal_dgrh").attr('readonly','true');
            $("#usuario_telefone_dgrh").removeAttr('readonly');
            $("#usuario_telefone_dgrh").val( ui.item.TELEFONE );
            $("#usuario_telefone_dgrh").attr('readonly','true');
            return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
          .append( "<a><b>Nome: </b>" + item.NOME + "<b> Matricula: </b>" + item.MATRICULA + " <b> Unidade: </b>" + item.LOCAL + "</a><br>" )
          .appendTo( ul );
      };

    // Função para limpar os campos caso a busca esteja vazia
    function limpaCampos(){
      var busca = $('#usuario_nome_dgrh').val();

      if(busca == ""){
        $("#usuario_matricula_dgrh").removeAttr('readonly');
        $("#usuario_matricula_dgrh").val('');
        $("#usuario_matricula_dgrh").attr('readonly','true');
        $("#usuario_local_dgrh").removeAttr('readonly');
        $("#usuario_local_dgrh").val('');
        $("#usuario_local_dgrh").attr('readonly','true');
        $("#usuario_email_dgrh").removeAttr('readonly');
        $("#usuario_email_dgrh").val('');
        $("#usuario_email_dgrh").attr('readonly','true');
        $("#usuario_ramal_dgrh").removeAttr('readonly');
        $("#usuario_ramal_dgrh").val('');
        $("#usuario_ramal_dgrh").attr('readonly','true');
        $("#usuario_telefone_dgrh").removeAttr('readonly');
        $("#usuario_telefone_dgrh").val('');
        $("#usuario_telefone_dgrh").attr('readonly','true');
      }
    }
  });  
</script>