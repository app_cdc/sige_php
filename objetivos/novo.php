<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Inserir objetivo"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

?>
<script type="text/javascript">
function apagar_obj_esp(obj_esp_id,acao_id)
{
  $.ajax({
    type: 'post',
    url: 'apagar.php',
    data: {
      id:obj_esp_id
    },
    success: function (response) {
      if(response){
        location.replace("novo.php?acao_id="+acao_id);
      }else{
        document.getElementById("msg").innerHTML="Erro ao apagar";
      }
    }
  });
}
</script>

<div class="container">
<?php
//
$erro = FALSE;
$msg = "";
//
if( !empty($_POST) ){

    $obj_esp_acao_id = $_POST['acao_id'];
    $obj_esp_descricao = $mysqli->real_escape_string($_POST['descricao']);

    $sql = "INSERT INTO objetivos_especificos (acao_extensao,descricao) VALUES ($obj_esp_acao_id,'$obj_esp_descricao')";
    

    if ($mysqli->query($sql) === TRUE) {

      $msg = "Objetivo específico inserirdo com sucesso";
      $erro = FALSE;

    } else {
        $msg .= "Erro ao inserir o objetivo especifico. Error: " . $mysqli->error . "<br>" . $sql;
        $erro = TRUE;
    }

    if($erro){
        $mysqli->rollback();
    }else{
        $mysqli->commit();
    }

    echo "<div>$msg</div>";

}

if(!$erro){

  if (isset($_GET['acao_id'])) {

    $apagar_link = "";

    $acao_id = $_GET['acao_id'];
      
    $sql = "SELECT * FROM acoes_extensao WHERE id=".$acao_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {

        echo "<h3>Objetivos Específicos da Ação de Extensão: ".$dados['titulo']."</h3>";
        echo "<p>Nesta fase do preenchimento da Proposta será necessário detalhar seus <b>Objetivos Específicos</b> (um ou mais) e as <b>Ações</b> (Descrição e Resultados Esperados) que serão necessárias para o alcance de cada um dos <b>Objetivos Específicos</b> definidos. Este é um importante elemento de estruturação e, portanto, de avaliação da proposta: a coerência e vinculação entre o <b>Objetivo Geral</b>, seu(s) <b>Objetivo(s) Específico(s)</b> e as <b>Ações</b> previstas para cada um deles.</p>";
        $objetivo_geral =  "<p><b>Objetivo Geral:</b> ".$dados['objetivo_geral']."</p>";
      }
    }

?>
<hr>
<form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
  
  <input type="hidden" name="acao_id" value="<?php echo $acao_id ?>">
  <h4>Descrição do Objetivo Específico: (máx 1000 caracteres)</h4>

  <div class="form-group">
      <div class="col-sm-12">
          <textarea class="form-control" rows="5" name="descricao" maxlength="1000" required autofocus></textarea>
      </div>
  </div><!--div form-group-->  
  <br>
  <br>
  <br>  
  <br>
  <br>

  <div>
    <ul class="pager">
        <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-plus"></span> Adicionar</button></li>
    </ul>
  </div>

</form>

<?php
//
?>

<hr>


<table class="table table-striped">

    <thead>
      <tr class="tabela_cabecalho">
        <th colspan="2">Operação</th>
        <th>Objetivo Específico</th>
        <th>Ação</th>
        <th>Resultado</th>
      </tr>
    </thead>

    <tbody>     



<?php

    $sql = "SELECT oe.id as obj_esp_id, oe.descricao as obj_esp_descricao, ao.id as acao_esp_id, ao.descricao as acao, ao.resultado as resultado FROM objetivos_especificos oe LEFT JOIN acoes_objetivos ao ON oe.id = ao.objetivo_especifico WHERE oe.acao_extensao=".$acao_id;
    //$sql = "SELECT * FROM objetivos_especificos WHERE acao_extensao=".$acao_id;
    $query = $mysqli->query($sql);

    $verifica_obj_esp_diferente = 0;

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {

        $sql_acao = "SELECT * FROM acoes_objetivos WHERE objetivo_especifico=".$dados['obj_esp_id'];
        $query_acao = $mysqli->query($sql_acao);
        $count_acoes_esp = $query_acao->num_rows;

        
?>
    <tr>
<?php
        if ($verifica_obj_esp_diferente != $dados['obj_esp_id']){
          $verifica_obj_esp_diferente = $dados['obj_esp_id'];

          //$apagar_link =  $dados['obj_esp_id'].",".$acao_id;
?>
      <td width=65 rowspan="<?php echo ($count_acoes_esp == 0) ? 1 : $count_acoes_esp ; ?>">
        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal" data-obj-item="<?php echo $dados['obj_esp_id']; ?>" data-obj-acao="<?php echo $acao_id; ?>">
          <span class="glyphicon glyphicon-trash"></span>
        </button>
      </td>

      <td width=100 rowspan="<?php echo ($count_acoes_esp == 0) ? 1 : $count_acoes_esp ; ?>">
        <a class="btn btn-success" role="button" href="inserir_acao.php?id=<?php echo $dados['obj_esp_id'] ?>"><span class="glyphicon glyphicon-plus"></span> Inserir Ação</a>
      </td>

      <td rowspan="<?php echo ($count_acoes_esp == 0) ? 1 : $count_acoes_esp ; ?>">
        <a data-toggle="tooltip" title="Clique para editar" data-placement="right"  href="editar.php?obj_esp_id=<?php echo $dados['obj_esp_id'] ?>"><?php echo $dados['obj_esp_descricao'] ?></a>
      </td>
<?php
        }
?>    
      <td><?php echo $dados['acao'] ?></td>
      <td><?php echo $dados['resultado'] ?></td>
      
    </tr>
<?php 
      }
    }
?>
  </tbody>
</table>
<?php
echo $objetivo_geral;
?>
  <div>
    <ul class="pager">
        <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='../acoes_extensao/show.php?id=<?php echo $acao_id?>';">< Voltar ao Principal</button></li>
    </ul>
  </div>

<?php
  }
}
?>

</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>Deseja apagar mesmo?</p>
                <p class="text-warning"><small>Irá apagar as ações e resultados vinculados ao objetivo</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnApagar">Apagar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                descricao: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a descrição'
                        },
                        stringLength: {
                            max: 500,
                            message: 'A descrição deve ter no máximo 500 caracteres'
                        }
                    }
                }
            }
        });


    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var objetivo_item_id = button.data('objItem'); // Extract info from data-* attributes
      var objetivo_acao_id = button.data('objAcao'); // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('#btnApagar').click(function() {
        apagar_obj_esp(objetivo_item_id,objetivo_acao_id);
      });
    });

});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
