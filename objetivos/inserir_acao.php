<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Inserir Ações e Resultados Específicos"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

?>
<script type="text/javascript">
function apagar_obj_esp_acao(obj_esp_acao_id,obj_esp_id)
{
  $.ajax({
    type: 'post',
    url: 'apagar_acao.php',
    data: {
      id:obj_esp_acao_id
    },
    success: function (response) {
      if(response){
        //document.getElementById("msg").innerHTML="Apagado com sucesso";
        location.replace("inserir_acao.php?id="+obj_esp_id);
      }else{
        document.getElementById("msg").innerHTML="Erro ao apagar";
      }
    }
  });
}
</script>

<div class="container">
<div id="msg"></div>
<?php
//
$erro = FALSE;
$msg = "";
//
if( !empty($_POST) ){

    $acao_obj_esp_id = $_POST['objetivo_id'];
    $acao_obj_esp_descricao = $mysqli->real_escape_string($_POST['descricao']);
    $acao_obj_esp_resultado = $mysqli->real_escape_string($_POST['resultado']);

    $sql = "INSERT INTO acoes_objetivos (objetivo_especifico,descricao,resultado) VALUES ($acao_obj_esp_id,'$acao_obj_esp_descricao','$acao_obj_esp_resultado')";

    if ($mysqli->query($sql) === TRUE) {

      $msg = "Ação e resultado inserido com sucesso";
      $erro = FALSE;

    } else {
        $msg .= "Erro ao inserir a ação e o resultado. Error: " . $mysqli->error . "<br>" . $sql;
        $erro = TRUE;
    }

    if($erro){
        $mysqli->rollback();
    }else{
        $mysqli->commit();
    }

    echo "<script>document.getElementById('msg').innerHTML=$msg</script>";
}

if(!$erro){

 
  if (isset($_GET['id'])) {
    
    $apagar_link = "";

    $objetivo_id = $_GET['id'];
    $acao_id = "";

    $sql = "SELECT * FROM objetivos_especificos WHERE id=".$objetivo_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {
        $acao_id = $dados['acao_extensao'];
        echo "<h3>Inserir Ações e Resultados para o Objetivo Específico: </h3><br><p><b>Objetivo Específico:</b> ".$dados['descricao']."</p>";
      }
    }

?>
<hr>
<form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">

  <input type="hidden" name="objetivo_id" value="<?php echo $objetivo_id ?>">
  <h4>Descrição da Ação: (máx 1000 caracteres)</h4>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" name="descricao" maxlength="1000" required autofocus></textarea>
                    </div>
                </div><!--div form-group-->  
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

  <h4>Resultado esperado da Ação: (máx 1000 caracteres)</h4>

  <div class="form-group">
      <div class="col-sm-12">
          <textarea class="form-control" rows="5" name="resultado" maxlength="1000" required ></textarea>
      </div>
  </div><!--div form-group-->  
  <br>
  <br>
  <br>
  <br>
  <br>

  <div>
    <ul class="pager">
        <li>
          <button type="button" class="btn btn-default btnAnterior" onclick="location.href='novo.php?acao_id=<?php echo $acao_id?>';">< Voltar para Objetivos</button>
        </li>
        <li>
          <button type="submit" class="btn btn-success btn-lg" id="btnSalvar">
            <span class="glyphicon glyphicon-plus"></span> Inserir Ação
          </button>
        </li>
    </ul>
  </div>

</form>
<hr>

<table class="table table-striped">

    <thead>
      <tr class="tabela_cabecalho">
        <th>Ação</th>
        <th>Resultado</th>
        <th>Excluir</th>
      </tr>
    </thead>

    <tbody>     

<?php

    $sql = "SELECT * FROM acoes_objetivos WHERE objetivo_especifico=".$objetivo_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {
        //$apagar_link = $dados['id'].",".$objetivo_id;
?>
    <tr>
      <td><?php echo $dados['descricao'] ?></td>
      <td><?php echo $dados['resultado'] ?></td>
      <td width=65 style="text-align: center;">
        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal" data-obj-item="<?php echo $dados['id']; ?>" data-obj-id="<?php echo $objetivo_id; ?>">
          <span class="glyphicon glyphicon-trash"></span>
        </button>
      </td>
    </tr>

<?php
      }
    }
?>
  </tbody>
</table>
<?php    
  }

}
?>

</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>Deseja apagar mesmo?</p>
                <p class="text-warning"><small>Se você apagar, não terá volta</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnApagar">Apagar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                descricao: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a descrição'
                        },
                        stringLength: {
                            max: 500,
                            message: 'A descrição deve ter no máximo 500 caracteres'
                        }
                    }
                },
                resultado: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o resultado'
                        },
                        stringLength: {
                            max: 500,
                            message: 'O resultado deve ter no máximo 500 caracteres'
                        }
                    }
                }
            }
        });

    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var objetivo_item_id = button.data('objItem'); // Extract info from data-* attributes
      var objetivo_id = button.data('objId'); // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('#btnApagar').click(function() {
        apagar_obj_esp_acao(objetivo_item_id,objetivo_id);
      });
    });

});
</script>
