<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Alterar objetivo"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

?>
<div class="container">
<?php
//
$erro = FALSE;
$msg = "";
//
if( !empty($_POST) ){

    $obj_esp_acao_id = $_POST['obj_esp_acao_id'];
    $obj_esp_id = $_POST['obj_esp_id'];
    $obj_esp_descricao = $mysqli->real_escape_string($_POST['descricao']);

    $sql = "UPDATE objetivos_especificos SET descricao = '$obj_esp_descricao' WHERE id = $obj_esp_id";

    if ($mysqli->query($sql) === TRUE) {

      $msg = "Objetivo específico salvo com sucesso";
      $erro = FALSE;

    } else {
        $msg .= "Erro ao salvar o objetivo especifico. Error: " . $mysqli->error . "<br>" . $sql;
        $erro = TRUE;
    }

    if($erro){
        $mysqli->rollback();
    }else{
        $mysqli->commit();
    }

    echo "<div>$msg</div>";

}

if(!$erro){

  if (isset($_GET['obj_esp_id'])) {

    $obj_esp_acao_id = "";
    $obj_esp_id = $_GET['obj_esp_id'];
    $obj_esp_descricao = "";
      
    //
    $sql_obj_esp = "SELECT * FROM objetivos_especificos WHERE id=".$obj_esp_id;
    $query_obj_esp = $mysqli->query($sql_obj_esp);

    if ($result_obj_esp = $mysqli->query($sql_obj_esp)) {
      while ($dados_obj_esp = $query_obj_esp->fetch_array()) {

        $obj_esp_acao_id = $dados_obj_esp['acao_extensao'];
        $obj_esp_descricao = $dados_obj_esp['descricao'];

      }
    }

?>
<hr>
<form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
  
  <input type="hidden" name="obj_esp_acao_id" value="<?php echo $obj_esp_acao_id ?>">
  <input type="hidden" name="obj_esp_id" value="<?php echo $obj_esp_id ?>">
  <h4>Descrição do Objetivo Específico: (máx 1000 caracteres)</h4>

  <div class="form-group">
      <div class="col-sm-12">
          <textarea class="form-control" rows="5" name="descricao" maxlength="1000" required autofocus><?php echo $obj_esp_descricao; ?></textarea>
      </div>
  </div><!--div form-group-->  
  <br>
  <br>
  <br>
  <br>
  <br>

  <div>
    <ul class="pager">
        <li><button type="button" class="btn btn-default btnAnterior" onclick="location.href='novo.php?acao_id=<?php echo $obj_esp_acao_id?>';">< Voltar para Objetivos</button></li>
        <li><button type="submit" class="btn btn-success btn-lg" id="btnSalvar"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button></li>
    </ul>
  </div>

</form>

<?php
  }
}
?>

</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                descricao: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a descrição'
                        },
                        stringLength: {
                            max: 500,
                            message: 'A descrição deve ter no máximo 500 caracteres'
                        }
                    }
                }
            }
        });
});
</script>
