<?php
header('Content-Type: text/html; charset=utf-8');
# layout.php
define( 'DS', DIRECTORY_SEPARATOR );
define( 'BASE_DIR', dirname( __FILE__ ) . DS );

include "includes/conecta.php";
include "includes/validasessao.php";
require 'vendor/autoload.php';
include "lib/permissao.php";

if (!function_exists('before')) {
    function before ($isso, $naquilo)
    {
        return substr($naquilo, 0, strpos($naquilo, $isso));
    };
}
// Se a sessão não existir, inicia uma
if (isset($_SESSION['UsuarioNome'])){
    setcookie("logado", "SIM");
    $usuario_logado_nome = before(" ", $_SESSION['UsuarioNome']);
} else {
    setcookie("logado", "", 1);
    $usuario_logado_nome = '';
}

require_once(BASE_DIR . 'lib/PageTemplate.php');
?>
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="../style/css/mystyle.css">
        <link rel="stylesheet" href="../style/css/bootstrapValidator.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
        <!-- datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">

        <link rel="icon" href="../style/images/favicon.png" />
        <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../style/js/bootstrapValidator.js"></script>
        <script src="../style/js/language/pt_BR.js"></script>
        <script type="text/javascript" src="../style/js/jquery.maskMoney.js"></script>
        <!-- datatables -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>


        <script type="text/javascript" src="../style/js/custom.js"></script>
        <title>SIGE<?php if(isset($TPL->PageTitle)) { echo " - ". $TPL->PageTitle; } ?></title>
        <?php if(isset($TPL->ContentHead)) { include $TPL->ContentHead; } ?>
    </head>

    <body>
    <div id="menu">
        <?php include BASE_DIR."includes/menu.php";	?>
    </div>
    <div id="content">
        <?php if(isset($TPL->ContentBody)) { include $TPL->ContentBody; } ?>
    </div>
    <br><br><br>
    <footer class="container-fluid text-center">
        <?php include BASE_DIR."includes/rodape.php"; ?>
    </footer>
    </body>

</html>