<?php
// TEMPLATE
require_once('lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Home"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "layout.php";
    exit;
}
// END TEMPLATE
?>

<div class="container">


<h1><b>Contato</b></h1>
<hr>

<p>
    <strong>Telefone</strong>: (19) 3521-4753
    <br />
    <strong>E-mail Institucional</strong>: <a href="mailto:pec@unicamp.br">pec@unicamp.br</a>
    <br />
</p>
<p>
    <strong>Endereço</strong>:<br />
Av. Érico Veríssimo, 500 &#8211; Cidade Universitária &#8220;Zeferino Vaz&#8221; &#8211; Barão Geraldo<br />
CEP 13083-851 &#8211; Campinas &#8211; SP</p>
<p><strong>Mapa</strong>:</p>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d919.4150228302233!2d-47.07240827081011!3d-22.81505559906616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjLCsDQ4JzU0LjIiUyA0N8KwMDQnMTguNyJX!5e0!3m2!1spt-BR!2sbr!4v1552067767613" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<hr>

</div>
<script type="text/javascript">
    $(document).ready( function(){
       $("#contato").addClass("active");
    });
</script>