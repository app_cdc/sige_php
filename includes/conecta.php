<?php
include 'settings.php';

//MySql
$mysqli = new mysqli($servidor, $usuario_bd, $senha_bd, $banco);

if ($mysqli->connect_error) {
    die('Não foi possível fazer a conexão com o banco de dados (' . $mysqli->connect_errno . ') '
        . $mysqli->connect_error);
}

if (!$mysqli->set_charset("utf8")) {
      printf("Error loading character set utf8: %s\n", $mysqli->error);
}

$mysqli->autocommit(FALSE);

$_SG['conexao_bd_mysqli'] = $mysqli;


//MSSQL
try {
  //Establishes the connection
  $pdo = new PDO ("dblib:host=$hostname;dbname=$dbname","$username","$pw");
} catch (PDOException $e){
  die("Não foi possível conectar com o banco de dados: " . $e->getMessage() . "\n");
}

?>
