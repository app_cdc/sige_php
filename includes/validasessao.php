<?php
// A sessão precisa ser iniciada em cada página diferente

if (!isset($_SESSION)){
  session_cache_expire(43200);
  session_start();
} 

//$nivel_necessario = 2;

// Verifica se não há a variável da sessão que identifica o usuário
//if (!isset($_SESSION['UsuarioID']) OR ($_SESSION['UsuarioNivel'] < $nivel_necessario)) {

/**
* Função que protege uma página
*/
function protegePagina(array $permissoes) {

  global $_SG;
  //Se não tem sessão manda para o login
  if (!isset($_SESSION['UsuarioID'])){
    //Se ele já estava logado, então a sessão expirou, manda para login e pede para logar novamente
    if(isset($_COOKIE['logado'])){
      expulsaVisitante("?erro=EXPIRADO");
    }else{ // Se não estava logado significa que ele está tentando acessar sem fazer o login
      expulsaVisitante("?erro=RESTRITO");
    }
  }else{
  	// usuario está logado, verifica de ele tem permissão para acessar a pagina
    $count_permissao = 0;
    foreach ($permissoes as $permissao) {
      $sql = "SELECT * FROM permissao_usuario WHERE usuario = ".$_SESSION['UsuarioID']." AND permissao = ".$permissao;
      $query = $_SG['conexao_bd_mysqli']->query($sql);
      $count_permissao += $query->num_rows;
    }
    
    // se não encontrar a permissão manda pra pagina de login
    if($count_permissao == 0){
		  expulsaVisitante("?erro=RESTRITO");
    }
  }
}

/**
* Função para expulsar um visitante
*/
function expulsaVisitante($atributo_get = "") {

  // Remove as variáveis da sessão (caso elas existam)
  //unset($_SESSION['UsuarioID'], $_SESSION['UsuarioNome'], $_SESSION['UsuarioUser'], $_SESSION['UsuarioMatricula']);
  //session_destroy();
  //setcookie("logado", "", 1);
  // Manda pra tela de login
  echo '<script>window.location.href = "../logout.php'.$atributo_get.'"</script>';
  exit;
}
?>

