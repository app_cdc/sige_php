<?php
// se estiver em ambiente de desenvolvimento colocar TRUE
$development = FALSE;
// Configuração do banco de dados local MySQL/MariaDB
$servidor = "localhost";
$usuario_bd = "sige";
$senha_bd = "sigepass";
$banco = "sige";

//Configuração do banco de dados local MS SQL Server
$hostname = "xxx.xxx.xxx.xxx";
$dbname = "xxxxx";
$username = "xxxxxxxx";
$pw = "xxxxxxxxxx";

// habilita msgs de erro - descomentar no ambiente de desenvolvimento
if ($development){
  ini_set('display_errors', 'On');
  error_reporting(E_ALL | E_STRICT);
}

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

//permissões
if (!defined('ADMINISTRADOR')) define('ADMINISTRADOR', 1);
if (!defined('COMISSAO')) define('COMISSAO', 2);
if (!defined('AVALIADOR')) define('AVALIADOR', 3);
if (!defined('COORDENADOR')) define('COORDENADOR', 4);

?>
