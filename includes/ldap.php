<?php
class Ldap{

    public $usuario;
    public $senha;

    public $nome;
    public $matricula;

    public $erro;

    function __construct($usuario,$senha) {
        $this->usuario = $usuario;
        $this->senha = $senha;
    }

    public function ldap_login ()
    {
        $ldaphost = "ldaps://ldap.unicamp.br";
        $ldaptree = "dc=unicamp,dc=br";

        // using ldap bind
        $ldaprdn = 'uid='.$this->usuario.',ou=people,dc=unicamp,dc=br'; // ldap rdn
        $ldappass = $this->senha; // associated password

        // connect to ldap server
        $ldapconn = ldap_connect($ldaphost);

        if(!$ldapconn){
            $this->erro = "Não foi possível conectar ao servidor LDAP.";
            return;
        }

        // binding to ldap server
        $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
        
        // verify binding
        if (!$ldapbind) {
            $this->erro = ldap_error($ldapconn);
            return;
        }

        $result = ldap_search($ldapconn,$ldaptree, "(uid=".$this->usuario.")") or die ("Error in search query: ".ldap_error($ldapconn));
        $data = ldap_get_entries($ldapconn, $result);

        // iterate over array and get data for each entry
        for ($i=0; $i<$data["count"]; $i++) {
            $this->nome=$data[$i]["cn"][0];
            $this->matricula=$data[$i]["employeenumber"][0];
        }

    }

}

?>