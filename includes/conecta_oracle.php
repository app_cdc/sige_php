<?php
include 'settings.php';
include "conecta.php";
include "logger.php";

$conn = oci_connect($usuario_bd_oracle, $senha_bd_oracle, $connection_string_bd_oracle);
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$stid = oci_parse($conn, 'SELECT * FROM VETORH54P.V_PROEC_FICHABASICA');
oci_execute($stid);

$count_inserts = 0;
$count_updates = 0;
$msg = "";
$data_atual = date("Y-m-d H:i:s");

while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {

    $usuario_matricula  = $row['MATRICULA'];
	$usuario_nome 		= $mysqli->real_escape_string($row['NOME']);
	$usuario_cod_local  = $row['COD_LOCAL'];
    $usuario_local      = $row['LOCAL'];
	$usuario_ramal 		= $row['RAMAL'];
	$usuario_email 		= $row['EMAIL'];
    $usuario_perfil     = $row['PERFIL'];

    $sql = "SELECT * FROM usuarios_dgrh WHERE matricula = ".$usuario_matricula;
    $query = $mysqli->query($sql);
    $count_usuarios = $query->num_rows;
    if($count_usuarios == 1){

    	$update_usuario = "UPDATE usuarios_dgrh SET nome = '$usuario_nome', cod_local = '$usuario_cod_local', local = '$usuario_local', ramal = '$usuario_ramal', email = '$usuario_email', perfil = '$usuario_perfil' WHERE matricula ='$usuario_matricula'";

    	if ($mysqli->query($update_usuario) === TRUE) {
    		$count_updates += 1;
    		$mysqli->commit();
    	}else{
            $msg .= "Erro update $update_usuario <br>\n";
            $mysqli->rollback();
        }

    }elseif ($count_usuarios == 0) {

    	$insert_usuario = "INSERT INTO usuarios_dgrh (nome,cod_local,local,matricula,ramal,email,perfil) VALUES ('$usuario_nome','$usuario_cod_local','$usuario_local','$usuario_matricula','$usuario_ramal','$usuario_email','$usuario_perfil')";

    	if ($mysqli->query($insert_usuario) === TRUE) {
    		$count_inserts += 1;
    		$mysqli->commit();
        }else{
            $msg .= "Erro insert $insert_usuario <br>\n";
            $mysqli->rollback();
    	}

    }

}

Logger("$data_atual - Inseridos: $count_inserts. Atualizados: $count_updates. <br> \n Erros: $msg.");

?>


