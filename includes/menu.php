<!-- Inicio Navbar -->
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#" title="SIGE">
	<img style="max-width:217px; margin-top: -6px;"
             src="../style/images/logo_sige_branco.png">
	</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
<?php // se estiver logado mostra os projetos do usuario
if ($development){
?>
        <li id="dev" style="background-color: red;"><a href="/"><b>DEV</b></a></li>
<?php } ?>
	<li id="home"><a href="/"><b>HOME</b></a></li>
        <li id="editais"><a href="/editais"><b>EDITAIS</b></a></li>
<?php // se estiver logado mostra os projetos do usuario
if (!empty($usuario_logado_nome)){
?>
        <li id="projetos"><a href="/acoes_extensao"><b>PROJETOS</b></a></li>
<?php } ?>
        <li id="cronograma"><a href="/cronograma.php"><b>CRONOGRAMA</b></a></li>
        <li id="contato"><a href="/contato.php"><b>CONTATO</b></a></li>
<?php // se estiver logado mostra os projetos do usuario
if (isset($_SESSION['UsuarioID']) && temPermissao($_SESSION['UsuarioID'],ADMINISTRADOR)){
?>
        <li id="usuarios"><a href="/usuarios"><b>USUÁRIOS</b></a></li>
<?php } ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <?php if (!empty($usuario_logado_nome)){ ?>
            <li><a href="#"><span class="glyphicon"></span><?php echo $usuario_logado_nome; ?></a></li>
            <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Sair</a></li>
          <?php }else{ ?>
            <li id="loginmenu"><a href="../login.php"><span class="glyphicon glyphicon-log-in"></span> Entrar</a></li>
          <?php } ?>
      </ul>
    </div>
  </div>
</nav>
<!-- Fim Navbar --> 

<script>
//Remove item do navbar que está ativo. Necessita ativar em cada página html.
$('.navbar li.active').removeClass('active');
</script>

