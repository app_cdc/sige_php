<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Relatórios Pendentes"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
?>
<script type="text/javascript">
function apagar_usuario_pendente(pendente_id)
{
  $.ajax({
    type: 'post',
    url: 'apagar.php',
    data: {
      id:pendente_id
    },
    success: function (response) {
      if(response){
        location.replace("/pendentes");
      }else{
        document.getElementById("msg").innerHTML="Erro ao apagar";
      }
    }
  });
}
</script>

<div class="container">
	<h1><b>Usuários com Relatórios Pendentes</b></h1>
	<hr>
  <button onclick="window.location.href='/usuarios'" class="btn btn-primary btn-lg" >
      < Voltar 
  </button>&nbsp;
  <button onclick="window.location.href='novo.php'" class="btn btn-primary btn-lg" >
      <span class="glyphicon glyphicon-plus"></span> Inserir novo 
  </button>
  <br>
  <hr>

  <ul class="nav nav-tabs">
      <li class="active"><a href="#Coordenadores" data-toggle="tab">Coordenadores <i class="fa"></i></a></li>
  </ul>
  <div class="tab-content">
      <div id="Coordenadores" class="tab-pane fade in active" >
		  	<br>
			  <table class="table table-bordered table-striped">
			  	<thead>
			    	<tr>
			        <th class="col-md-1">Matricula</th>
			        <th class="col-md-8">Nome</th>
			        <th class="col-md-3">Ano</th>
			        <th class="col-md-3">Operação</th>
			      </tr>
			    </thead>
			    <tbody>
					<?php
						$sql = "SELECT ur.matricula, 
									   ur.ano,
									   ur.id
								FROM usuarios_relatorio_final_pendente ur";
						if ($result = $mysqli->query($sql)) {
							while($dados = $result->fetch_array()){
                $query_dgrh = $pdo->prepare("SELECT nome FROM siarh_sige WHERE matricula = :matricula");
                $query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
                $query_dgrh->execute();
                $dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
					?>    
			    	<tr>
				        <td><?php echo $dados['matricula'];?></td>
				        <td><?php echo $dgrh_dados['nome'];?></td>
				        <td><?php echo $dados['ano'];?></td>
				        <td>
				        	<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#myModal" data-pend-id="<?php echo $dados['id']; ?>">
				        		<span class="glyphicon glyphicon-trash"></span>
	        				</button>
	        			</td>
			    	</tr>
					<?php
							}
						}
					?>    
          <?php
            $sql_2 = "SELECT e.ano_base, u.nome, u.matricula, a.* 
                      FROM editais e,
                      usuarios u,
                        (SELECT ae.id as acao_id, ae.edital, ae.coordenador, rf.*
                        FROM acoes_extensao ae
                        LEFT JOIN (rel_final rf) ON (ae.id = rf.id_projeto)
                        WHERE (rf.estado <> 2 OR rf.estado IS NULL) 
                        AND ae.estado_acao = 7) a
                      WHERE a.edital = e.id
                      AND a.coordenador = u.id
                      AND e.data_fim_projeto < NOW()";
            if ($result_2 = $mysqli->query($sql_2)) {
              while($dados_2 = $result_2->fetch_array()){
          ?>    
            <tr>
                <td><?php echo $dados_2['matricula'];?></td>
                <td><?php echo $dados_2['nome'];?></td>
                <td><?php echo $dados_2['ano_base'];?></td>
                <td>
                </td>
            </tr>
          <?php
              }
            }
          ?>    
			    </tbody>
			  </table>
		  </div><!--div tab-pane-->


  </div><!--div tab-content-->



</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>O usuário entregou o relatório que faltava?</p>
                <p class="text-warning"><small>Essa ação irá liberar o usuário para inserir novos projetos</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnApagar">Apagar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<script type="text/javascript">

    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var pendente_id = button.data('pendId'); // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('#btnApagar').click(function() {
        apagar_usuario_pendente(pendente_id);
      });
    });
</script>
