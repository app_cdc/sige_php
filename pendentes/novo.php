<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Usuários"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
// END TEMPLATE
$permissoes = array(ADMINISTRADOR);
protegePagina($permissoes);
//
$erro = FALSE;
//
if (!empty($_POST['usuario_inserir'])) {

    $msg = "";
    $msg_erro = "";

    $usuario_nome = $_POST['usuario_nome'];
    $usuario_matricula = $_POST['usuario_matricula'];
    $usuario_ano = $_POST['usuario_ano'];

    $sql_usuario_existente = "SELECT * FROM usuarios_relatorio_final_pendente u WHERE u.matricula = $usuario_matricula";
    if ($result_usuario_existente = $mysqli->query($sql_usuario_existente)) {
        if($result_usuario_existente->num_rows > 0){
            $erro = TRUE;
            $msg = "Usuário já cadastrado no sistema com relatório pendente";
        }else{
            $sql_usuario = "INSERT INTO usuarios_relatorio_final_pendente (matricula, ano) VALUES ($usuario_matricula, $usuario_ano)";
            if ($mysqli->query($sql_usuario) === FALSE) {
                $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql_usuario . "<br>";
            }

            if(empty($msg_erro)){
                $mysqli->commit();
                $msg = "Inserido com sucesso!";
            }else{
                $mysqli->rollback();
                $erro = TRUE;
                $msg = "Erro ao inserir.";
            }
        }
    }



?>

<!-- Modal HTML -->
<div id="myModalErro" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $msg ?></p>
                <p class="text-warning"><small><?php echo $msg_erro ?></small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#myModalErro").modal('show');
});
</script>
<!-- Modal HTML -->

<?php
}

?>
<div class="container">
    <h1><b>Novo usuário com relatório pendente: </b></h1>
    <hr>
    <form class="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
      
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_nome">Nome completo:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_nome" id="usuario_nome" size="100" required />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_matricula">Matricula:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_matricula" id="usuario_matricula" size="8" required />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="usuario_ano">Ano:</label>
            <div class="col-sm-10">
                <input class="form-control" name="usuario_ano" id="usuario_ano" size="8" required />
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="location.href='/pendentes';">< Voltar</button>
        <button type="submit" class="btn btn-default" name="usuario_inserir" value="Inserir usuário">Inserir</button>

    </form>
</div>