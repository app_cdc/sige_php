<?php
// TEMPLATE
require_once('lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Login"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "layout.php";
    exit;
}
// END TEMPLATE
?>
<div class="container">
    <div style="padding: 0px;" class="col-md-4 col-md-offset-4">
<?php

include 'includes/ldap.php';

$erro = FALSE;
$msg = "";
// Verifica se houve POST e se o usuário ou a senha é(são) vazio(s)
if (!empty($_POST)) {

    $usuario_novo = FALSE;

    if(empty($_POST['usuario']) OR empty($_POST['senha'])) {
        $msg = "Usuário e senha devem ser preenchidos!";
        $erro = TRUE;
    }

    if(!$erro){

        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];

        $dados_ldap = new Ldap($usuario,$senha);
        $dados_ldap->ldap_login();

        if(!empty($dados_ldap->erro)){

            $msg = $dados_ldap->erro;
            $erro = TRUE;

            if($dados_ldap->erro == "Invalid credentials"){
                $msg = "Usuário ou senha inválidos!";

            }
            if($dados_ldap->erro == "No such object"){
                $msg = "Cadastro não encontrado no SiSe!";
            }

        }
        if (!$erro){

            $usuarios_ldap_matricula = $dados_ldap->matricula;
            $usuarios_ldap_nome = $mysqli->real_escape_string($dados_ldap->nome);

            $sql_pendencia = "SELECT * FROM usuarios_relatorio_final_pendente WHERE matricula = ".$usuarios_ldap_matricula;
            $query_pendencia = $mysqli->query($sql_pendencia);
            $count_usuarios_pendencia = $query_pendencia->num_rows;

            if($query_pendencia->num_rows > 0){ // está devendo relatorio final
                $msg = "Há pendências a serem resolvidas, para isso entre em contato com a organização do PEC pelo telefone (19) 3521-7641.";
                $erro = TRUE;
            }

            if (!$erro){
                $sql = "SELECT * FROM usuarios WHERE matricula = ".$usuarios_ldap_matricula;
                $query = $mysqli->query($sql);
                $count_usuarios = $query->num_rows;

                if(!$query->num_rows > 0){ // não encontrado na tabela usuarios, buscar na siarh_sige

                  // -- mssql inicio
                  $query_dgrh = $pdo->prepare('SELECT * FROM siarh_sige WHERE matricula = :matricula');
                  $query_dgrh->bindValue(':matricula', $usuarios_ldap_matricula, PDO::PARAM_INT);
                  $query_dgrh->execute();

                  // mssql
                  if(!$dados_dgrh = $query_dgrh->fetch(PDO::FETCH_ASSOC)){ // não encontrado nos dados do DGRH
                    $msg = "Cadastro não encontrado na DGRH";
                    $erro = TRUE;
                  }else{ //se encontrou insere na tabela usuarios
                      // mssql
                      do {

                        if (strpos($dados_dgrh['PERFIL'], 'DOCENTE ') !== FALSE || strpos($dados_dgrh['PERFIL'], 'PESQUISADOR') !== FALSE || strpos($dados_dgrh['PERFIL'], 'PROFESSOR COLABORADOR') !== FALSE) { // só insere se for Docente, Pesquisador e PROFESSOR COLABORADOR
                          // -- mssql fim
                          // -- mysql inicio
                          $sql_insert_usuario = "INSERT INTO usuarios (nome,matricula) VALUES ('$usuarios_ldap_nome','$usuarios_ldap_matricula')";
                          if ($mysqli->query($sql_insert_usuario) === FALSE) {
                            $msg = "Erro no login. Entre em contado com o administrador do sistema. "."Error: " . $mysqli->error . "<br>" . $sql_insert_usuario;
                            $erro = TRUE;
                            $mysqli->rollback();
                          }else{
                            //$mysqli->insert_id
                            $permissao_id = COORDENADOR;
                            $sql_insert_permissao = "INSERT INTO permissao_usuario (usuario,permissao) VALUES ($mysqli->insert_id, $permissao_id)";
                            if ($mysqli->query($sql_insert_permissao) === FALSE) {
                              $msg = "Erro no login. Entre em contado com o administrador do sistema. "."Error: " . $mysqli->error . "<br>" . $sql_insert_permissao;
                              $erro = TRUE;
                              $mysqli->rollback();
                            }else{
                              $usuario_novo = TRUE;
                              $mysqli->commit();
                            }
                          }
                          // -- mysql fim
                        }else{
                          $msg = "Somente Docentes e Pesquisadores podem entrar no sistema para enviar propostas.";
                          $erro = TRUE;
                        }
                      } while ($dados_dgrh = $query_dgrh->fetch(PDO::FETCH_ASSOC));
                  }
                }
                // se a pessoa já está cadastrada na tabela ususarios ou acabou de ser cadastrada faz o login
                if(!$erro){

                    // se acabou de inserir faz a busca novamente
                    if($usuario_novo){
                        $sql = "SELECT * FROM usuarios WHERE matricula = ".$usuarios_ldap_matricula;
                        $query = $mysqli->query($sql);
                    }

                    $usuario_id = "";
                    $usuario_cpf = "";
                    $usuario_email = "";

                    if ($result = $mysqli->query($sql)) {
                        while ($dados = $query->fetch_array()) {
                            $usuario_id = $dados['id'];
                            $usuario_cpf = $dados['cpf'];
                            $usuario_email = ($dados['email_dgrh']) ? $dados['email_dgrh'] : $dados['email_alternativo'];
                        }
                    }

                    $data_hora_login = date("Y-m-d H:i:s");
                    $sql_update_ultimo_login = "UPDATE usuarios SET ultimo_login = '$data_hora_login' WHERE id = $usuario_id";
                    if ($mysqli->query($sql_update_ultimo_login) === TRUE) {
                        $mysqli->commit();
                    }

                    $_SESSION['UsuarioID'] = $usuario_id;
                    $_SESSION['UsuarioCPF'] = $usuario_cpf;
                    $_SESSION['UsuarioUser'] = $dados_ldap->usuario;
                    $_SESSION['UsuarioNome'] = $usuarios_ldap_nome;
                    $_SESSION['UsuarioMatricula'] = $usuarios_ldap_matricula;
                    $_SESSION['UsuarioEmail'] = $usuario_email;

                    echo '<script>window.location.href = "index.php"</script>'; exit;
                }
            }
        }

    }


}

if (isset($_COOKIE["erro_login"])) {
    if ($_COOKIE["erro_login"] == "RESTRITO"){
        $erro = TRUE;
        $msg = "Acesso restrito!";
        setcookie("erro_login", "", 1);
    }elseif ($_COOKIE["erro_login"] == "EXPIRADO") {
        $erro = TRUE;
        $msg = "Sua sessão expirou. Faça o login novamente.";
        setcookie("erro_login", "", 1);
    }
}

if($erro){
?>
        <!-- Modal HTML -->
        <div id="myModalErro" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Atenção!</h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo $msg?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
            $("#myModalErro").modal('show');
        });
        </script>
        <!-- Modal HTML -->

<?php
}

?>
        <!-- Formulário de Login -->
        <form class="form-horizontal" role="form" id="login"  name="login" method="post">

            <div class="panel panel-success" style="text-align: center;">

                <div class="panel-heading">
                    <img style="max-width:250px;" src="../style/images/logo_sige.png">
                </div>

                <div class="panel-body" style="text-align: center;padding: 25px;">

                    <div style="margin-bottom: 25px" class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="usuario" type="text" class="form-control" name="usuario" placeholder="Usuário" required autofocus/>
                    </div>

                    <div style="margin-bottom: 25px" class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="senha" type="password" class="form-control" name="senha" placeholder="Senha" required/>
                    </div>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-log-in"></span> Entrar</button></li>
                    </div>
                </div>
            </div>

        </form>

    </div>

</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#login')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                usuario: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha o usuário'
                        }
                    }
                },
                senha: {
                    validators: {
                        notEmpty: {
                            message: 'Preencha a senha'
                        }
                    }
                }
            }
        });

        $('#usuario').tooltip({'trigger':'focus',
                               'title': 'Usuário sem o "@unicamp.br"',
                                'container': 'body',
                                'placement': 'right'});
});
</script>
<script type="text/javascript">
    $(document).ready( function(){
       $("#loginmenu").addClass("active");
    });
</script>