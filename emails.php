#!/usr/bin/php
<?php
include 'vendor/autoload.php';
include "includes/conecta.php";

$html = '';
$html .= '<br>';
$html .= "<b>Não enviados para análise: </b>";

$sql_nao_enviado_pra_analise = "select e.titulo as edital_titulo, ae.* from acoes_extensao ae, editais e where e.id = 6 and e.id = ae.edital and ae.estado_acao = 2 and ae.id not in (select ed.id_acao_extensao from acoes_edicao ed) order by e.titulo, ae.titulo";
if ($query = $mysqli->query($sql_nao_enviado_pra_analise)) {
	$html .= $query->num_rows;
	$html .= '<br><br>';
	$html .= "<table border='1'><thead><th>Edital</th><th>Título</th><thead><tbody>";
	while ($dados = $query->fetch_array()) {
		$html .= '<tr><td>' . $dados['edital_titulo'] . '</td><td>' . $dados['titulo'] . '</td></tr>';
	}
	$html .= "</tbody></table>";
} else {
	$html .= '0';
	$html .= '<br>';
}

$html .= '<br>';
$html .= "<b>Não enviados para avaliação: </b>";
$sql_nao_enviado_pra_avaliacao = "select e.titulo as edital_titulo, u.nome as editor, ae.titulo, ed.* from acoes_edicao ed, usuarios u, acoes_extensao ae, editais e where e.id = 6 and e.id = ae.edital and ae.id = ed.id_acao_extensao and u.id = ed.id_editor and ed.id_acao_extensao not in (select av.id_acao_extensao from acoes_avaliacao av) order by e.titulo, u.nome, ae.titulo";
if ($query = $mysqli->query($sql_nao_enviado_pra_avaliacao)) {
	$html .= $query->num_rows;
	$html .= '<br><br>';
	$html .= "<table border='1'><thead><th>Edital</th><th>Editor</th><th>Título</th><thead><tbody>";
	while ($dados = $query->fetch_array()) {
		$html .= '<tr><td>' . $dados['edital_titulo'] . '</td><td>' . $dados['editor'] . '</td><td>' . $dados['titulo'] . '</td></tr>';
	}
	$html .= "</tbody></table>";
} else {
	$html .= '0';
	$html .= '<br>';
}

$html .= '<br>';
$html .= "<b>Não avaliados: </b>";
$sql_nao_avaliado = "select e.titulo as edital_titulo, u.nome as avaliador, ae.titulo, av.* from acoes_avaliacao av, usuarios u, acoes_extensao ae, editais e where e.id = 6 and e.id = ae.edital and ae.id = av.id_acao_extensao and u.id = av.id_avaliador and av.data_avaliacao is null order by e.titulo, u.nome, ae.titulo";
if ($query = $mysqli->query($sql_nao_avaliado)) {
	$html .= $query->num_rows;
	$html .= '<br><br>';
	$html .= "<table border='1'><thead><th>Edital</th><th>Avaliador</th><th>Título</th><thead><tbody>";
	while ($dados = $query->fetch_array()) {
		$html .= '<tr><td>' . $dados['edital_titulo'] . '</td><td>' . $dados['avaliador'] . '</td><td>' . $dados['titulo'] . '</td></tr>';
	}
	$html .= "</tbody></table>";
	
} else {
	$html .= '0';
	$html .= '<br>';
}

//echo $html;

// Create the Transport
$transport = (new Swift_SmtpTransport('192.168.0.122', 25))
	->setUsername('sistemas')
	->setPassword('*Pro@SQ')
;

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = (new Swift_Message('Atualização sobre os projetos SIGE'))
	->setFrom(['nao-responda@sistemas.proec.unicamp.br' => 'SIGE'])
	->setBody($html, 'text/html')
	;

//$message->addTo('daniloe@g.unicamp.br','Danilo Elias');

$sql = "SELECT u.id,
							u.matricula, 
							u.nome
				FROM usuarios u, 
						permissao_usuario pu, 
						permissoes p
				WHERE pu.usuario = u.id 
					and pu.permissao = p.id 
					and p.nome = 'Administrador'
				ORDER BY u.nome";

if ($query = $mysqli->query($sql)) {
	while ($dados = $query->fetch_array()) {
		$query_dgrh = $pdo->prepare("SELECT LOCAL, EMAIL FROM siarh_sige WHERE matricula = :matricula");
		$query_dgrh->bindValue(':matricula', $dados['matricula'], PDO::PARAM_INT);
		$query_dgrh->execute();
		$dgrh_dados = $query_dgrh->fetch(PDO::FETCH_ASSOC);
		
		$certificado_nome = $dados['nome'];
		$certificado_email = $dgrh_dados['EMAIL'];
		
		$message->addTo($dgrh_dados['EMAIL'], $dados['nome']);
	}
}


// Send the message
$result = $mailer->send($message);	
