<?php
// TEMPLATE
require_once('lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Cronograma"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "layout.php";
    exit;
}
// END TEMPLATE
?>

<div class="container">

  <h1><b>Cronograma</b></h1>
  <hr>

  <br>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Data</th>
        <th>Evento</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>20/05/2019</td>
        <td>Divulgação do 1º Edital PROEC - PEX 2019.</td>
      </tr>
      <tr>
        <td>07/07/2019</td>
        <td>Limite para inscrição da proposta via <i>sige.unicamp.br</i>.</td>
      </tr>
      <tr>
        <td>16/08/2019</td>
        <td>Encerramento do prazo para análise dos pareceristas.</td>
      </tr>
      <tr>
        <td>30/08/2019</td>
        <td>Divulgação do resultado em <a href="http://www.proec.unicamp.br/" target="_blank">www.proec.unicamp.br</a> e <a href="http://www.dproj.preac.unicamp.br/" target="_blank">www.dproj.preac.unicamp.br</a></td>
      </tr>
      <tr>
        <td>16/09/2019</td>
        <td>Assinatura na Funcamp do termo de Outorga pelo coordenador da proposta aprovada e INÍCIO efetivo da execução do projeto.</td>
      </tr>
      <tr>
        <td>01/10/2020</td>
        <td>Término do Projeto.</td>
      </tr>
      <tr>
        <td>01/10/2020</td>
        <td>Prestação de Contas (item 7).</td>
      </tr>
      <tr>
        <td>01/10/2020</td>
        <td>Relatório Técnico Final (item 7).</td>
      </tr>
    </tbody>
  </table>
</div>

<script type="text/javascript">
    $(document).ready( function(){
       $("#cronograma").addClass("active");
    });
</script>
