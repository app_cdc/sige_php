<?php
// TEMPLATE
require_once('../lib/PageTemplate.php');
# trick to execute 1st time, but not 2nd so you don't have an inf loop
if (!isset($TPL)) {
    $TPL = new PageTemplate();
    $TPL->PageTitle = "Alterar orçamento"; // Título da Página
    //$TPL->ContentHead = ""; // Header da Página
    $TPL->ContentBody = __FILE__;
    include "../layout.php";
    exit;
}
?>
<div class="container">
<div id="msg"></div>
<?php
// END TEMPLATE
$permissoes = array(COORDENADOR);
protegePagina($permissoes);

if (!function_exists('convertCoin')) {
  function convertCoin($xCoin = "EN", $xDecimal = 2, $xValue) {
     $xValue       = preg_replace( '/[^0-9]/', '', $xValue); // Deixa apenas números
     $xNewValue    = substr($xValue, 0, -$xDecimal); // Separando número para adição do ponto separador de decimais
     $xNewValue    = ($xDecimal > 0) ? $xNewValue.".".substr($xValue, strlen($xNewValue), strlen($xValue)) : $xValue;
     return $xCoin == "EN" ? number_format($xNewValue, $xDecimal, '.', '') : ($xCoin == "BR" ? number_format($xNewValue, $xDecimal, ',', '.') : NULL);
  }
}
//
if( !empty($_POST) ){

    $orcamento_acao_id = $_POST['acao_id'];
    $orcamento_item_orcamento = $_POST['item_orcamento'];
    $orcamento_valor_solicitado = convertCoin("EN",2,$_POST['valor_solicitado']);

    $sql = "INSERT INTO orcamentos (acao_extensao,item_orcamento,valor_solicitado) VALUES ($orcamento_acao_id,$orcamento_item_orcamento,'$orcamento_valor_solicitado')";

    $msg_erro = '';

    if ($mysqli->query($sql) === TRUE) {

      $mysqli->commit();
      echo '<script>window.location.href = "editar.php?id='.$orcamento_acao_id.'"</script>'; exit;

    } else {
        $msg_erro .= "Error: " . $mysqli->error . "<br>" . $sql;
    }

    if(!empty($msg_erro)){
        $mysqli->rollback();
        echo 'Erro ao criar o objetivo especifico. '.$msg_erro;
    }
    $mysqli->close();

}
else
{

 
  if (isset($_GET['id'])) {
    
    $acao_id = $_GET['id'];

    $sql = "SELECT * FROM acoes_extensao WHERE id=".$acao_id;
    $query = $mysqli->query($sql);

    if ($result = $mysqli->query($sql)) {
      while ($dados = $query->fetch_array()) {

        $edital_id = $dados["edital"];

       echo "<h3>Orçamento da Ação de Extensão: ".$dados['titulo']."</h3>";
      }
    }

    $sql_edital = "SELECT * FROM editais WHERE id=".$edital_id;
    $query_edital = $mysqli->query($sql_edital);

    if ($result_edital = $mysqli->query($sql_edital)) {
      while ($dados_edital = $query_edital->fetch_array()) {
        $edital_valor = $dados_edital["valor"];
      }
    }

?>
<script type="text/javascript">
function fetch_select(val)
{
   $.ajax({
     type: 'post',
     url: '../includes/fetch_data_tipo_orcamento.php',
     data: {
       tipo_orcamento_id:val
     },
     success: function (response) {
       document.getElementById("item_orcamento").innerHTML=response; 
     }
   });
}
</script>

<script type="text/javascript">
function apagar_item_orcamento(orcamento_id,acao_id)
{
  $.ajax({
    type: 'post',
    url: 'apagar.php',
    data: {
      id:orcamento_id
    },
    success: function (response) {
      if(response){
        //document.getElementById("msg").innerHTML="Apagado com sucesso";
        location.replace("editar.php?id="+acao_id);
      }else{
        document.getElementById("msg").innerHTML="Erro ao apagar";
      }
    }
  });
}
</script>


<hr>
<form class="form-horizontal" role="form" name="cadastro" id="cadastro" method="post" accept-charset="utf-8">
  
  <input type="hidden" name="acao_id" value="<?php echo $acao_id ?>">

  <div class="form-group">
    <label class="control-label col-sm-3" for="tipo_orcamento">Tipo do Item do Orçamento:</label>
    <div class="col-sm-3">
      <select class="form-control" name="tipo_orcamento" id="tipo_orcamento" onchange="fetch_select(this.value);" required>
        <option>Selecione o tipo</option>
        <?php
            $sql = "SELECT * FROM tipo_orcamento";
            $query = $mysqli->query($sql);

            if ($result = $mysqli->query($sql)) {
              while ($dados = $query->fetch_array()) {
                $tipo_orcamento_id = $dados['id'];
                $tipo_orcamento_nome = $dados['nome'];
               echo "<option value='$tipo_orcamento_id'>$tipo_orcamento_nome</option>";
              }
            }
        ?>
      </select>
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <label class="control-label col-sm-3" for="item_orcamento">Item do Orçamento:</label>
    <div class="col-sm-3">
      <select class="form-control" name="item_orcamento" id="item_orcamento" required>
      </select>
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <label class="col-sm-3 control-label" for="valor_solicitado">Valor solicitado:</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" name="valor_solicitado" id="valor_solicitado" required>
      <script type="text/javascript">$("#valor_solicitado").maskMoney({prefix:'R$ ', showSymbol:true, thousands:'.', decimal:',', affixesStay: false});</script>
    </div>
  </div><!--div form-group-->

  <div class="form-group">
    <div class="col-sm-3">                 
    </div>
    <div class="col-sm-3">
      <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span> Inserir Item do Orçamento</button></li>
    </div>
  </div><!--div form-group-->

</form>
<hr>

<table class="table table-striped">

    <thead>
      <tr class="tabela_cabecalho">
        <th>Nome do Item</th>
        <th>Valor</th>
        <th width=65>Operação</th>
      </tr>
    </thead>

    <tbody>     

<?php
    $sub_total_orcamento = 0;
    $total_orcamento = 0;

    $sql_tipo_orcamento = "SELECT * FROM tipo_orcamento ORDER by id";
    $query_tipo_orcamento = $mysqli->query($sql_tipo_orcamento);

    if ($result_tipo_orcamento = $mysqli->query($sql_tipo_orcamento)) {
      while ($dados_tipo_orcamento = $query_tipo_orcamento->fetch_array()) {

        $tipo_orcamento_id = $dados_tipo_orcamento['id'];
        $tipo_orcamento_nome = $dados_tipo_orcamento['nome'];
        echo "<tr><th COLSPAN=3 class='tabela_subtitulo'>$tipo_orcamento_nome</th></tr>";

        $sql_item_orcamento = "SELECT o.id, io.nome, o.valor_solicitado FROM orcamentos o, item_orcamento io WHERE o.item_orcamento = io.id AND o.acao_extensao = $acao_id AND io.tipo = $tipo_orcamento_id";

        $query_item_orcamento = $mysqli->query($sql_item_orcamento);

        if ($result_item_orcamento = $mysqli->query($sql_item_orcamento)) {
          while ($dados_item_orcamento = $query_item_orcamento->fetch_array()) {

            $sub_total_orcamento += floatval($dados_item_orcamento['valor_solicitado']);
            $item_orcamento_nome = $dados_item_orcamento['nome'];
            $item_orcamento_valor_solicitado = convertCoin("BR",2,$dados_item_orcamento['valor_solicitado']);

            echo "<tr><td>$item_orcamento_nome</td><td>$item_orcamento_valor_solicitado</td><td style='text-align: center;'><button type='button' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#myModal' data-orc-item='".$dados_item_orcamento['id']."' data-orc-acao='".$acao_id."'><span class='glyphicon glyphicon-trash'></span></button></td></tr>";

          }
          echo "<tr><th>Sub total: </th><th COLSPAN=2>".number_format($sub_total_orcamento,2,",",".")."</th></tr>";
        }
        $total_orcamento += $sub_total_orcamento;
        $sub_total_orcamento = 0;
      }
    echo '<tr><th class="tabela_total">Total:</th><th class="tabela_total" COLSPAN=2>'.number_format($total_orcamento,2,",","."). '</th></tr>';

    $orcamento_valor_disponivel = $edital_valor - $total_orcamento;

    $cor_fundo = ($orcamento_valor_disponivel < 0)? "tabela_valor_negativo":"tabela_valor_positivo";

    echo '<tr><th class="'.$cor_fundo.'">Disponível:</th><th class="'.$cor_fundo.'" COLSPAN=2>'.number_format($orcamento_valor_disponivel,2,",","."). '</th></tr>';
    }

  }

}
?>
</tbody>
</table>

<div>
    <ul class="pager">
        <li ><button type="button" class="btn btn-default btnAnterior" onclick="location.href='../acoes_extensao/show.php?id=<?php echo $acao_id?>';">< Voltar ao Principal</button></li>
    </ul>
  </div>

</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Atenção!</h4>
            </div>
            <div class="modal-body">
                <p>Deseja apagar mesmo?</p>
                <p class="text-warning"><small>Se você apagar, não terá volta</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnApagar">Apagar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->

<script type="text/javascript">
$(document).ready(function() {
    $('#cadastro')
        .bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'pt_BR',
            fields: {
                item_orcamento: {
                    validators: {
                        notEmpty: {
                            message: 'Selecione o tipo e depois o item do orçamento'
                        }
                    }
                }
            }
        });
    
    $('#valor_solicitado').change(function() {
      $('#cadastro').data('bootstrapValidator').updateStatus('valor_solicitado', 'NOT_VALIDATED').validateField('valor_solicitado');
    });
    //
    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var orcamento_item_id = button.data('orcItem'); // Extract info from data-* attributes
      var orcamento_acao_id = button.data('orcAcao'); // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('#btnApagar').click(function() {
        apagar_item_orcamento(orcamento_item_id,orcamento_acao_id);
      });
    });

});
</script>
