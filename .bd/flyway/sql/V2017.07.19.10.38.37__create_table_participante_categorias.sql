CREATE TABLE `participante_categorias` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificação da categoria',
	`descricao` VARCHAR(100) NOT NULL COMMENT 'Descrição da categoria',
	PRIMARY KEY (`id`)
)
COMMENT='Categorias de participantes em projetos (Docentes, Discentes com Auxílio Financeiro a Alunos, Discentes sem o Auxílio Financeiro a Alunos, Comunidade)'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
participante_categorias

id INT
descricao VARCHAR
*/