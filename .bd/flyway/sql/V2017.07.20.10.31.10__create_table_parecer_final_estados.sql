CREATE TABLE `parecer_final_estados` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`descricao` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`id`)
)
COMMENT='Estados do parecer final sobre o o relatorio tecnico final'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
parecer_final_estados

id INT
descricao VARCHAR
*/