INSERT INTO `rel_final_perguntas` (`id`, `descricao`) VALUES
  (1, 'Contribuição do projeto para a formação acadêmica, profissional e cidadã dos envolvidos e para a universidade.'),
  (2, 'O projeto contribuiu/contribui para:'),
  (3, 'De que maneira a comunidade externa se envolveu no projeto:'),
  (4, 'Dificuldades encontradas na realização do projeto.');
