--
-- Estrutura para tabela `acoes_desclassificacao`
--

CREATE TABLE IF NOT EXISTS `acoes_desclassificacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo_id` int(11) NOT NULL,
  `acao_extensao_id` int(11) NOT NULL,
  `administrador_id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acoes_desclassificacao_motivos_desclassificacao` (`motivo_id`),
  KEY `fk_acoes_desclassificacao_acoes_extensao` (`acao_extensao_id`),
  KEY `fk_acoes_desclassificacao_usuarios` (`administrador_id`),
  CONSTRAINT `fk_acoes_desclassificacao_acoes_extensao` FOREIGN KEY (`acao_extensao_id`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_acoes_desclassificacao_motivos_desclassificacao` FOREIGN KEY (`motivo_id`) REFERENCES `motivos_desclassificacao` (`id`),
  CONSTRAINT `fk_acoes_desclassificacao_usuarios` FOREIGN KEY (`administrador_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com o historico de desclassificações dos projetos';
