CREATE TABLE IF NOT EXISTS `rel_final` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificação do relatório',
	`id_projeto` INT NOT NULL COMMENT 'Projeto ao qual o relatório se refere',
	`dt_inicio` DATETIME NOT NULL COMMENT 'Data de inicio efetivo do projeto',
	`dt_fim` DATETIME NOT NULL COMMENT 'Data do fim efetivo do projeto',
	`local` INT NOT NULL COMMENT 'Identificação da cidade onde o projeto foi realizado',
	`conclusao` TEXT NOT NULL COMMENT 'Conclusões finais sobre o projeto e sua realização',
	`total_despesa_realizada` DECIMAL(10,2) NOT NULL COMMENT 'Despezas realizadas pelo projeto',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_rel_final_acoes_extensao` FOREIGN KEY (`id_projeto`) REFERENCES `acoes_extensao` (`id`)
)
COMMENT='Relatório Técnico Final do PEC 2016'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*

rel_final

id INT
id_projeto INT
dt_inicio DATETIME
dt_fim DATETIME
local(cod_cidade) INT
conclusao TEXT
total_despesa_realizada DECIMAL(10,2)

*/

