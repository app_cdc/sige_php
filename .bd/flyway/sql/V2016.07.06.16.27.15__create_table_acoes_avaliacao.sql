--
-- Estrutura para tabela `acoes_avaliacao`
--

CREATE TABLE IF NOT EXISTS `acoes_avaliacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_acao_extensao` int(11) NOT NULL COMMENT 'Chave estrangeira da ação de extensão',
  `id_editor` int(11) NOT NULL COMMENT 'Chave estrangeira do usuário editor',
  `id_avaliador` int(11) NOT NULL COMMENT 'Chave estrangeira do usuário avaliador',
  `data_envio` datetime NOT NULL COMMENT 'Quando o Editor enviou para o Avaliador',
  `data_avaliacao` datetime DEFAULT NULL COMMENT 'Quando o Avaliador deu todas as notas',
  `observacao` text COMMENT 'Campo observação do avaliador sobre o projeto em avaliação',
  `nota_administrador` int(11) DEFAULT NULL COMMENT 'Replica o resultado, onde ele pode alterar esse valor, esse campo servirá como ordem classificatória dos projetos inscritos',
  PRIMARY KEY (`id`),
  KEY `fk_avaliacao_acao` (`id_acao_extensao`),
  KEY `fk_avaliacao_editor` (`id_editor`),
  KEY `fk_avaliacao_avaliador` (`id_avaliador`),
  CONSTRAINT `fk_avaliacao_acao` FOREIGN KEY (`id_acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_avaliacao_avaliador` FOREIGN KEY (`id_avaliador`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `fk_avaliacao_editor` FOREIGN KEY (`id_editor`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com dados do envio do Editor para o Avaliador';
