CREATE TABLE `rel_final_perguntas` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador da pergunta',
	`descricao` VARCHAR(250) NOT NULL COMMENT 'Descrição da pergunta',
	PRIMARY KEY (`id`)
)
COMMENT='Perguntas do relatorio tecnico final'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


/*
rel_final_perguntas

id INT
descricao VARCHAR
*/