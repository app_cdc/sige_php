-- --------------------------------------------------------
-- Servidor:                     143.106.114.10
-- Versão do servidor:           5.5.47-0ubuntu0.14.04.1 - (Ubuntu)
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para sige
--CREATE DATABASE IF NOT EXISTS `sige` /*!40100 DEFAULT CHARACTER SET utf8 */;
--USE `sige`;


-- Copiando estrutura para tabela sige.acao_conhecimento
CREATE TABLE IF NOT EXISTS `acao_conhecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `area_conhecimento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `area_conhecimento` (`area_conhecimento`),
  CONSTRAINT `fk_acao_extensao_conhecimento` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_area_conhecimento` FOREIGN KEY (`area_conhecimento`) REFERENCES `areas_conhecimento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.acao_tematica
CREATE TABLE IF NOT EXISTS `acao_tematica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `area_tematica` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `area_tematica` (`area_tematica`),
  CONSTRAINT `fk_acao_extensao_tematica` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_area_tematica` FOREIGN KEY (`area_tematica`) REFERENCES `areas_tematicas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.acoes_extensao
CREATE TABLE IF NOT EXISTS `acoes_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `edital` int(11) NOT NULL,
  `estado_acao` int(11) NOT NULL COMMENT 'Trabalhar via código, os diversos estados da ação.',
  `coordenador` int(11) NOT NULL,
  `criacao` datetime NOT NULL,
  `alteracao` datetime NOT NULL,
  `tipo_extensao` int(11) NOT NULL COMMENT 'Trabalhar futuramente com demais tipos de extensão: Programa, Evento, Prestação de Serviço, etc.',
  `indicador1` varchar(500) NOT NULL COMMENT 'O projeto é indissociável de ensino e pesquisa?',
  `indicador2` varchar(500) NOT NULL COMMENT 'O Projeto é efetivamente de extensão porque ?',
  `indicador3` varchar(500) NOT NULL COMMENT 'Contem aspectos relevante na formação dos alunos.',
  `indicador4` varchar(500) NOT NULL COMMENT 'Prevê aspectos relevantes de uma formação cidadã dos alunos.',
  `indicador5` varchar(500) NOT NULL COMMENT 'Há participação da comunidade externa à Unicamp?',
  `indicador6` varchar(500) NOT NULL COMMENT 'Um projeto de pesquisa foi aplicado em um subconjunto da comunidade?',
  `indicador7` varchar(500) NOT NULL COMMENT 'Trará conhecimento para a comunidade participante do projeto?',
  `indicador8` varchar(500) NOT NULL COMMENT 'Haverá desenvolvimento de habilidades a equipe participante do projeto?',
  `indicador9` varchar(500) NOT NULL COMMENT 'Possibilitará a continuidade de crescimento e desenvolvimento dos participantes do projeto mesmo após o término do projeto?',
  `indicador10` varchar(500) NOT NULL COMMENT 'Haverá um acompanhamento junto aos participantes, realizado pelo responsável pelo projeto, após o termino do mesmo?',
  `indicador11` varchar(500) DEFAULT NULL COMMENT 'O projeto tem ligação com algum projeto de pesquisa?',
  `indicador12` varchar(500) DEFAULT NULL COMMENT 'O projeto estimulará a aprendizagem de uma postura avaliativa e crítica responsável dos alunos',
  `indicador13` varchar(500) NOT NULL COMMENT 'O projeto possibilitará ações efetivas de alunos junto a grupos de pesquisa da universidade?',
  `indicador14` varchar(500) DEFAULT NULL COMMENT 'As metas e objetivos definidos beneficiarão a comunidade externa participante do projeto?',
  `indicador15` varchar(500) NOT NULL COMMENT 'Haverá melhoria de processo de trabalho do público alvo?',
  `indicador16` varchar(500) DEFAULT NULL COMMENT 'Haverá aquisição de novos conhecimentos pela equipe participante do Projeto?',
  `indicador17` varchar(500) NOT NULL COMMENT 'Haverá continuidade de melhorias do público alvo após o termino do projeto?',
  `indicador18` varchar(500) NOT NULL COMMENT 'Haverá melhoria da qualidade de vida do público alvo?',
  `metodologia` text NOT NULL,
  `local` int(11) NOT NULL,
  `ano_inicio` year(4) NOT NULL,
  `inicio` date NOT NULL,
  `termino` date NOT NULL,
  `situacao` int(1) NOT NULL COMMENT 'Trabalhar com essa informação no código fonte. Ativo, Concluído, Desatualizado.',
  `n_pessoas_alvo_interno` int(11) NOT NULL,
  `n_pessoas_alvo_externo` int(11) NOT NULL,
  `caracterizacao_comunidade` text NOT NULL,
  `objetivo_geral` text NOT NULL,
  `grau_envolvimento_equipe` int(11) NOT NULL,
  `parceria_instituicao` varchar(500) NOT NULL COMMENT 'Parceria com outra instituição',
  `parecer_pdf` varchar(100) DEFAULT NULL COMMENT 'PDF com parecer da unidade sobre o projeto',
  `em_execucao` varchar(50) NOT NULL,
  `quanto_tempo` varchar(50) DEFAULT NULL,
  `em_execucao_descricao` varchar(500) DEFAULT NULL,
  `linha_extensao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `edital` (`edital`),
  KEY `coordenador` (`coordenador`),
  KEY `local` (`local`),
  KEY `grau_envolvimento_equipe` (`grau_envolvimento_equipe`),
  KEY `fk_tipo_extensao` (`tipo_extensao`),
  KEY `fk_linha_extensao` (`linha_extensao`),
  CONSTRAINT `fk_coordenador_usuario` FOREIGN KEY (`coordenador`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `fk_edital` FOREIGN KEY (`edital`) REFERENCES `editais` (`id`),
  CONSTRAINT `fk_grau_envolvimento_equipe` FOREIGN KEY (`grau_envolvimento_equipe`) REFERENCES `graus_envolvimento_equipe` (`id`),
  CONSTRAINT `fk_linha_extensao` FOREIGN KEY (`linha_extensao`) REFERENCES `linhas_extensao` (`id`),
  CONSTRAINT `fk_tipo_extensao` FOREIGN KEY (`tipo_extensao`) REFERENCES `tipo_extensao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.acoes_objetivos
CREATE TABLE IF NOT EXISTS `acoes_objetivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objetivo_especifico` int(11) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  `resultado` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objetivo_especifico` (`objetivo_especifico`),
  CONSTRAINT `fk_objetivo_especifico` FOREIGN KEY (`objetivo_especifico`) REFERENCES `objetivos_especificos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.areas_conhecimento
CREATE TABLE IF NOT EXISTS `areas_conhecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.areas_tematicas
CREATE TABLE IF NOT EXISTS `areas_tematicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.cidades
CREATE TABLE IF NOT EXISTS `cidades` (
  `estados_cod_estados` int(11) DEFAULT NULL,
  `cod_cidades` int(11) DEFAULT NULL,
  `nome` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.editais
CREATE TABLE IF NOT EXISTS `editais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `resumo` varchar(300) NOT NULL,
  `codigo` int(6) NOT NULL,
  `anexo` varchar(100) NOT NULL,
  `ano_base` int(4) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `data_validade` date NOT NULL COMMENT 'Data final para envio de propostas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.edital_tipo_extensao
CREATE TABLE IF NOT EXISTS `edital_tipo_extensao` (
  `id` int(11) NOT NULL,
  `edital_id` int(11) NOT NULL,
  `tipo_extensao_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_edital_tipo_extensao_edital` (`edital_id`),
  KEY `fk_edital_tipo_extensao_tipo_extensao` (`tipo_extensao_id`),
  CONSTRAINT `fk_edital_tipo_extensao_edital` FOREIGN KEY (`edital_id`) REFERENCES `editais` (`id`),
  CONSTRAINT `fk_edital_tipo_extensao_tipo_extensao` FOREIGN KEY (`tipo_extensao_id`) REFERENCES `tipo_extensao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `cod_estados` int(11) DEFAULT NULL,
  `sigla` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.estados_acao
CREATE TABLE IF NOT EXISTS `estados_acao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.graus_envolvimento_equipe
CREATE TABLE IF NOT EXISTS `graus_envolvimento_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.item_orcamento
CREATE TABLE IF NOT EXISTS `item_orcamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo` (`tipo`),
  CONSTRAINT `fk_tipo_orcamento` FOREIGN KEY (`tipo`) REFERENCES `tipo_orcamento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.linhas_extensao
CREATE TABLE IF NOT EXISTS `linhas_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.objetivos_especificos
CREATE TABLE IF NOT EXISTS `objetivos_especificos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  CONSTRAINT `fk_acao_extensao_obj_especifico` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.orcamentos
CREATE TABLE IF NOT EXISTS `orcamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `item_orcamento` int(11) NOT NULL,
  `valor_solicitado` decimal(10,2) NOT NULL,
  `valor_concedido` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `item_orcamento` (`item_orcamento`),
  CONSTRAINT `fk_acao_extensao_orcamento` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_item_orcamento` FOREIGN KEY (`item_orcamento`) REFERENCES `item_orcamento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.participantes
CREATE TABLE IF NOT EXISTS `participantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `instituicao` varchar(50) NOT NULL,
  `funcao` varchar(50) NOT NULL,
  `documento_id` int(11) NOT NULL COMMENT 'RA, Matricula, CPF (Segundo sua função)',
  `data_inicio` date NOT NULL,
  `data_termino` date NOT NULL,
  `curso` varchar(50) NOT NULL,
  `nivel` int(11) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `cep` int(11) NOT NULL,
  `telefone` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  CONSTRAINT `fk_acao_extensao_participante` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.permissao_usuario
CREATE TABLE IF NOT EXISTS `permissao_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) NOT NULL,
  `permissao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`),
  KEY `permissao` (`permissao`),
  CONSTRAINT `fk_permissoes` FOREIGN KEY (`permissao`) REFERENCES `permissoes` (`id`),
  CONSTRAINT `fk_usuarios` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.permissoes
CREATE TABLE IF NOT EXISTS `permissoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.tipo_extensao
CREATE TABLE IF NOT EXISTS `tipo_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.tipo_orcamento
CREATE TABLE IF NOT EXISTS `tipo_orcamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `matricula` int(8) NOT NULL,
  `usuario_dgrh` int(11) DEFAULT NULL,
  `ultimo_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuario_dgrh` (`usuario_dgrh`),
  CONSTRAINT `fk_usuario_dgrh` FOREIGN KEY (`usuario_dgrh`) REFERENCES `usuarios_dgrh` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela sige.usuarios_dgrh
CREATE TABLE IF NOT EXISTS `usuarios_dgrh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cod_local` varchar(50) NOT NULL,
  `local` varchar(50) NOT NULL,
  `matricula` int(8) NOT NULL,
  `ramal` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `perfil` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
