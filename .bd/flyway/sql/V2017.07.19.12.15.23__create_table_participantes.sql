DROP TABLE participantes;

CREATE TABLE `participantes` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador do participante',
	`id_rel_final` INT NOT NULL COMMENT 'Identificador do Relatório Final',
	`nome` VARCHAR(300) NOT NULL COMMENT 'Nome do participante',
	`id_categoria` INT NOT NULL COMMENT 'Categoria do participante',
	`ra` VARCHAR(50) NULL COMMENT 'RA caso seja discente',
	`unidade` VARCHAR(100) NULL COMMENT 'Unidade do participante caso tenha',
	`instituicao` VARCHAR(100) NULL COMMENT 'Instituição do participante caso tenha',
	`carga_horaria_semanal` INT NOT NULL COMMENT 'Carga horaria semanal',
	`carga_horaria_total` INT NOT NULL COMMENT 'Carga horaria total',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_participantes_rel_final` FOREIGN KEY (`id_rel_final`) REFERENCES `rel_final` (`id`),
	CONSTRAINT `FK_participantes_participante_categorias` FOREIGN KEY (`id_categoria`) REFERENCES `participante_categorias` (`id`)
)
COMMENT='Participantes do projeto'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*

participantes

id INT
id_rel_final INT
nome TEXT
id_categoria INT
ra TEXT
unidade TEXT
instituicao TEXT
carga_horaria_semanal INT
carga_horaria_total INT

----

*/