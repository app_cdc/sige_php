--
-- Estrutura para tabela `pergunta_avaliacao`
--

CREATE TABLE IF NOT EXISTS `pergunta_avaliacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pergunta` int(11) NOT NULL DEFAULT '0' COMMENT 'Chave estrangeira da pergunta',
  `id_avaliacao` int(11) NOT NULL DEFAULT '0' COMMENT 'Chave estrangeira da avaliação da ação de extensão (acoes_avaliacao)',
  `nota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_avaliacao_pergunta` (`id_pergunta`),
  KEY `fk_avaliacao_id` (`id_avaliacao`),
  CONSTRAINT `fk_avaliacao_id` FOREIGN KEY (`id_avaliacao`) REFERENCES `acoes_avaliacao` (`id`),
  CONSTRAINT `fk_avaliacao_pergunta` FOREIGN KEY (`id_pergunta`) REFERENCES `perguntas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
