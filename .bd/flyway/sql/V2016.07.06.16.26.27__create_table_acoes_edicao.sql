--
-- Estrutura para tabela `acoes_edicao`
--

CREATE TABLE IF NOT EXISTS `acoes_edicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_acao_extensao` int(11) NOT NULL,
  `id_administrador` int(11) NOT NULL,
  `id_editor` int(11) NOT NULL,
  `data_envio` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_edicao_acao_extensao` (`id_acao_extensao`),
  KEY `fk_edicao_administrador` (`id_administrador`),
  KEY `fk_edicao_editor` (`id_editor`),
  CONSTRAINT `fk_edicao_acao_extensao` FOREIGN KEY (`id_acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_edicao_administrador` FOREIGN KEY (`id_administrador`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `fk_edicao_editor` FOREIGN KEY (`id_editor`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com dados do envio do Administrador para o Editor';
