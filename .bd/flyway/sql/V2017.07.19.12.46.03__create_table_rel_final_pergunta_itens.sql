CREATE TABLE `rel_final_pergunta_itens` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador do item da pergunta',
	`id_pergunta` INT NOT NULL COMMENT 'identificador da pergunta',
	`descricao` VARCHAR(200) NOT NULL COMMENT 'Descrição do item da pergunta',
	`detalhe` TINYINT(1) NOT NULL COMMENT 'Se tem detalhe ou não',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_itens_rel_final_perguntas` FOREIGN KEY (`id_pergunta`) REFERENCES `rel_final_perguntas` (`id`)
)
COMMENT='Itens das perguntas do relatorio tecnico final'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


/*
rel_final_pergunta_itens

id INT
id_pergunta INT
descricao TEXT
detalhe BOOLEAN
*/