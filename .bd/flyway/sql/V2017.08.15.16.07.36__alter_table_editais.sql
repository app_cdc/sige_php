ALTER TABLE `editais`
	ADD COLUMN `imagem` DATE NULL COMMENT 'Imagem para a tela editais';

ALTER TABLE `editais`
	ADD COLUMN `cabecalho_relatorio` DATE NULL COMMENT 'Imagem para o cabeçalho dos relatórios';
