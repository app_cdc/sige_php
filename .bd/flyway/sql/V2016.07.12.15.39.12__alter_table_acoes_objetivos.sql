ALTER TABLE `acoes_objetivos`
	ALTER `descricao` DROP DEFAULT,
	ALTER `resultado` DROP DEFAULT;
ALTER TABLE `acoes_objetivos`
	CHANGE COLUMN `descricao` `descricao` TEXT NOT NULL AFTER `objetivo_especifico`,
	CHANGE COLUMN `resultado` `resultado` TEXT NOT NULL AFTER `descricao`;
