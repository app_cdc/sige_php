ALTER TABLE `edital_rel_final_perguntas`
	DROP FOREIGN KEY `FK_editais_perguntas`;
ALTER TABLE `edital_rel_final_perguntas`
	ADD CONSTRAINT `FK_editais_perguntas` FOREIGN KEY (`id_pergunta`) REFERENCES `rel_final_perguntas` (`id`);
