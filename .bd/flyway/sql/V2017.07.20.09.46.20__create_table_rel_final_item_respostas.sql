CREATE TABLE `rel_final_item_respostas` (
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador da resposta',
	`id_rel_final` INT NOT NULL COMMENT 'Identificador do relatório',
	`id_item` INT NOT NULL COMMENT 'Identificador do item da pergunta',
	`resposta` TINYINT(1) NOT NULL COMMENT 'Se o item foi selecionado ou não',
	`detalhe` TEXT NULL COMMENT 'Case tenha algum detalhe, ele é preenchido aqui',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_respostas_rel_final` FOREIGN KEY (`id_rel_final`) REFERENCES `rel_final` (`id`),
	CONSTRAINT `FK_respostas_rel_final_pergunta_itens` FOREIGN KEY (`id_item`) REFERENCES `rel_final_pergunta_itens` (`id`)
)
COMMENT='Respostas do relatorio tecnico final'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
rel_final_item_respostas

id INT
id_rel_final INT
id_item INT
resposta BOOLEAN?
detalhe TEXT NULL
*/