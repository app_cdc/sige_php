ALTER TABLE `editais`
	ADD COLUMN `data_divulgacao` DATE NULL COMMENT 'Data da divulgação do edital' AFTER `ativo`;
