CREATE TABLE `edital_perguntas` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`id_edital` INT NOT NULL,
	`id_pergunta` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_perguntas_editais` FOREIGN KEY (`id_edital`) REFERENCES `editais` (`id`),
	CONSTRAINT `FK_editais_perguntas` FOREIGN KEY (`id_pergunta`) REFERENCES `perguntas` (`id`)
)
COMMENT='Perguntas do relatorio final referente ao edital'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
edital_perguntas

id INT
id_edital INT
id_pergunta INT
*/