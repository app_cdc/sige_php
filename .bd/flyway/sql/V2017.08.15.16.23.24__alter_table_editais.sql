ALTER TABLE `editais`
	CHANGE COLUMN `imagem` `imagem` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Imagem para a tela editais' AFTER `data_fim_projeto`,
	CHANGE COLUMN `cabecalho_relatorio` `cabecalho_relatorio` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Imagem para o cabeçalho dos relatórios' AFTER `imagem`;
