--
-- Estrutura para tabela `motivos_desclassificacao`
--

CREATE TABLE IF NOT EXISTS `motivos_desclassificacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com os motivos para desclassificação de um projeto';
