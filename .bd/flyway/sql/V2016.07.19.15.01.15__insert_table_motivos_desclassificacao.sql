INSERT IGNORE INTO `motivos_desclassificacao` (`id`, `motivo`) VALUES
  (1, 'Coordenador está em débito com a apresentação do RELATÓRIO TÉCNICO FINAL'),
  (2, 'Deixou de informar dados solicitados'),
  (3, 'Texto apresentado não atende a o que foi solicitado'),
  (4, 'Proposta vai de encontro ao item 3.8 do Edital'),
  (5, 'Não apresentou ou está incompleto o Parecer da Comissão de Extensão da Unidade');
