ALTER TABLE `acoes_extensao`
	ALTER `indicador2` DROP DEFAULT,
	ALTER `indicador5` DROP DEFAULT,
	ALTER `indicador7` DROP DEFAULT,
	ALTER `indicador8` DROP DEFAULT,
	ALTER `indicador9` DROP DEFAULT;
ALTER TABLE `acoes_extensao`
	CHANGE COLUMN `indicador2` `indicador2` TEXT NOT NULL AFTER `indicador1`,
	CHANGE COLUMN `indicador5` `indicador5` TEXT NOT NULL AFTER `indicador4`,
	CHANGE COLUMN `indicador7` `indicador7` TEXT NOT NULL AFTER `indicador6`,
	CHANGE COLUMN `indicador8` `indicador8` TEXT NOT NULL AFTER `indicador7`,
	CHANGE COLUMN `indicador9` `indicador9` TEXT NOT NULL AFTER `indicador8`;
