CREATE TABLE `parecer_final` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`id_rel_final` INT NOT NULL,
	`id_estado` INT NOT NULL,
	`observacao` TEXT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_parecer_final_rel_final` FOREIGN KEY (`id_rel_final`) REFERENCES `rel_final` (`id`),
	CONSTRAINT `FK_estado_parecer_final_estados` FOREIGN KEY (`id_estado`) REFERENCES `parecer_final_estados` (`id`)
)
COMMENT='Parecer final sobre o relatorio tecnico final'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
parecer_final

id INT
id_rel_final INT
id_estado INT
observacao TEXT NULL
*/