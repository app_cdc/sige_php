CREATE TABLE IF NOT EXISTS `usuarios_relatorio_final_pendente` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`matricula` INT NOT NULL DEFAULT '0',
	`ano` INT NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
