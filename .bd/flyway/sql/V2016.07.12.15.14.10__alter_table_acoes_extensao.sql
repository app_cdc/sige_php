ALTER TABLE `acoes_extensao`
	ALTER `indicador1` DROP DEFAULT,
	ALTER `indicador3` DROP DEFAULT,
	ALTER `indicador4` DROP DEFAULT,
	ALTER `indicador6` DROP DEFAULT,
	ALTER `indicador10` DROP DEFAULT,
	ALTER `indicador17` DROP DEFAULT,
	ALTER `parceria_instituicao` DROP DEFAULT;
ALTER TABLE `acoes_extensao`
	CHANGE COLUMN `indicador1` `indicador1` TEXT NOT NULL COMMENT 'O projeto é indissociável de ensino e pesquisa?' AFTER `tipo_extensao`,
	CHANGE COLUMN `indicador3` `indicador3` TEXT NOT NULL COMMENT 'Contem aspectos relevante na formação dos alunos.' AFTER `indicador2`,
	CHANGE COLUMN `indicador4` `indicador4` TEXT NOT NULL COMMENT 'Prevê aspectos relevantes de uma formação cidadã dos alunos.' AFTER `indicador3`,
	CHANGE COLUMN `indicador6` `indicador6` TEXT NOT NULL COMMENT 'Um projeto de pesquisa foi aplicado em um subconjunto da comunidade?' AFTER `indicador5`,
	CHANGE COLUMN `indicador10` `indicador10` TEXT NOT NULL COMMENT 'Haverá um acompanhamento junto aos participantes, realizado pelo responsável pelo projeto, após o termino do mesmo?' AFTER `indicador9`,
	CHANGE COLUMN `indicador17` `indicador17` TEXT NOT NULL COMMENT 'Haverá continuidade de melhorias do público alvo após o termino do projeto?' AFTER `indicador16`,
	CHANGE COLUMN `parceria_instituicao` `parceria_instituicao` TEXT NOT NULL COMMENT 'Parceria com outra instituição' AFTER `grau_envolvimento_equipe`;
