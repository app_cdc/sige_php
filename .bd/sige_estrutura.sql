-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 17/03/2016 às 13:18
-- Versão do servidor: 5.5.47-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `sige`
--
CREATE DATABASE IF NOT EXISTS `sige` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sige`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `acao_conhecimento`
--

CREATE TABLE IF NOT EXISTS `acao_conhecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `area_conhecimento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `area_conhecimento` (`area_conhecimento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `acao_tematica`
--

CREATE TABLE IF NOT EXISTS `acao_tematica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `area_tematica` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `area_tematica` (`area_tematica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `acoes_extensao`
--

CREATE TABLE IF NOT EXISTS `acoes_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `edital` int(11) NOT NULL,
  `estado_acao` int(11) NOT NULL COMMENT 'Trabalhar via código, os diversos estados da ação.',
  `coordenador` int(11) NOT NULL,
  `criacao` datetime NOT NULL,
  `alteracao` datetime NOT NULL,
  `tipo_extensao` int(11) NOT NULL COMMENT 'Trabalhar futuramente com demais tipos de extensão: Programa, Evento, Prestação de Serviço, etc.',
  `indicador1` varchar(500) NOT NULL COMMENT 'O projeto é indissociável de ensino e pesquisa?',
  `indicador2` varchar(500) NOT NULL COMMENT 'O Projeto é efetivamente de extensão porque ?',
  `indicador3` varchar(500) NOT NULL COMMENT 'Contem aspectos relevante na formação dos alunos.',
  `indicador4` varchar(500) NOT NULL COMMENT 'Prevê aspectos relevantes de uma formação cidadã dos alunos.',
  `indicador5` varchar(500) NOT NULL COMMENT 'Há participação da comunidade externa à Unicamp?',
  `indicador6` varchar(500) NOT NULL COMMENT 'Um projeto de pesquisa foi aplicado em um subconjunto da comunidade?',
  `indicador7` varchar(500) NOT NULL COMMENT 'Trará conhecimento para a comunidade participante do projeto?',
  `indicador8` varchar(500) NOT NULL COMMENT 'Haverá desenvolvimento de habilidades a equipe participante do projeto?',
  `indicador9` varchar(500) NOT NULL COMMENT 'Possibilitará a continuidade de crescimento e desenvolvimento dos participantes do projeto mesmo após o término do projeto?',
  `indicador10` varchar(500) NOT NULL COMMENT 'Haverá um acompanhamento junto aos participantes, realizado pelo responsável pelo projeto, após o termino do mesmo?',
  `indicador11` varchar(500) DEFAULT NULL COMMENT 'O projeto tem ligação com algum projeto de pesquisa?',
  `indicador12` varchar(500) DEFAULT NULL COMMENT 'O projeto estimulará a aprendizagem de uma postura avaliativa e crítica responsável dos alunos',
  `indicador13` varchar(500) NOT NULL COMMENT 'O projeto possibilitará ações efetivas de alunos junto a grupos de pesquisa da universidade?',
  `indicador14` varchar(500) DEFAULT NULL COMMENT 'As metas e objetivos definidos beneficiarão a comunidade externa participante do projeto?',
  `indicador15` varchar(500) NOT NULL COMMENT 'Haverá melhoria de processo de trabalho do público alvo?',
  `indicador16` varchar(500) DEFAULT NULL COMMENT 'Haverá aquisição de novos conhecimentos pela equipe participante do Projeto?',
  `indicador17` varchar(500) NOT NULL COMMENT 'Haverá continuidade de melhorias do público alvo após o termino do projeto?',
  `indicador18` varchar(500) NOT NULL COMMENT 'Haverá melhoria da qualidade de vida do público alvo?',
  `metodologia` text NOT NULL,
  `local` int(11) NOT NULL,
  `ano_inicio` year(4) NOT NULL,
  `inicio` date NOT NULL,
  `termino` date NOT NULL,
  `situacao` int(1) NOT NULL COMMENT 'Trabalhar com essa informação no código fonte. Ativo, Concluído, Desatualizado.',
  `n_pessoas_alvo_interno` int(11) NOT NULL,
  `n_pessoas_alvo_externo` int(11) NOT NULL,
  `caracterizacao_comunidade` text NOT NULL,
  `objetivo_geral` text NOT NULL,
  `grau_envolvimento_equipe` int(11) NOT NULL,
  `parceria_instituicao` varchar(500) NOT NULL COMMENT 'Parceria com outra instituição',
  `parecer_pdf` varchar(100) DEFAULT NULL COMMENT 'PDF com parecer da unidade sobre o projeto',
  `em_execucao` varchar(50) NOT NULL,
  `quanto_tempo` varchar(50) DEFAULT NULL,
  `em_execucao_descricao` varchar(500) DEFAULT NULL,
  `linha_extensao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `edital` (`edital`),
  KEY `coordenador` (`coordenador`),
  KEY `local` (`local`),
  KEY `grau_envolvimento_equipe` (`grau_envolvimento_equipe`),
  KEY `fk_tipo_extensao` (`tipo_extensao`),
  KEY `fk_linha_extensao` (`linha_extensao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `acoes_objetivos`
--

CREATE TABLE IF NOT EXISTS `acoes_objetivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objetivo_especifico` int(11) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  `resultado` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objetivo_especifico` (`objetivo_especifico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `areas_conhecimento`
--

CREATE TABLE IF NOT EXISTS `areas_conhecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `areas_tematicas`
--

CREATE TABLE IF NOT EXISTS `areas_tematicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `editais`
--

CREATE TABLE IF NOT EXISTS `editais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(50) NOT NULL,
  `resumo` varchar(300) NOT NULL,
  `codigo` int(6) NOT NULL,
  `anexo` varchar(100) NOT NULL,
  `ano_base` int(4) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `data_validade` DATE NOT NULL COMMENT 'Data final para envio de propostas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `estados_acao`
--

CREATE TABLE IF NOT EXISTS `estados_acao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `graus_envolvimento_equipe`
--

CREATE TABLE IF NOT EXISTS `graus_envolvimento_equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `item_orcamento`
--

CREATE TABLE IF NOT EXISTS `item_orcamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `linhas_extensao`
--

CREATE TABLE IF NOT EXISTS `linhas_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `objetivos_especificos`
--

CREATE TABLE IF NOT EXISTS `objetivos_especificos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `orcamentos`
--

CREATE TABLE IF NOT EXISTS `orcamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `item_orcamento` int(11) NOT NULL,
  `valor_solicitado` decimal(10,2) NOT NULL,
  `valor_concedido` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`),
  KEY `item_orcamento` (`item_orcamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `participantes`
--

CREATE TABLE IF NOT EXISTS `participantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acao_extensao` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `instituicao` varchar(50) NOT NULL,
  `funcao` varchar(50) NOT NULL,
  `documento_id` int(11) NOT NULL COMMENT 'RA, Matricula, CPF (Segundo sua função)',
  `data_inicio` date NOT NULL,
  `data_termino` date NOT NULL,
  `curso` varchar(50) NOT NULL,
  `nivel` int(11) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `cep` int(11) NOT NULL,
  `telefone` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acao_extensao` (`acao_extensao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `permissao_usuario`
--

CREATE TABLE IF NOT EXISTS `permissao_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) NOT NULL,
  `permissao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`),
  KEY `permissao` (`permissao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `permissoes`
--

CREATE TABLE IF NOT EXISTS `permissoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipo_orcamento`
--

CREATE TABLE IF NOT EXISTS `tipo_orcamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipo_extensao`
--

CREATE TABLE IF NOT EXISTS `tipo_extensao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `matricula` int(8) NOT NULL,
  `usuario_dgrh` int(11) DEFAULT NULL,
  `ultimo_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuario_dgrh` (`usuario_dgrh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios_dgrh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cod_local` varchar(50) NOT NULL,
  `local` varchar(50) NOT NULL,
  `matricula` int(8) NOT NULL,
  `ramal` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `perfil` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cidades`
--

CREATE TABLE `cidades` (
  `estados_cod_estados` int(11) DEFAULT NULL,
  `cod_cidades` int(11) DEFAULT NULL,
  `nome` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `estados`
--

CREATE TABLE `estados` (
  `cod_estados` int(11) DEFAULT NULL,
  `sigla` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `edital_tipo_edital`
--

CREATE TABLE `edital_tipo_extensao` (
  `id` int(11) NOT NULL,
  `edital_id` int(11) NOT NULL,
  `tipo_extensao_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_edital_tipo_extensao_edital` (`edital_id`),
  KEY `fk_edital_tipo_extensao_tipo_extensao` (`tipo_extensao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `acoes_edicao`
--

CREATE TABLE IF NOT EXISTS `acoes_edicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_acao_extensao` int(11) NOT NULL,
  `id_administrador` int(11) NOT NULL,
  `id_editor` int(11) NOT NULL,
  `data_envio` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_edicao_acao_extensao` (`id_acao_extensao`),
  KEY `fk_edicao_administrador` (`id_administrador`),
  KEY `fk_edicao_editor` (`id_editor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com dados do envio do Administrador para o Editor';

-- --------------------------------------------------------

--
-- Estrutura para tabela `acoes_avaliacao`
--

CREATE TABLE IF NOT EXISTS `acoes_avaliacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_acao_extensao` int(11) NOT NULL COMMENT 'Chave estrangeira da ação de extensão',
  `id_editor` int(11) NOT NULL COMMENT 'Chave estrangeira do usuário editor',
  `id_avaliador` int(11) NOT NULL COMMENT 'Chave estrangeira do usuário avaliador',
  `data_envio` datetime NOT NULL COMMENT 'Quando o Editor enviou para o Avaliador',
  `data_avaliacao` datetime DEFAULT NULL COMMENT 'Quando o Avaliador deu todas as notas',
  `observacao` text COMMENT 'Campo observação do avaliador sobre o projeto em avaliação',
  `nota_administrador` int(11) DEFAULT NULL COMMENT 'Replica o resultado, onde ele pode alterar esse valor, esse campo servirá como ordem classificatória dos projetos inscritos',
  PRIMARY KEY (`id`),
  KEY `fk_avaliacao_acao` (`id_acao_extensao`),
  KEY `fk_avaliacao_editor` (`id_editor`),
  KEY `fk_avaliacao_avaliador` (`id_avaliador`),
  CONSTRAINT `fk_avaliacao_acao` FOREIGN KEY (`id_acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  CONSTRAINT `fk_avaliacao_avaliador` FOREIGN KEY (`id_avaliador`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `fk_avaliacao_editor` FOREIGN KEY (`id_editor`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com dados do envio do Editor para o Avaliador';

-- --------------------------------------------------------

--
-- Estrutura para tabela `perguntas`
--

CREATE TABLE IF NOT EXISTS `perguntas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pergunta` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela com as peguntas do edital de “a” até “l”';

-- --------------------------------------------------------

--
-- Estrutura para tabela `pergunta_avaliacao`
--

CREATE TABLE IF NOT EXISTS `pergunta_avaliacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pergunta` int(11) NOT NULL DEFAULT '0' COMMENT 'Chave estrangeira da pergunta',
  `id_avaliacao` int(11) NOT NULL DEFAULT '0' COMMENT 'Chave estrangeira da avaliação da ação de extensão (acoes_avaliacao)',
  `nota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_avaliacao_pergunta` (`id_pergunta`),
  KEY `fk_avaliacao_id` (`id_avaliacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `acao_conhecimento`
--
ALTER TABLE `acao_conhecimento`
  ADD CONSTRAINT `fk_acao_extensao_conhecimento` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  ADD CONSTRAINT `fk_area_conhecimento` FOREIGN KEY (`area_conhecimento`) REFERENCES `areas_conhecimento` (`id`);

--
-- Restrições para tabelas `acao_tematica`
--
ALTER TABLE `acao_tematica`
  ADD CONSTRAINT `fk_acao_extensao_tematica` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  ADD CONSTRAINT `fk_area_tematica` FOREIGN KEY (`area_tematica`) REFERENCES `areas_tematicas` (`id`);

--
-- Restrições para tabelas `acoes_extensao`
--
ALTER TABLE `acoes_extensao`
  ADD CONSTRAINT `fk_grau_envolvimento_equipe` FOREIGN KEY (`grau_envolvimento_equipe`) REFERENCES `graus_envolvimento_equipe` (`id`),
  ADD CONSTRAINT `fk_coordenador_usuario` FOREIGN KEY (`coordenador`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fk_tipo_extensao` FOREIGN KEY (`tipo_extensao`) REFERENCES `tipo_extensao` (`id`),
  ADD CONSTRAINT `fk_edital` FOREIGN KEY (`edital`) REFERENCES `editais` (`id`),
  ADD CONSTRAINT `fk_linha_extensao` FOREIGN KEY (`linha_extensao`) REFERENCES `linhas_extensao` (`id`);

--
-- Restrições para tabelas `acoes_objetivos`
--
ALTER TABLE `acoes_objetivos`
  ADD CONSTRAINT `fk_objetivo_especifico` FOREIGN KEY (`objetivo_especifico`) REFERENCES `objetivos_especificos` (`id`);

--
-- Restrições para tabelas `item_orcamento`
--
ALTER TABLE `item_orcamento`
  ADD CONSTRAINT `fk_tipo_orcamento` FOREIGN KEY (`tipo`) REFERENCES `tipo_orcamento` (`id`);

--
-- Restrições para tabelas `objetivos_especificos`
--
ALTER TABLE `objetivos_especificos`
  ADD CONSTRAINT `fk_acao_extensao_obj_especifico` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`);

--
-- Restrições para tabelas `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD CONSTRAINT `fk_acao_extensao_orcamento` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  ADD CONSTRAINT `fk_item_orcamento` FOREIGN KEY (`item_orcamento`) REFERENCES `item_orcamento` (`id`);

--
-- Restrições para tabelas `participantes`
--
ALTER TABLE `participantes`
  ADD CONSTRAINT `fk_acao_extensao_participante` FOREIGN KEY (`acao_extensao`) REFERENCES `acoes_extensao` (`id`);

--
-- Restrições para tabelas `permissao_usuario`
--
ALTER TABLE `permissao_usuario`
  ADD CONSTRAINT `fk_permissoes` FOREIGN KEY (`permissao`) REFERENCES `permissoes` (`id`),
  ADD CONSTRAINT `fk_usuarios` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`);

--
-- Restrições para tabelas `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuario_dgrh` FOREIGN KEY (`usuario_dgrh`) REFERENCES `usuarios_dgrh` (`id`);

--
-- Restrições para tabelas `edital_tipo_extensao`
--
ALTER TABLE `edital_tipo_extensao`
  ADD CONSTRAINT `fk_edital_tipo_extensao_edital` FOREIGN KEY (`edital_id`) REFERENCES `editais` (`id`),
  ADD CONSTRAINT `fk_edital_tipo_extensao_tipo_extensao` FOREIGN KEY (`tipo_extensao_id`) REFERENCES `tipo_extensao` (`id`);

--
-- Restrições para tabelas `acoes_edicao`
--
ALTER TABLE `acoes_edicao`
  ADD CONSTRAINT `fk_edicao_acao_extensao` FOREIGN KEY (`id_acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  ADD CONSTRAINT `fk_edicao_administrador` FOREIGN KEY (`id_administrador`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fk_edicao_editor` FOREIGN KEY (`id_editor`) REFERENCES `usuarios` (`id`);

--
-- Restrições para tabelas `acoes_avaliacao`
--
ALTER TABLE `acoes_avaliacao`
  ADD CONSTRAINT `fk_avaliacao_acao_extensao` FOREIGN KEY (`id_acao_extensao`) REFERENCES `acoes_extensao` (`id`),
  ADD CONSTRAINT `fk_avaliacao_avaliador` FOREIGN KEY (`id_avaliador`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fk_avaliacao_editor` FOREIGN KEY (`id_editor`) REFERENCES `usuarios` (`id`);

--
-- Restrições para tabelas `pergunta_avaliacao`
--
ALTER TABLE `pergunta_avaliacao`
  ADD CONSTRAINT `fk_avaliacao_id` FOREIGN KEY (`id_avaliacao`) REFERENCES `acoes_avaliacao` (`id`),
  ADD CONSTRAINT `fk_avaliacao_pergunta` FOREIGN KEY (`id_pergunta`) REFERENCES `perguntas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
