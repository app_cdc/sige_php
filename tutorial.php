// abrir o arquivo através do link do seu servidor
$file = 'file.pdf';
// nome do arquivo
$filename = 'file.pdf'; /* Nota: sempre use .pdf no final. */

// Conectando ao banco
$link = mysqli_connect("localhost", "my_user", "my_password", "world");
// Realizando a query
mysqli_query($link, "UPDATE acessos SET contador = contador+1 WHERE `nomearquivo` = $filename);

// exibindo o arquivo
header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');

@readfile($file);