<?php
$manutencao = FALSE;
if ($manutencao){
    $msg = "Em manutenção!";
}else{
    $msg = "Em breve...";
}
?>
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="../style/css/mystyle.css">
        <link rel="stylesheet" href="../style/css/bootstrapValidator.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
        <link rel="icon" href="../style/images/favicon.png" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../style/js/bootstrapValidator.js"></script>
        <script src="../style/js/language/pt_BR.js"></script>
        <script type="text/javascript" src="../style/js/jquery.maskMoney.js"></script>
        <title><?php echo $msg ?></title>
    </head>
    <style>
    *{
        min-height: 100%;
    }
    h1{
        font-family: "Arial Black", Gadget, sans-serif;
    }
    table{
        margin-top:225px;
    }
    

    </style>
    <body style="background-color: #11513c;">
    <div id="content" class="col-sm-12">
        <table>
        <tr>
            <td class="col-sm-4">
                <h1 style="color: white"><?php echo $msg ?></h1>
            </td>
            <td class="col-sm-4"> </td>
            <td class="col-sm-4"> </td>
        </tr>

        <tr>
            <td class="col-sm-4"> </td>
            <td class="col-sm-4">
                <img style="width: 100%;" src="../style/images/logo_sige_branco.png" alt="">
            </td>
            <td class="col-sm-4"> </td>
        </tr>

        <tr>
            <td class="col-sm-4"> </td>
            <td class="col-sm-4"> </td>
            <td class="col-sm-4" style="text-align: right;">
                <h1 style="color: white">Aguarde</h1>
            </td>
        </tr>
        </table>
    </div>
    </body>

</html>